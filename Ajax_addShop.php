<?php
	require_once("top.php");
	header('Content-Type: application/json; charset=utf-8');
	
	$debug = false;

	if($debug){
		print_r($_POST);
		return;
	}

	$error = false;

	// init data
	$action = $_POST['action'];	

	// $shop_no = $_POST['shop_no'];
	$shop_name = $_POST['shop_name'];
	$fax = $_POST['fax'];	
	$address = $_POST['address'];
	$email = $_POST['email'];
	$website = $_POST['website'];
	$phone = $_POST['phone'];
	$ip_address = $_POST['ip_address'];
	// $device_token = $_POST['device_token'];
	// $latitude = $_POST['latitude'];
	// $longitude = $_POST['longitude'];

	$result = [];

	switch ($action) {
		case 'add':
			// get new staff int/id
			$sql_new_id = "SELECT MAX(id) AS new_id FROM shop";
			$rs_new_id = mysqli_query($db_conn,$sql_new_id) or die ("$sql_new_id: ".mysqli_error($db_conn));
			$row_new_id = mysqli_fetch_assoc($rs_new_id);
			$new_shop_id = ($row_new_id['new_id'] + 1);

			if(!$new_shop_id){
				$result['status'] = false;
				$result['msg'] = "FAIL TO GET NEW SHOP ID";
			}else{
				// Create Shop
				mysqli_query($db_conn, "START TRANSACTION");
				$shop_number = 'SH'.str_pad($new_shop_id, 3, '0', STR_PAD_LEFT);
				$sql_create_shop = "INSERT INTO shop 
									 (id,
									  shop_no,
									  name,
									  address,
									  phone,
									  fax,
									  email,
									  website)
									 VALUES
									 ($new_shop_id,
									 '$shop_number',
									 '$shop_name',
									 '$address',
									 '$phone',
									 '$fax',
									 '$email',
									 '$website'
									 );
									";
				$rs_create_shop = mysqli_query($db_conn,$sql_create_shop) or die ("$sql_create_shop: ".mysqli_error($db_conn));
				if($rs_create_shop){
					// insert shop ip
					$new_shop_id = mysqli_insert_id($db_conn);
					$sql_create_ip = "INSERT INTO shop_fixed_ip (shop_id,ip_address) VALUES ('$new_shop_id','$ip_address')";
					$rs_new_ip = mysqli_query($db_conn,$sql_create_ip) or die ("$sql_create_ip: ".mysqli_error($db_conn));

					if($rs_new_ip){
						mysqli_query($db_conn, "COMMIT");
						$result['status'] = true;
						$result['msg'] = 'shop create successful';
					}else{
						mysqli_query($db_conn, "ROLLBACK");
						$result['status'] = false;
						$result['msg'] = $sql_create_ip;
					}


				}else{
					mysqli_query($db_conn, "ROLLBACK");
					$result['status'] = false;
					$result['msg'] = $sql_create_shop;
				}
			}
			break;
		case 'edit':
			$shop_id = $_POST['shop_id'];
			if($shop_id){
				mysqli_query($db_conn, "START TRANSACTION");
				$sql_update_shop = "UPDATE shop SET name = '$shop_name',
													 address = '$address',
													 phone = '$phone',
													 fax = '$fax',
													 email = '$email',
													 website = '$website'

									 WHERE id = $shop_id";
				$rs_update_shop = mysqli_query($db_conn,$sql_update_shop) or die ("$sql_update_shop: ".mysqli_error($db_conn));
				if($rs_update_shop){
					// update shop ip
					$sql_update_ip = "UPDATE shop_fixed_ip SET ip_address = '$ip_address' WHERE shop_id = $shop_id";
					$rs_update_ip = mysqli_query($db_conn,$sql_update_ip) or die ("$sql_update_ip: ".mysqli_error($db_conn));
					if($rs_update_ip){
						mysqli_query($db_conn, "COMMIT");
						$result['status'] = true;
						$result['msg'] = 'shop update successful';
					}else{
						mysqli_query($db_conn, "ROLLBACK");
						$result['status'] = false;
						$result['msg'] = 'Shop update FAILED';
					}
				}else{
					mysqli_query($db_conn, "ROLLBACK");
					$result['status'] = false;
					$result['msg'] = 'Shop update FAILED';
				}

			}else{
				$result['status'] = false;
				$result['msg'] = 'FAIL TO GET Shop ID';
			}
			break;
		default:
			$result['status'] = false;
			$result['msg'] = 'FAIL TO GET ACTION';
			break;
	}


	echo json_encode($result);


	require_once("bottom.php");
?>