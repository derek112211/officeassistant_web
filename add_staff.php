<?php
  require_once("top.php");
  require_once("header.php");
  require_once("page_check.php");
  
  $debug = false;
  $page_title = "Add Staff";
  $sidebar = "staff_management";

  // prepare staff data
  $sql_member = "SELECT * FROM user_staff";
  $rs_member = mysqli_query($db_conn, $sql_member) or die ("$sql_member : ".mysqli_error($db_conn));
  $row_member = mysqli_fetch_assoc($rs_member);

  // prepare job title
  $job_titles = [];
  $sql_job_title = "SELECT * FROM leave_application_approver_type";
  $rs_job_title = mysqli_query($db_conn, $sql_job_title) or die ("$sql_member : ".mysqli_error($db_conn));
  while($row_job_title = mysqli_fetch_assoc($rs_job_title)){
    $job_titles[$row_job_title['id']] = $row_job_title['name_chi'];
  };

  // prepare shop
  $shops = [];
  $sql_shop = "SELECT * FROM shop";
  $rs_shop = mysqli_query($db_conn, $sql_shop) or die ("$sql_shop : ".mysqli_error($db_conn));
  while($row_shop = mysqli_fetch_assoc($rs_shop)){
    $shops[$row_shop['id']] = $row_shop['name'];
  };

  // get staff id
  if(isset($_POST['staff_id']) && $_POST['staff_id']!=''){
    $staff_id = $_POST['staff_id'];
    $sql_get_staff = "SELECT u.*
                      FROM user_staff AS u
                      WHERE u.id = $staff_id 
                      LIMIT 1";

    if($debug){
      echo '<br>$sql_get_staff<br>';
      print_r($sql_get_staff);
    }

    $rs_get_staff = mysqli_query($db_conn, $sql_get_staff) or die ("$sql_get_staff : ".mysqli_error($db_conn));

    $row_staff = mysqli_fetch_assoc($rs_get_staff);
    // print_r($row_staff);
  }

?>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

<?php 

require_once('nav.php');
require_once('sidebar.php'); 

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $page_title ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="homePage.php">Home</a></li>
              <li class="breadcrumb-item"><a href="staff_management.php">Staff Management</a></li>
              <li class="breadcrumb-item active"><?php echo $page_title ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title"><?php echo $page_title ?></h3>
              </div>
              <div class="row">
              <?php
                if(isset($error_msg)){
                  echo '<div class="alert alert-danger">';
                  echo '<h5><i class="icon fas fa-ban"></i> Error!</h5>';
                  echo $error_msg;
                  echo '</div>';
                }
              ?>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <!-- form start -->
                <form role="form" id="form_add_staff" action="" method="post">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>User Name</label>
                        <input type="text" class="form-control" name="user_name" id="user_name" placeholder="Please input user name" value="<?php echo isset($row_staff['user_name']) ? $row_staff['user_name'] : ''; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>First Name (ENG)</label>
                          <input type="text" class="form-control" name="firstname_eng" id="firstname_eng" placeholder="Please input First Name (English)" value="<?php echo isset($row_staff['first_name_eng']) ? $row_staff['first_name_eng'] : ''; ?>">
                        </div>                        
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Last Name (ENG)</label>
                          <input type="text" class="form-control" name="lastname_eng" id="lastname_eng" placeholder="Please input Last Name (English)" value="<?php echo isset($row_staff['last_name_eng']) ? $row_staff['last_name_eng'] : ''; ?>">
                        </div>                        
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>First Name (Chinese)</label>
                          <input type="text" class="form-control" name="firstname_chi" id="firstname_chi" placeholder="Please input First Name (Chinese)" value="<?php echo isset($row_staff['first_name']) ? $row_staff['first_name'] : ''; ?>">
                        </div>                        
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Last Name (Chinese)</label>
                          <input type="text" class="form-control" name="lastname_chi" id="lastname_chi" placeholder="Please input Last Name (Chinese)" value="<?php echo isset($row_staff['last_name']) ? $row_staff['last_name'] : ''; ?>">
                        </div>                        
                      </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password1" id="password1" placeholder="Please input password">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Password Confirm</label>
                        <input type="password" class="form-control" name="password2" id="password2" placeholder="Please input password again">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Short Name</label>
                        <input type="text" class="form-control" name="short_name" id="short_name" placeholder="Please input short name" value="<?php echo isset($row_staff['short_name']) ? $row_staff['short_name'] : ''; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control" name="address" id="address" placeholder="Please input address" value="<?php echo isset($row_staff['address']) ? $row_staff['address'] : ''; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>E-mail</label>
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                            </div>
                           <input type="email" name="email" class="form-control" id="email" placeholder="Please enter email" value="<?php echo isset($row_staff['email']) ? $row_staff['email'] : ''; ?>">
                          </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Phone</label>
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-phone"></i></span>
                            </div>
                            <?php

                            ?>
                            <input type="text" class="form-control" data-inputmask='"mask": "9999-9999"' data-mask name="phone_display" id="phone_display" placeholder="Please input phone number" value="<?php echo isset($row_staff['phone']) ? $row_staff['phone'] : ''; ?>">
                            <input type="hidden" class="form-control" name="phone" id="phone" value="<?php echo isset($row_staff['phone']) ? $row_staff['phone'] : ''; ?>">
                          </div>
                      </div>
                    </div>
                  </div>                  

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Date of birth</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask="" im-insert="false" name="dob" id="dob" value="<?php echo isset($row_staff['dob']) ? $row_staff['dob'] : ''; ?>">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>ID Card Number</label>
                        <input type="text" class="form-control" name="id_card" id="id_card" placeholder="Please input id card number" value="<?php echo isset($row_staff['identity_number']) ? $row_staff['identity_number'] : ''; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Sex</label>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="sex" value="1" <?php echo (isset($row_staff['sex']) && $row_staff['sex'] == 1) ? 'checked' : ''; ?>>
                          <label class="form-check-label">M</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="sex" value="0" <?php echo (isset($row_staff['sex']) && $row_staff['sex'] == 0) ? 'checked' : ''; ?>>
                          <label class="form-check-label">F</label>
                        </div>
                      </div>
                    </div>
                  </div>

<!--                   <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Job Title</label>
                        <select class="form-control" name="job_title" id="job_title">
                          <option value="-1">Please Select</option>
                          <?php
                            if(isset($row_staff['job_title_id'])){
                              $job_title_id = $row_staff['job_title_id'];
                            }
                            foreach ($job_titles as $key => $job_title) {
                              echo '<option value="'.$key.'" '.formOption_isSelected($key,$job_title_id).'>';
                              echo $job_title;
                              echo '</option>';
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div> -->

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Shop</label>
                        <select class="form-control" name="shop" id="shop">
                          <option value="-1">Please Select</option>
                          <?php
                            if(isset($row_staff['home_shop_id'])){
                              $shop_id = $row_staff['home_shop_id'];
                            }
                            foreach ($shops as $key => $shop_name) {
                              echo '<option value="'.$key.'"'.formOption_isSelected($key,$shop_id).'>';
                              echo $shop_name;
                              echo '</option>';
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-check">
                      <?php
                          $is_admin = '';
                          if(isset($row_staff['is_admin'])){
                            $is_admin = $row_staff['is_admin'];
                          }
                      ?>
                     <input type="checkbox" name="cms_admin" class="form-check-input" id="cms_admin" <?php echo formCheckBox_isChecked($is_admin); ?>>
                      <label class="form-check-label" for="cms_admin">CMS Admin</label>
                    </div>
                  </div>

                  <?php
                    if(isset($row_staff)){

                  ?>
                    <input type="hidden" name="action" value="edit">
                    <input type="hidden" name="staff_id" value="<?php echo $row_staff['id']; ?>">
                  <?php }else{ ?>
                    <input type="hidden" name="action" value="add">
                  <?php } ?>

                  <!-- footer -->
                  <div class="card-footer">
                  <button type="button" class="btn btn-info" onclick="check_submit();">Submit</button>
                  <button type="reset" class="btn btn-default float-right">Reset</button>
                </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
  // load footer
  require_once("footer.php");

  // close resources, without global resources
  require_once("bottom.php");
?>
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">

$(document).ready(function () {
  $('[data-mask]').inputmask();

  $.validator.setDefaults({
    submitHandler: function () {
      check_submit();
    }
  });
  $('#form_add_staff').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password1: {
        required: true,
        minlength: 5
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password1: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});

function check_submit(){

  // user name check
  if (!$("#user_name").val().length) {
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_NAME; ?>", function() {
                setTimeout(function() {
                  $("#user_name").focus();
                }, 10);
              });
      return false;
  }

  // first name & last name eng check
  if (!$("#firstname_eng").val().length) {
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_NAME_FIRST_ENG; ?>", function() {
                setTimeout(function() {
                  $("#firstname_eng").focus();
                }, 10);
              });
      return false;
  }
  if (!$("#lastname_eng").val().length) {
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_NAME_LAST_ENG; ?>", function() {
                setTimeout(function() {
                  $("#lastname_eng").focus();
                }, 10);
              });
      return false;
  }

  // first name & last name chi check
  if (!$("#firstname_chi").val().length) {
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_NAME_FIRST_CHI; ?>", function() {
                setTimeout(function() {
                  $("#firstname_chi").focus();
                }, 10);
              });
      return false;
  }
  if (!$("#lastname_chi").val().length) {
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_NAME_LAST_CHI; ?>", function() {
                setTimeout(function() {
                  $("#lastname_chi").focus();
                }, 10);
              });
      return false;
  }

  // password check
  if (!$("#password1").val().length) {
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_PASSWORD; ?>", function() {
                setTimeout(function() {
                  $("#password").focus();
                }, 10);
              });
      return false;
  }

  // password re input check
  if (!$("#password2").val().length) {
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_PASSWORD2; ?>", function() {
                setTimeout(function() {
                  $("#password2").focus();
                }, 10);
              });
      return false;
  }

  // short name check
  if (!$("#short_name").val().length) {
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_SHORTNAME; ?>", function() {
                setTimeout(function() {
                  $("#short_name").focus();
                }, 10);
              });
      return false;
  }

  // address check
  if (!$("#address").val().length) {
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_ADDRESS; ?>", function() {
                setTimeout(function() {
                  $("#address").focus();
                }, 10);
              });
      return false;
  }

  // email check
  if (!$("#email").val().length) {
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_EMAIL; ?>", function() {
                setTimeout(function() {
                  $("#email").focus();
                }, 10);
              });
      return false;
  }

  // phone check
  if ($("#phone_display").val().length) {
      var phone_val = $("#phone_display").val().replace(/[-_]/gi, '');
      // format error
      if(phone_val.length != 8){
        bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_PHONE_FORMAT; ?>", function() {
                  setTimeout(function() {
                    $("#phone").focus();
                  }, 10);
                });
        return false;        
      }else{
        $("#phone").val(phone_val);
      }
  }else{
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_PHONE; ?>", function() {
                setTimeout(function() {
                  $("#phone").focus();
                }, 10);
              });
      return false;  
  }

  // birth check
  if (!$("#dob").val().length) {
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_BIRTH; ?>", function() {
                setTimeout(function() {
                  $("#dob").focus();
                }, 10);
              });
      return false;
  }else{
    // format error
    if($("#dob").val().includes("d") || $("#dob").val().includes("m") || $("#dob").val().includes("y")){
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_BIRTH_FORMAT; ?>", function() {
                setTimeout(function() {
                  $("#dob").focus();
                }, 10);
              });
      return false;      
    }
  }

  // id card check
  if (!$("#id_card").val().length) {
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_ID_CARD; ?>", function() {
                setTimeout(function() {
                  $("#id_card").focus();
                }, 10);
              });
      return false;
  }

  var radio_buttons = $("input[name='sex']");
  if( radio_buttons.filter(':checked').length == 0){
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_SEX; ?>", function() {
                setTimeout(function() {
                  $("#input[name=sex]").focus();
                }, 10);
              });
      return false;
  }

  // if ($("#job_title").val()==-1) {
  //     bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_JOBTITLE; ?>", function() {
  //               setTimeout(function() {
  //                 $("#job_title").focus();
  //               }, 10);
  //             });
  //     return false;
  // }
  if ($("#shop").val()==-1) {
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_SHOP; ?>", function() {
                setTimeout(function() {
                  $("#shop").focus();
                }, 10);
              });
      return false;
  }

  if ($("#password1").val() != $("#password2").val()) {
      bootbox.alert("<?php echo __LANG_RES_ERROR_MISSING_PASSWORD3; ?>", function() {
                setTimeout(function() {
                  $("#password2").focus();
                }, 10);
              });
      return false;
  }

  var action = $('#action').val();

  $.ajax({
   type: "POST",
   url: "Ajax_addStaff.php",
   data: $("#form_add_staff").serialize(),
   type:"POST",
   success: function(data)
   {
      // console.log(data.status);
      // console.log(data.msg);
      if(data.status){
        window.location.href = "staff_management.php";
        // bootbox.alert(data.msg);
      }else{
        bootbox.alert(data.msg);
      }
   },
   error:function(xhr, ajaxOptions, thrownError){ 
      alert(xhr.status); 
      alert(thrownError); 
   }
 });  
}
</script>
