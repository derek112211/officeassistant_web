<?php
  require_once("top.php");
  require_once("header.php");
  require_once("page_check.php");
  
  $debug = false;
  $page_title = "Leave Details";
  $sidebar = "leave_management";

  if($debug){
    print_r($_POST);
  }
  

  $leave_application_form_id = $_POST['leave_appliaction_form_id'];
  $leave_application_date_id = $_POST['leave_application_date_id'];
  $staff_id = $_POST['staff_id'];

  // get staff
  $staffs = [];
  $sql_staff = "SELECT * FROM user_staff";
  $rs_staff = mysqli_query($db_conn,$sql_staff) or die ("$sql_staff :".mysqli_error($db_conn));
  while($row_staff = mysqli_fetch_assoc($rs_staff)){
    $staffs[$row_staff['id']] = $row_staff;
  } 

  // get leave application
  $sql_leave_application = "SELECT laf.*,
                                   s.name AS shop_name,
                                   lad.department_approve_status,
                                   lad.department_approve_staff_id,
                                   lad.department_approve_time,
                                   lad.department_approve_remark,
                                   lad.hr_approve_status,
                                   lad.hr_approve_time,
                                   lad.hr_approve_staff_id,
                                   lad.hr_approve_remark,
                                   lad.id AS leave_application_date_id
                            FROM leave_application_form AS laf
                            LEFT JOIN shop AS s ON s.id = laf.shop_id
                            LEFT JOIN leave_application_date AS lad ON lad.leave_application_form_id = laf.id
                            WHERE laf.delete_staff_id = 0
                            AND laf.id = {$_POST['leave_appliaction_form_id']}

                           ";
  $rs_leave_application = mysqli_query($db_conn,$sql_leave_application) or die ("$sql_leave_application :".mysqli_error($db_conn));
  $leave_application = mysqli_fetch_assoc($rs_leave_application);

  // get staff check in record limit 5
  // $check_in = [];
  // $check_in_dates = [];
  $sql_check_in = "SELECT * FROM staff_checkin WHERE staff_id = $staff_id ORDER BY id DESC LIMIT 5";
  $rs_check_in = mysqli_query($db_conn,$sql_check_in) or die ("$sql_check_in :".mysqli_error($db_conn));


  $check_in = [];
  $check_in_dates = [];
  $sql_check_in_date = "SELECT * FROM staff_checkin WHERE staff_id = $staff_id GROUP BY year,month,day ORDER BY id DESC";
  $rs_check_in_date = mysqli_query($db_conn,$sql_check_in_date) or die ("$sql_check_in_date :".mysqli_error($db_conn));
  while($row_check_in_date = mysqli_fetch_assoc($rs_check_in_date)){
    $check_in_date = explode(' ', $row_check_in_date['timestamp'])[0];
    $check_in_dates[] = "'".$check_in_date."'";

    if(array_key_exists($check_in_date, $check_in)){
      
      if($row_check_in_date['timestamp'] < $check_in[$check_in_date]['timestamp']){
        $check_in[$check_in_date] = $row_check_in_date;
      }
    }else{
      $check_in[$check_in_date] = $row_check_in_date;
    }
  }

  // get staff schedule
  $sql_schedule = "SELECT s.* ,si.*
                   FROM staff_schedule AS s
                   LEFT JOIN staff_schedule_item AS si ON si.id = s.staff_schedule_item_id
                   WHERE user_staff_id = $staff_id 
                   AND si.is_leave = 0
                   AND working_date IN (".implode(',', $check_in_dates).")
                   ";
  $rs_schedule = mysqli_query($db_conn,$sql_schedule) or die ("$sql_schedule :".mysqli_error($db_conn));
  while($row_schedule = mysqli_fetch_assoc($rs_schedule)){
    foreach ($check_in as $check_in_date => $value) {
      if($check_in_date == $row_schedule['working_date']){
        $check_in[$row_schedule['working_date']]['schedule_start'] = $row_schedule['start_time'];
      }
    }
  }
  // echo '<br>';
  // print_r($check_in);

?>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

<?php 

require_once('nav.php');
require_once('sidebar.php'); 

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $page_title ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="homePage.php">Home</a></li>
              <li class="breadcrumb-item"><a href="leave_management.php">Leave Management</a></li>
              <li class="breadcrumb-item active"><?php echo $page_title ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
                <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fas fa-globe"></i> Application ID - <?php echo $leave_application['id']; ?>
                    <small class="float-right">Create Date: <?php echo $leave_application['create_time']; ?></small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info" style="margin: 20px 0px;">
                <!-- /.col -->
                <div class="col-sm-12 invoice-col">

                  <b>Application #<?php echo $leave_application['id'] ?></b><br>
                  <br>

                  <!-- Status -->
                  <div class="row">
                    <div class="col-4">
                      <b>Status: </b>
                    </div>
                    <div class="col-4">
                        <?php
                              switch ($leave_application['department_approve_status']) {
                                case '-1':
                                  echo 'Waiting Department Head Approve';
                                  break;
                                case '0':
                                  echo '<span style="color:red;">Rejected(Department Head)</span>';
                                  break;
                                default:
                                  switch ($leave_application['hr_approve_status']) {
                                    case '-1':
                                      echo 'Waiting HR Approve';
                                      break;
                                    case '0':
                                      echo '<span style="color:red;">Rejected(HR)</span>';
                                      break;
                                    case '1':
                                      echo '<span style="color:green;">Approved</span>';
                                      break;
                                    default:
                                      echo '<span style="color:red;">Error</span>';
                                      break;
                                  }
                                  break;
                              }
                        ?>
                    </div>
                    <div class="col-4">
                      
                    </div>
                  </div>

                  <!-- Staff Number -->
                  <div class="row">
                    <div class="col-4">
                      <b>Staff Number: </b>
                    </div>
                    <div class="col-4">
                      <?php echo $staffs[$leave_application['user_staff_id']]['staff_number']; ?>
                    </div>
                    <div class="col-4">
                      
                    </div>
                  </div>

                  <!-- Staff Name -->
                  <div class="row">
                    <div class="col-4">
                      <b>Staff Name: </b>
                    </div>
                    <div class="col-4">
                      <?php echo $staffs[$leave_application['user_staff_id']]['full_name']; ?>                  
                    </div>
                    <div class="col-4">
                      
                    </div>
                  </div>

                  <!-- Apply Reason -->
                  <div class="row">
                    <div class="col-4">
                      <b>Reason: </b>
                    </div>
                    <div class="col-4">
                      <?php echo $leave_application['reason']; ?>
                    </div>
                    <div class="col-4">
                      
                    </div>
                  </div>


                  <!-- Department Head Comment -->
                  
                  <?php
                    if($leave_application['department_approve_status'] != -1){
                      echo '<br>';
                      echo '<div>';
                        echo '<div class="row">';
                          echo '<b style="color: brown;">- Department Approve Details - </b>';
                        echo '</div>';

                        // department head
                        echo '<div class="row">';
                          echo '<div class="col-4">';
                          echo '<b>Department Head Name: </b>';
                          echo '</div>';

                          echo '<div class="col-4">';
                          echo $staffs[$leave_application['department_approve_staff_id']]['full_name'];

                          echo '</div>';

                          echo '<div class="col-4">';
                          echo '</div>';
                        echo '</div>';

                        // department comments
                        echo '<div class="row">';
                          echo '<div class="col-4">';
                          echo '<b>Department Comments: </b>';
                          echo '</div>';

                          echo '<div class="col-4">';
                          if($leave_application['department_approve_remark'] != ''){
                            echo $leave_application['department_approve_remark'];
                          }else{
                            echo ' - ';
                          }

                          echo '</div>';

                          echo '<div class="col-4">';
                          echo '</div>';
                        echo '</div>';

                        // department approve time
                        echo '<div class="row">';
                          echo '<div class="col-4">';
                          echo '<b>Comment Time: </b>';
                          echo '</div>';

                          echo '<div class="col-4">';
                          echo $leave_application['department_approve_time'];    
                          echo '</div>';

                          echo '<div class="col-4">';
                          echo '</div>';
                        echo '</div>';

                        
                      echo '</div>';
                    }


                    if($leave_application['hr_approve_status'] != -1){
                    echo '<br>';

                      echo '<div>';
                        echo '<div class="row">';
                          echo '<b style="color: brown;">- HR Approve Details - </b>';
                        echo '</div>';

                        // hr head
                        echo '<div class="row">';
                          echo '<div class="col-4">';
                          echo '<b>HR Name: </b>';
                          echo '</div>';

                          echo '<div class="col-4">';
                          echo $staffs[$leave_application['hr_approve_staff_id']]['full_name'];
                          echo '</div>';

                          echo '<div class="col-4">';
                          echo '</div>';
                        echo '</div>';

                        // hr comments
                        echo '<div class="row">';
                          echo '<div class="col-4">';
                          echo '<b>HR Comments: </b>';
                          echo '</div>';

                          echo '<div class="col-4">';
                          if($leave_application['hr_approve_remark'] != ''){
                            echo $leave_application['hr_approve_remark'];
                          }else{
                            echo ' - ';
                          }

                          echo '</div>';

                          echo '<div class="col-4">';
                          echo '</div>';
                        echo '</div>';
                        
                        // hr approve time
                        echo '<div class="row">';
                          echo '<div class="col-4">';
                          echo '<b>Comment Time: </b>';
                          echo '</div>';

                          echo '<div class="col-4">';
                          echo $leave_application['hr_approve_time'];
                          
                          echo '</div>';

                          echo '<div class="col-4">';
                          echo '</div>';
                        echo '</div>';

                        
                      echo '</div>';
                    }


                  ?>
                  <br>

                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <h4>Staff Last 5 Check-in Records</h4>
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Check-in Type</th>
                      <th>Check-in Time</th>
                      <th>Img</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php
                        while($row_check_in = mysqli_fetch_assoc($rs_check_in)){
                          echo '<tr>';
                          // ID
                          echo '<td>'.$row_check_in['id'].'</td>';

                          // Type
                          echo '<td>';
                          if($row_check_in['type'] == 1){
                            echo 'GPS';
                          }else if($row_check_in['type'] == 2){
                            echo 'QR CODE';
                          }
                          echo'</td>';

                          // Time
                          echo '<td>'.$row_check_in['timestamp'].'</td>';

                          echo '<td>';
                          if($row_check_in['type'] == 1){
                            echo '<button type="button" class="btn btn-info btn-lg" onclick="viewImg($(this))">';
                            echo '<input type="hidden" value="'.$row_check_in['img'].'">';
                            echo 'View Image';
                            echo '</button>';
                          }else if($row_check_in['type'] == 2){
                            echo '--';
                          }

                          echo '</td>';

                          echo '</tr>';

                        }
                      ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <h4>Staff Last 5 DAYS Check-in Records</h4>
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>Date</th>
                      <th>Check-in Type</th>
                      <th>Check-in Time</th>
                      <th>Schedule Start Time</th>
                      <th>Late Status</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php
                        foreach ($check_in as $date => $value) {
                          $check_in_time = explode(' ', $value['timestamp'])[1];
                          $check_in_date = explode(' ', $value['timestamp'])[0];

                          echo '<tr>';
                          // Date
                          echo '<td>'.$date.'</td>';

                          // Type
                          echo '<td>';
                          if($value['type'] == 1){
                            echo 'GPS';
                          }else if($value['type'] == 2){
                            echo 'QR CODE';
                          }
                          echo'</td>';

                          // Time
                          echo '<td>'.$check_in_time.'</td>';

                          // Schedule Start Time
                          echo '<td>';
                          if(isset($value['schedule_start'])){
                            echo $value['schedule_start'];
                          }else{
                            echo ' - ';
                          }
                          echo '</td>';
                          

                          // Late Status
                          echo '<td>';
                          if(isset($value['schedule_start'])){
                            
                            if($check_in_time > $value['schedule_start']){
                              echo '<span style="color:red;">LATE</span>';
                            }else{
                              echo '<span style="color:green;">ON TIME</span>';
                            }
                          }else{
                            echo ' - ';
                          }
                          echo '</td>';


                          echo '</tr>';

                        }
                      ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->


              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  <!-- <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a> -->
                  <a href="leave_management.php">
                  <button type="button" class="btn btn-primary float-right">
                    <i class="fas fa-chevron-left"></i>Back
                  </button>
                  </a>
                </div>
              </div>
            </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="img_title"></h4>

        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <img src="" id="img_src" style="width: 50%;margin: 0 auto;">
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
  </div>
<?php
  // load footer
  require_once("footer.php");

  // close resources, without global resources
  require_once("bottom.php");
?>
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">

$(document).ready(function () {
});

function viewImg(element){
  var img_path = $(element).find('input').val();
  console.log(img_path);

  $('#img_src').attr('src',img_path);
  var name = img_path.split('/')[2];
  $('#img_title').html(name);
  $('#myModal').modal();
}
</script>
