<?php
require_once("top.php");
require_once 'php_lib/api_functions.php';
header('Content-Type: application/json; charset=utf-8');

function checking(&$req) {

    global $db_conn;
    $req["staff_id"] = mysqli_real_escape_string($db_conn,$req['staff_id']);

    if(!api_utils_issetAndNotEqual($req["staff_id"])){
        throw new Exception("Staff id is required");
    }
    return true;
}


function main($request) {
    global $db_conn;
    $res = [];
    $res["status"] = true;
    $res['code'] = "E0001";
    $res['message'] = "Retrieve staff profile successfully.";
    $res['last_request_at'] = date("Y-m-d H:i:s");
    $res['params'] = [];

    $sql_findStaff = "SELECT user_staff.*,user_staff.device_token AS device_code, shop.name AS shop_name , shop.phone AS shop_phone
                        from user_staff, shop 
                        where user_staff.id = '{$request['staff_id']}' 
                        AND user_staff.home_shop_id = shop.id";
    $rs_findStaff = mysqli_query($db_conn, $sql_findStaff);
    if (!$rs_findStaff) {
        if (isset($request["debug"]))
            throw new Exception($sql_find_member . ":" . mysqli_error($db_conn));
        else
            throw new Exception("Server Error");
    }
    
    if(mysqli_num_rows($rs_findStaff) == 0){
        throw new Exception("Staff not exist");
    }
    $res['params'] = mysqli_fetch_assoc($rs_findStaff);
    
    return $res;
}

try {
    checking($_REQUEST);
    $res = main($_REQUEST);
    echo json_encode($res);
} catch (Exception $ex) {
    echo json_encode([
        "status" => false,
        "code" => "E0002",
        "message" => "Staff profile does not exist",
        //"message" => $ex->getMessage(),
        "last_request" => date("Y-m-d H:i:s"),
        "params" => [
            "reason" => $ex->getMessage()
        ]
    ]);
}



require_once("bottom.php");
?>