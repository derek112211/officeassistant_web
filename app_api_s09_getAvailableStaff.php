<?php
require_once("top.php");
require_once 'php_lib/api_functions.php';
header('Content-Type: application/json; charset=utf-8');

function checking(&$req) {

    global $db_conn;
    $req["staff_id"] = mysqli_real_escape_string($db_conn,$req['staff_id']);

    if(!api_utils_issetAndNotEqual($req["staff_id"])){
        throw new Exception("Staff ID is required");
    }    
    return true;
}


function main($request) {
    global $db_conn;
    $res = [];
    $res["status"] = true;
    $res['code'] = "E0001";
    $res['message'] = "Get Available Staff successfully.";
    $res['last_request_at'] = date("Y-m-d H:i:s");
    $res['params'] = api_getAvailableStaff($request);
    return $res;
}

try {
    checking($_POST);
    $res = main($_POST);
    echo json_encode($res);
} catch (Exception $ex) {
    echo json_encode([
        "status" => false,
        "code" => "E0002",
        "message" => "Get Available Staff Failed.Please Contact Office",
        "last_request" => date("Y-m-d H:i:s"),
        "params" => [
            "reason" => $ex->getMessage()
        ]
    ]);

}



require_once("bottom.php");
?>