<?php
	require_once("top.php");
	header('Content-Type: application/json; charset=utf-8');
	
	$debug = false;

	if($debug){
		print_r($_POST);
		return;
	}


	// init data
	$type = $_POST['type'];	
	$uuid = $_POST['uuid'];
	

	$result = [];

	switch ($type) {
		case 'staff':
			$staff_id = $_POST['staff_id'];
			$sql_update_staff = "UPDATE user_staff SET device_token = '$uuid' WHERE id = $staff_id";
			$rs_update_staff = mysqli_query($db_conn,$sql_update_staff) or die("$sql_update_staff : ".mysqli_error($db_conn));
			if($rs_update_staff){
				$result['status'] = true;
				$result['msg'] = "UPDATE STAFF SUCCESS";
				$result['uuid'] = $uuid;
				$result['type'] = $type;
			}else{
				$result['status'] = false;
				$result['uuid'] = $uuid;
				$result['msg'] = "UPDATE STAFF FAILED";
			}
			break;
		case 'shop':
			$uuid_id = $_POST['uuid_id'];
			// $ip = $_POST['ip'];
			mysqli_query($db_conn, "START TRANSACTION");

			// update ip
			// $sql_update_shop_ip = "UPDATE shop_fixed_ip SET ip_address = '$ip' WHERE shop_id = $shop_id";
			// $rs_update_shop_ip = mysqli_query($db_conn,$sql_update_shop_ip) or die("$sql_update_shop_ip : ".mysqli_error($db_conn));

			// update uuid
			$sql_update_shop_uuid = "UPDATE shop_uuid SET device_token = '$uuid' WHERE id = $uuid_id";
			$rs_update_shop_uuid = mysqli_query($db_conn,$sql_update_shop_uuid) or die("$sql_update_shop_uuid : ".mysqli_error($db_conn));

			// if($rs_update_shop_uuid && $rs_update_shop_ip){
			if($rs_update_shop_uuid){
				mysqli_query($db_conn, "COMMIT");
				$result['status'] = true;
				$result['msg'] = "UPDATE SHOP SUCCESS";
				$result['sql'] = $sql_update_shop_uuid;
				$result['uuid'] = $uuid;
				// $result['ip'] = $ip;
				$result['type'] = $type;
			}else{
				mysqli_query($db_conn, "ROLLBACK");
				$result['status'] = false;
				$result['uuid'] = $uuid;
				// $result['ip'] = $ip;
				$result['type'] = $type;
				$result['msg'] = "UPDATE SHOP FAILED";
			}
			break;
		default:
			$result['status'] = false;
			$result['uuid'] = $uuid;
			$result['msg'] = "WRONG TYPE PASSED(shop/staff)";
			break;
	}

	echo json_encode($result);


	require_once("bottom.php");
?>