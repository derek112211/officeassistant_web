<?php
  require_once("top.php");
  require_once("header.php");
  require_once("page_check.php");

  $debug = false;

  $page_title = "Staff Management";
  $sidebar = "staff_management";

  // init table header
  $table_header = [];
  $table_header[] = __LANG_RES_STAFF_MANAGEMENT_STAFF_CODE;
  $table_header[] = __LANG_RES_STAFF_MANAGEMENT_STAFF_NAME;
  $table_header[] = __LANG_RES_STAFF_MANAGEMENT_STAFF_SHOP;
  // $table_header[] = __LANG_RES_STAFF_MANAGEMENT_STAFF_JOB;
  $table_header[] = __LANG_RES_STAFF_MANAGEMENT_STAFF_ADDRESS;
  $table_header[] = __LANG_RES_STAFF_MANAGEMENT_STAFF_PHONE;
  $table_header[] = __LANG_RES_STAFF_MANAGEMENT_STAFF_IS_ADMIN;
  $table_header[] = __LANG_RES_STAFF_MANAGEMENT_STAFF_ACTION;

  // retrieve data from db
  $staffs = [];
  $sql_staff = "SELECT u.*,s.name as shop_name
                FROM user_staff AS u
                LEFT JOIN shop AS s ON s.id = u.home_shop_id
                WHERE leave_date = '0000-00-00' 
               ";

  $rs_staff = mysqli_query($db_conn,$sql_staff) or die ("$sql_staff :".mysqli_error($db_conn));
  while($row_staff = mysqli_fetch_assoc($rs_staff)){
    $staffs[$row_staff['id']] = $row_staff;
  } 

?>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">



<?php 

require_once('nav.php');
require_once('sidebar.php'); 

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $page_title ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="homePage.php">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $page_title ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- TABLE -->

          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-lg-11 col-6"><h3 class="card-title">Staffs</h3></div>
                

                <div class="col-lg-1 col-6">
                  <a href="add_staff.php" class="btn btn-block btn-success" role="button">Add Staff</a>
                </div>
                
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="staff_table" class="table table-bordered table-striped no-warp">
                  <thead>
                  <tr>
                    <?php
                      foreach ($table_header as $header) {
                        echo "<th>".$header."</th>";
                      }
                    ?>
                  </tr>
                  </thead>
                  <tbody>
                    <!-- table body -->
                    <?php
                      foreach ($staffs as $staff) {
                        echo '<tr data-id="'.$staff['id'].'">';
                        echo '<td>'.$staff['staff_number'].'</td>';
                        echo '<td>'.$staff['full_name'].'</td>';
                        echo '<td>'.$staff['shop_name'].'</td>';
                        // echo '<td>'.$staff['job_title'].'</td>';
                        echo '<td>'.$staff['address'].'</td>';
                        echo '<td>'.$staff['phone'].'</td>';
                        echo '<td>'.($staff['is_admin'] == 1 ? "是" : "否").'</td>';
                        echo '<td>';
                        echo '<div class="row">';
                        echo '<div class="col-sm-6 col-lg-6 col-xl-6"><button type="button" class="btn btn-block btn-primary" onclick="edit_staff($(this));">Edit</button></div>';
                        echo '<div class="col-sm-6 col-lg-6 col-xl-6"><button type="button" class="btn btn-block btn-danger" onclick="delete_staff($(this));">Delete</button></div>';
                        echo '</div>';
                        echo '</td>';

                        echo '</tr>';
                      }
                    ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <!-- table footer -->
                  </tr>
                  </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
          </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

<?php
  // load footer
  require_once("footer.php");

  // close resources, without global resources
  require_once("bottom.php");
?>
<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script type="text/javascript">
    $('#staff_table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });

  function delete_staff(element){
    var staff_id = element.parents('tr').attr('data-id');

    bootbox.confirm("<?php echo __LANG_RES_STAFF_CONFIRM_DELETE; ?>", function(result){ 
      if(result){
        $.ajax({
             type: "POST",
             url: "Ajax_deleteStaff.php",
             data: {
                staff_id : staff_id
             },
             type:"POST",
             success: function(data)
             {
                // console.log(data.status);
                // console.log(data.msg);
                if(data.status){
                  window.location.href = "staff_management.php";
                  // bootbox.alert(data.msg);
                }else{
                  bootbox.alert(data.msg);
                }
             },
             error:function(xhr, ajaxOptions, thrownError){ 
                alert(xhr.status); 
                alert(thrownError); 
             }
         });
      }
    });
  }

  function edit_staff(element){
    var staff_id = element.parents('tr').attr('data-id');
    var form = document.createElement("form");
    var element1 = document.createElement("input");

    form.method = "POST";
    form.action = "add_staff.php";

    element1.value = staff_id;
    element1.name = "staff_id";
    element1.style.cssText = "display: none;";
    form.appendChild(element1);

    document.body.appendChild(form);

    form.submit();
    form.remove();
  }

</script>