<?php
	require_once("top.php");
	header('Content-Type: application/json; charset=utf-8');
	
	$debug = false;

	if($debug){
		print_r($_POST);
		return;
	}


	$staff_id = $_POST['staff_id'];


	$result = [];

	// check staff
	$sql_check_staff = "SELECT id FROM user_staff WHERE id = $staff_id";
	$rs_check_staff = mysqli_query($db_conn,$sql_check_staff) or die ("$sql_check_staff: ".mysqli_error($db_conn));
	if(mysqli_num_rows($rs_check_staff) > 0){
		// staff exist
		// delete staff
		$sql_delete_staff = "DELETE FROM user_staff WHERE id = $staff_id";
		$rs_delete_staff = mysqli_query($db_conn,$sql_delete_staff) or die ("$sql_delete_staff: ".mysqli_error($db_conn));
		if($rs_delete_staff){
			// delete success
			$result['status'] = true;
			$result['msg'] = "Staff deleted";
		}else{
			// delete staff failed
			$result['status'] = false;
			$result['msg'] = "NO STAFF IS FOUND";
		}
	}else{
		// staff not exist
		$result['status'] = false;
		$result['msg'] = "NO STAFF IS FOUND";
	}

	echo json_encode($result);


	require_once("bottom.php");
?>