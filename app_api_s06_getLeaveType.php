<?php
require_once("top.php");
require_once 'php_lib/api_functions.php';
header('Content-Type: application/json; charset=utf-8');


function main($request) {
    global $db_conn;
    $res = [];
    $res["status"] = true;
    $res['code'] = "E0001";
    $res['message'] = "Get Leave Type successfully.";
    $res['last_request_at'] = date("Y-m-d H:i:s");
    $res['params'] = api_getLeaveTypes($request);
    return $res;
}

try {
    $res = main($_POST);
    echo json_encode($res);
} catch (Exception $ex) {
    echo json_encode([
        "status" => false,
        "code" => "E0002",
        "message" => "Get Leave Type Failed",
        //"message" => $ex->getMessage(),
        "last_request" => date("Y-m-d H:i:s"),
        "params" => [
            "reason" => $ex->getMessage()
        ]
    ]);

}



require_once("bottom.php");
?>