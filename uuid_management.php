<?php
  require_once("top.php");
  require_once("header.php");
  require_once("page_check.php");

  $debug = false;

  $page_title = "UUID Management";
  $sidebar = "uuid";

  // init table header
  $table_header1 = [];
  $table_header1[] = __LANG_RES_UUID_MANAGEMENT_STAFF_CODE;
  $table_header1[] = __LANG_RES_UUID_MANAGEMENT_STAFF_NAME;
  $table_header1[] = __LANG_RES_UUID_MANAGEMENT_STAFF_UUID;
  $table_header1[] = __LANG_RES_UUID_MANAGEMENT_ACTION;

  // init table header2
  $table_header2 = [];
  $table_header2[] = __LANG_RES_UUID_MANAGEMENT_SHOP_CODE;;
  // $table_header2[] = __LANG_RES_UUID_MANAGEMENT_SHOP_NAME;
  $table_header2[] = __LANG_RES_UUID_MANAGEMENT_SHOP_UUID;
  $table_header2[] = __LANG_RES_UUID_MANAGEMENT_ACTION;

  // handle add uuid
  if(isset($_POST['action']) && $_POST['action'] == 'add'){
    $shop_id = $_POST['shop_id'];
    $uuid = $_POST['uuid'];

    $sql_insert_uuid = "INSERT INTO shop_uuid (shop_id,device_token) VALUES ('$shop_id','$uuid')";
    $rs_insert_uuid = mysqli_query($db_conn,$sql_insert_uuid) or die ("$sql_insert_uuid :".mysqli_error($db_conn));
    if($rs_insert_uuid){
      $alert['style'] = 'success';
      $alert['msg'] = 'INSERT SUCCESS';
    }else{
      $alert['style'] = 'fail';
      $alert['msg'] = 'INSERT FAILED';
    }
  }


  if(isset($_POST['action']) && $_POST['action'] == 'delete'){
    $uuid_id = $_POST['uuid_id'];

    $sql_delete_uuid = "DELETE FROM shop_uuid WHERE id = $uuid_id";
    $rs_delete_uuid = mysqli_query($db_conn,$sql_delete_uuid) or die ("$sql_delete_uuid :".mysqli_error($db_conn));
    if($rs_delete_uuid){
      $alert['style'] = 'success';
      $alert['msg'] = 'DELETE SUCCESS';
    }else{
      $alert['style'] = 'fail';
      $alert['msg'] = 'DELETE FAILED';
    }
  }

  // retrieve data from db
  $staffs = [];
  $sql_staff = "SELECT * FROM user_staff";
  $rs_staff = mysqli_query($db_conn,$sql_staff) or die ("$sql_staff :".mysqli_error($db_conn));
  while($row_staff = mysqli_fetch_assoc($rs_staff)){
    $staffs[$row_staff['id']] = $row_staff;
  } 

  $device_tokens = [];
  $sql_token = "SELECT su.id,s.shop_no, s.name, su.device_token 
               FROM shop_uuid AS su
               LEFT JOIN shop AS s ON s.id = su.shop_id
              ";
  $rs_token = mysqli_query($db_conn,$sql_token) or die ("$sql_token :".mysqli_error($db_conn));
  while($row_token = mysqli_fetch_assoc($rs_token)){
    $device_tokens[$row_token['id']] = $row_token;
  } 

  // prepare shop
  $shops = [];
  $sql_shop = "SELECT * FROM shop";
  $rs_shop = mysqli_query($db_conn,$sql_shop) or die ("$sql_shop :".mysqli_error($db_conn));
  while($row_shop = mysqli_fetch_assoc($rs_shop)){
    $shops[$row_shop['id']] = $row_shop;
  } 

  // retrieve data from db - end  

  if($debug){
    echo '<br>$staffs</br>';
    print_r($staffs);
  }




?>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">



<?php 

require_once('nav.php');
require_once('sidebar.php'); 

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $page_title ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="homePage.php">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $page_title ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- STAFF UUID TABLE -->
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-lg-11 col-6"><h3 class="card-title">Staffs UUID</h3></div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="staff_uuid_table" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <?php
                      foreach ($table_header1 as $header) {
                        echo "<th>".$header."</th>";
                      }
                    ?>
                  </tr>
                  </thead>
                  <tbody>
                    <!-- table body -->
                    <?php
                      foreach ($staffs as $staff) {
                        echo '<tr data-id="'.$staff['id'].'">';
                        echo '<td>'.$staff['staff_number'].'</td>';
                        echo '<td>'.$staff['full_name'].'</td>';
                        echo '<td id="uuid">'.$staff['device_token'].'</td>';
                        echo '<td>';
                        echo '<div class="row">';
                        echo '<div class="col-sm-6 col-lg-6 col-xl-6">';
                        echo '<button type="button" class="btn btn-block btn-primary" onclick="edit_row($(this),\'staff\');">'.__LANG_RES_UUID_MANAGEMENT_ACTION_EDIT.'</button>';
                        echo '</div>';
                        echo '<div class="col-sm-6 col-lg-6 col-xl-6"><button type="button" class="btn btn-block btn-info" onclick="clear_row($(this),\'staff\');">'.__LANG_RES_UUID_MANAGEMENT_ACTION_CLEAR.'</button></div>';
                        echo '</div>';
                        echo '</td>';
                        echo '<input type="hidden" id="old_uuid" value="'.$staff['device_token'].'">';
                        echo '</tr>';
                      }
                    ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <!-- table footer -->
                  </tr>
                  </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
          </div>

      <!-- SHOP UUID TABLE -->
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-lg-11 col-6"><h3 class="card-title">Shop UUID</h3></div>          
              </div>
            </div>

            <!-- /.card-header -->
            <div class="card-body">

              <?php 

              if(isset($alert)){ 
                if($alert['style'] == 'success'){
                  echo '<div class="alert alert-success alert-dismissible">';
                  echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                  echo '<h5><i class="icon fas fa-check"></i> Success!</h5>';
                  echo $alert['msg'].'</div>';
                }else{
                  echo '<div class="alert alert-danger alert-dismissible">';
                  echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                  echo '<h5><i class="icon fas fa-check"></i> FAILED!</h5>';
                  echo $alert['msg'].'</div>';
                }
              } 
              ?>

                <table id="shop_uuid_table" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <?php
                      foreach ($table_header2 as $header) {
                        echo "<th>".$header."</th>";
                      }
                    ?>
                  </tr>
                  </thead>
                  <tbody>
                    <!-- table body -->

                    <!-- Add Row -->
                    <tr>
                      <td>
                        <div class="form-group">
                          <select class="form-control" id="add_shop_no">
                            <?php
                              foreach ($shops as $shop_id => $value) {
                                echo '<option value="'.$shop_id.'">'.$value['shop_no'].' '.$value['name'].'</option>';
                              }
                            ?>
                          </select>
                        </div>
                      </td>
                      <td>
                        <input type="text" class="form-control" id="add_uuid"/>
                      </td>
                      <td>
                        <div class="col-sm-12 col-lg-12 col-xl-12">
                          <button type="button" class="btn btn-block btn-success" onclick="check_add($(this))">Add</button>
                        </div>
                      </td>
                    </tr>
                    <?php
                      foreach ($device_tokens as $device_token) {
                        echo '<tr data-id="'.$device_token['id'].'">';
                        echo '<td>'.$device_token['shop_no'].' '.$device_token['name'].'</td>';
                        // echo '<td>'.$device_token['name'].'</td>';
                        echo '<td id="uuid">'.$device_token['device_token'].'</td>';
                        echo '<td>';
                        echo '<div class="row">';

                          echo '<div class="col-sm-4 col-lg-4 col-xl-4">';
                            echo '<button type="button" class="btn btn-block btn-primary" onclick="edit_row($(this),\'shop\');">';
                              echo __LANG_RES_UUID_MANAGEMENT_ACTION_EDIT;
                            echo '</button>';
                          echo '</div>';



                          echo '<div class="col-sm-4 col-lg-4 col-xl-4">';
                            echo '<button type="button" class="btn btn-block btn-info" onclick="clear_row($(this),\'shop\');">';
                              echo __LANG_RES_UUID_MANAGEMENT_ACTION_CLEAR;
                            echo '</button>';
                          echo '</div>';

                          echo '<div class="col-sm-4 col-lg-4 col-xl-4">';
                            echo '<button type="button" class="btn btn-block btn-danger" onclick="delete_row($(this));">';
                            echo __LANG_RES_UUID_MANAGEMENT_ACTION_DELETE;
                            echo '</button>';
                          echo '</div>';




                        echo '</div>';
                        echo '</td>';
                        echo '<input type="hidden" id="old_uuid" value="'.$device_token['device_token'].'">';
                        // echo '<input type="hidden" id="old_ip" value="'.$shop['ip_address'].'">';
                        echo '</tr>';
                      }
                    ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <!-- table footer -->
                  </tr>
                  </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
          </div>

    </section>
    <!-- /.content -->
  </div>

<?php
  // load footer
  require_once("footer.php");

  // close resources, without global resources
  require_once("bottom.php");
?>
<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#staff_uuid_table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });    
    $('#shop_uuid_table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
});

  function edit_row(element,type){
    var row_uuid = $(element).parents('tr').find('#uuid');

    // change td to input
    var element1 = document.createElement("input");
    element1.value = row_uuid.html();
    element1.className = "form-control";
    element1.id = 'uuid_input';
    row_uuid.html(element1);

    // change btn - ip
    // if(type == 'shop'){
    //   var row_ip = $(element).parents('tr').find('#ip');
    //   var element2 = document.createElement("input");
    //   element2.value = row_ip.html();
    //   element2.className = "form-control";
    //   element2.id = 'ip_input';
    //   row_ip.html(element2);
    // }  

    // change btn - uuid
      // success
      $(element).html('<?php echo __LANG_RES_UUID_MANAGEMENT_ACTION_SUBMIT; ?>');
      $(element).removeClass("btn-primary");
      $(element).addClass("btn-success");
      $(element).attr('onclick',"submit_edit_row($(this),'"+type+"','submit')");

      // cancel btn
      var cancel_btn = $(element).parent().siblings().find('button.btn-info');
      cancel_btn.attr('onclick',"cancel_edit_row($(this),'"+type+"')");
      cancel_btn.html('<?php echo __LANG_RES_UUID_MANAGEMENT_ACTION_CANCEL; ?>');

      // hide delete btn
      var delete_btn = $(element).parent().siblings().find('button.btn-danger');
      if(delete_btn){
        delete_btn.attr('style','display:none;')
      }



  }

  function clear_row(element,type){
    if(type == 'staff'){
      bootbox.confirm('<?php echo __LANG_RES_UUID_MANAGEMENT_CONFIRM_CLEAR_STAFF; ?>',function(result){
        if(result){
         submit_edit_row(element,type,'clear') 
        }
      });    
    }else if(type == 'shop'){
      bootbox.confirm('<?php echo __LANG_RES_UUID_MANAGEMENT_CONFIRM_CLEAR_SHOP; ?>',function(result){
        if(result){
         submit_edit_row(element,type,'clear') 
        }
      });    
    }else{
      bootbox.alert("<?php echo __LANG_RES_COMMON_ERROR; ?>");
    }
  }

  function cancel_edit_row(element,type){
    var submit_btn = $(element).parent().siblings().find('button.btn-success');
    var delete_btn = $(element).parent().siblings().find('button.btn-danger');
    var row_uuid = $(element).parents('tr').find('#uuid');
    var old_uuid = $(element).parents('tr').find('#old_uuid').val();
    // var uuid = $(element).parents('tr').find('#uuid_input').val();
    row_uuid.html(old_uuid);

    // if(type == 'shop'){
    //   var row_ip = $(element).parents('tr').find('#ip');
    //   var old_ip = $(element).parents('tr').find('#old_ip').val();
    //   // var ip = $(element).parents('tr').find('#ip_input').val();

    //   row_ip.html(old_ip);
    // }

    // change btn status
    $(element).html('<?php echo __LANG_RES_UUID_MANAGEMENT_ACTION_CLEAR; ?>');
    $(element).attr('onclick',"clear_row($(this),'"+type+"')");

    submit_btn.attr('onclick',"edit_row($(this),'"+type+"')");
    submit_btn.removeClass('btn-success');
    submit_btn.addClass('btn-primary');
    submit_btn.html('<?php echo __LANG_RES_UUID_MANAGEMENT_ACTION_EDIT; ?>');

    // show delete btn
    delete_btn.attr('style','');
  }


  function submit_edit_row(element,type,action){
    // var cancel_btn = $(element).parent().siblings().find('button');

     // set uuid
     if(action == 'submit'){
      var uuid = $(element).parents('tr').find('#uuid_input').val();
      // find ip value
      // if(type == 'shop'){
      //   var ip = $(element).parents('tr').find('#ip_input').val();
      // }
     }else if(action == 'clear'){
      var uuid = '';  
      // if(type == 'shop'){
      //   var ip = '';
      // } 
     }
    
    switch(type){
      case 'staff':
         var staff_id = $(element).parents('tr').attr('data-id');
         $.ajax({
           type: "POST",
           url: "Ajax_edit_uuid.php",
           data: {
              type : type,
              uuid : uuid,
              staff_id: staff_id
           },
           type:"POST",
           success: function(data)
           {
              // console.log(data.status);
              // console.log(data.msg);
              if(data.status){
                // set new uuid
                var row_uuid = $(element).parents('tr').find('#uuid');
                row_uuid.html(data.uuid);
                var old_uuid = $(element).parents('tr').find('#old_uuid');
                old_uuid.val(data.uuid);

                if(action == 'submit'){
                  // reset btn status
                  $(element).html('<?php echo __LANG_RES_UUID_MANAGEMENT_ACTION_EDIT; ?>');
                  $(element).removeClass('btn-success');
                  $(element).addClass('btn-primary');
                  $(element).attr('onclick',"edit_row($(this),'"+data.type+"')");

                  var cancel_btn = $(element).parent().siblings().find('button.btn-info');
                  cancel_btn.attr('onclick',"clear_row($(this),'"+data.type+"')");
                  cancel_btn.html('<?php echo __LANG_RES_UUID_MANAGEMENT_ACTION_CLEAR; ?>');                  
                }else if(action == 'clear'){
                  // reset btn status
                  $(element).html('<?php echo __LANG_RES_UUID_MANAGEMENT_ACTION_CLEAR; ?>');
                  $(element).attr('onclick',"clear_row($(this),'"+data.type+"')");

                  var submit_btn = $(element).parent().siblings().find('button.btn-primary');
                  submit_btn.attr('onclick',"edit_row($(this),'"+data.type+"')");
                  submit_btn.html('<?php echo __LANG_RES_UUID_MANAGEMENT_ACTION_EDIT; ?>');  
                }else{
                  bootbox.alert("<?php echo __LANG_RES_COMMON_ERROR; ?>");
                }

              }else{
                bootbox.alert("<?php echo __LANG_RES_COMMON_ERROR; ?>");
              }
           },
           error:function(xhr, ajaxOptions, thrownError){ 
              alert(xhr.status); 
              alert(thrownError); 
           }
         }); 
        break;
      case 'shop':
         var uuid_id = $(element).parents('tr').attr('data-id');
         $.ajax({
           type: "POST",
           url: "Ajax_edit_uuid.php",
           data: {
              type : type,
              uuid : uuid,
              // ip: ip,
              uuid_id: uuid_id
           },
           type:"POST",
           success: function(data)
           {
              // console.log(data.status);
              // console.log(data.msg);
              if(data.status){
                // set new uuid
                var row_uuid = $(element).parents('tr').find('#uuid');
                row_uuid.html(data.uuid);
                var old_uuid = $(element).parents('tr').find('#old_uuid');
                old_uuid.val(data.uuid);
                // set new ip
                // var row_ip = $(element).parents('tr').find('#ip');
                // row_ip.html(data.ip);
                // var old_ip = $(element).parents('tr').find('#old_ip');
                // old_ip.html(data.ip);

                if(action == 'submit'){
                  // reset btn status
                  $(element).html('<?php echo __LANG_RES_UUID_MANAGEMENT_ACTION_EDIT; ?>');
                  $(element).removeClass('btn-success');
                  $(element).addClass('btn-primary');
                  $(element).attr('onclick',"edit_row($(this),'"+data.type+"')");

                  var cancel_btn = $(element).parent().siblings().find('button.btn-info');
                  cancel_btn.attr('onclick',"clear_row($(this),'"+data.type+"')");
                  cancel_btn.html('<?php echo __LANG_RES_UUID_MANAGEMENT_ACTION_CLEAR; ?>');
                  var delete_btn = $(element).parent().siblings().find('button.btn-danger');
                  delete_btn.attr('style','');
                }else if(action == 'clear'){
                  // reset btn status
                  $(element).html('<?php echo __LANG_RES_UUID_MANAGEMENT_ACTION_CLEAR; ?>');
                  $(element).attr('onclick',"clear_row($(this),'"+data.type+"')");

                  var submit_btn = $(element).parent().siblings().find('button.btn-primary');
                  submit_btn.attr('onclick',"edit_row($(this),'"+data.type+"')");
                  submit_btn.html('<?php echo __LANG_RES_UUID_MANAGEMENT_ACTION_EDIT; ?>');  
                }else{
                  bootbox.alert("<?php echo __LANG_RES_COMMON_ERROR; ?>");
                }

              }else{
                bootbox.alert("<?php echo __LANG_RES_COMMON_ERROR; ?>");
              }
           },
           error:function(xhr, ajaxOptions, thrownError){ 
              alert(xhr.status); 
              alert(thrownError); 
           }
         }); 
        break;
      default:
        bootbox.alert("<?php echo __LANG_RES_COMMON_ERROR; ?>");
        break;
    }
 
  }

  function check_add(element){

    if (!$("#add_uuid").val()) {
        bootbox.alert("<?php echo __LANG_RES_UUID_MANAGEMENT_ERROR_MISSING_UUID; ?>", function() {
                  setTimeout(function() {
                    $("#add_uuid").focus();
                  }, 10);
                });
        return false;
    }

    var shop_id = $('#add_shop_no').val(); 
    var uuid = $("#add_uuid").val(); 

    var form = document.createElement("form");
    var element1 = document.createElement("input");
    var element2 = document.createElement("input");
    var action = document.createElement("input");

    form.method = "POST";
    form.action = "uuid_management.php";

    element1.value = shop_id;
    element1.name = "shop_id";
    element1.style.cssText = "display: none;";
    form.appendChild(element1);

    element2.value = uuid;
    element2.name = "uuid";
    element2.style.cssText = "display: none;";
    form.appendChild(element2);

    action.value = 'add';
    action.name = "action";
    action.style.cssText = "display: none;";
    form.appendChild(action);

    document.body.appendChild(form);

    form.submit();
    form.remove();
  }

  function delete_row(element){
    bootbox.confirm('<?php echo __LANG_RES_UUID_MANAGEMENT_CONFIRM_DELETE_UUID; ?>',function(result){
        if(result){
            var uuid_id = $(element).parents('tr').attr('data-id');

            var form = document.createElement("form");
            var element1 = document.createElement("input");
            var action = document.createElement("input");

            form.method = "POST";
            form.action = "uuid_management.php";

            element1.value = uuid_id;
            element1.name = "uuid_id";
            element1.style.cssText = "display: none;";
            form.appendChild(element1);

            action.value = 'delete';
            action.name = "action";
            action.style.cssText = "display: none;";
            form.appendChild(action);

            document.body.appendChild(form);

            form.submit();
            form.remove();
        }else{
          return;
        }
    }); 

  }
</script>