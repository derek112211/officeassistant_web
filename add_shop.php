<?php
  require_once("top.php");
  require_once("header.php");
  require_once("page_check.php");
  
  $debug = true;
  $page_title = "Add shop";
  $sidebar = "shop_management";

  if(isset($_POST['shop_id'])){
    $sql_shop = "SELECT s.*,sfi.ip_address,su.device_token FROM shop AS s
                 LEFT JOIN shop_fixed_ip AS sfi ON s.id = sfi.shop_id
                 LEFT JOIN shop_uuid AS su ON s.id = su.shop_id
                 WHERE s.id = {$_POST['shop_id']} LIMIT 1";
    $rs_shop = mysqli_query($db_conn,$sql_shop) or die("$sql_shop ".mysqli_error($db_conn));
    $shop = mysqli_fetch_assoc($rs_shop);
  }

?>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

<?php 

require_once('nav.php');
require_once('sidebar.php'); 

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $page_title ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="homePage.php">Home</a></li>
              <li class="breadcrumb-item"><a href="shop_management.php">Shop Management</a></li>
              <li class="breadcrumb-item active"><?php echo $page_title ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title"><?php echo $page_title ?></h3>
              </div>
              <div class="row">
              <?php
                if(isset($error_msg)){
                  echo '<div class="alert alert-danger">';
                  echo '<h5><i class="icon fas fa-ban"></i> Error!</h5>';
                  echo $error_msg;
                  echo '</div>';
                }
              ?>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <!-- form start -->
                <form role="form" id="form_add_shop" action="" method="post">


                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Shop Number</label>
                        <input type="text" class="form-control" name="shop_no" id="shop_no" value="<?php echo isset($shop['shop_no']) ? $shop['shop_no'] : 'Auto Generate'; ?>" disabled>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Shop Name</label>
                        <input type="text" class="form-control" name="shop_name" id="shop_name" placeholder="Please input shop name" value="<?php echo isset($shop['name']) ? $shop['name'] : ''; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Fax</label>
                        <input type="text" class="form-control" name="fax" id="fax" placeholder="Please input fax number (Optional)" value="<?php echo isset($shop['fax']) ? $shop['fax'] : ''; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control" name="address" id="address" placeholder="Please input address" value="<?php echo isset($shop['address']) ? $shop['address'] : ''; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>E-mail</label>
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                            </div>
                           <input type="email" name="email" class="form-control" id="email" placeholder="Please enter email" value="<?php echo isset($shop['email']) ? $shop['email'] : ''; ?>">
                          </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Website</label>
                        <input type="text" class="form-control" name="website" id="website" placeholder="Please input website (Optional)" value="<?php echo isset($shop['website']) ? $shop['website'] : ''; ?>">
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>IP Address</label>
                        <input type="text" class="form-control" name="ip_address" id="ip_address" placeholder="Please input IP Address" value="<?php echo isset($shop['ip_address']) ? $shop['ip_address'] : ''; ?>">
                      </div>
                    </div>
                  </div>

                  

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Phone</label>
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-phone"></i></span>
                            </div>
                            <input type="text" class="form-control" data-inputmask='"mask": "9999-9999"' data-mask name="phone_display" id="phone_display" placeholder="Please input phone number" value="<?php echo isset($shop['phone']) ? $shop['phone'] : ''; ?>">
                            <input type="hidden" class="form-control" name="phone" id="phone" value="<?php echo isset($shop['phone']) ? $shop['phone'] : ''; ?>">
                          </div>
                      </div>
                    </div>
                  </div>                  

<!--                   <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Latitude</label>
                          <input type="text" class="form-control" name="latitude" id="latitude" placeholder="Please input latitude" value="<?php echo isset($shop['first_name']) ? $shop['latitude'] : '0'; ?>">
                        </div>                        
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Longitude</label>
                          <input type="text" class="form-control" name="longitude" id="longitude" placeholder="Please input longitude" value="<?php echo isset($shop['longitude']) ? $shop['longitude'] : '0'; ?>">
                        </div>                        
                      </div>
                  </div> -->

                  <?php
                    if(isset($_POST['shop_id'])){

                  ?>
                    <input type="hidden" name="action" value="edit">
                    <input type="hidden" name="shop_id" value="<?php echo $shop['id']; ?>">
                  <?php }else{ ?>
                    <input type="hidden" name="action" value="add">
                  <?php } ?>

                  <!-- footer -->
                  <div class="card-footer">
                  <button type="button" class="btn btn-info" onclick="check_submit();">Submit</button>
                  <button type="reset" class="btn btn-default float-right">Reset</button>
                </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
  // load footer
  require_once("footer.php");

  // close resources, without global resources
  require_once("bottom.php");
?>
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">

$(document).ready(function () {
  $('[data-mask]').inputmask();

  $.validator.setDefaults({
    submitHandler: function () {
      check_submit();
    }
  });
  $('#form_add_shop').validate({
    rules: {
      email: {
        required: true,
        email: true,
      }
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});

function check_submit(){

  // user name check
  if (!$("#shop_name").val().length) {
      bootbox.alert("<?php echo __LANG_RES_SHOP_MANAGEMENT_ERROR_MISSING_NAME; ?>", function() {
                setTimeout(function() {
                  $("#shop_name").focus();
                }, 10);
              });
      return false;
  }

  // if (!$("#fax").val().length) {
  //     bootbox.alert("<?php echo __LANG_RES_SHOP_MANAGEMENT_ERROR_MISSING_FAX; ?>", function() {
  //               setTimeout(function() {
  //                 $("#fax").focus();
  //               }, 10);
  //             });
  //     return false;
  // }

  if (!$("#address").val().length) {
      bootbox.alert("<?php echo __LANG_RES_SHOP_MANAGEMENT_ERROR_MISSING_ADDRESS; ?>", function() {
                setTimeout(function() {
                  $("#lastname_eng").focus();
                }, 10);
              });
      return false;
  }

  // email check
  if (!$("#email").val().length) {
      bootbox.alert("<?php echo __LANG_RES_SHOP_MANAGEMENT_ERROR_MISSING_EMAIL; ?>", function() {
                setTimeout(function() {
                  $("#email").focus();
                }, 10);
              });
      return false;
  }

  // phone check
  if ($("#phone_display").val().length) {
      var phone_val = $("#phone_display").val().replace(/[-_]/gi, '');
      // format error
      if(phone_val.length != 8){
        bootbox.alert("<?php echo __LANG_RES_SHOP_MANAGEMENT_ERROR_MISSING_PHONE_FORMAT; ?>", function() {
                  setTimeout(function() {
                    $("#phone").focus();
                  }, 10);
                });
        return false;        
      }else{
        $("#phone").val(phone_val);
      }
  }else{
      bootbox.alert("<?php echo __LANG_RES_SHOP_MANAGEMENT_ERROR_MISSING_PHONE; ?>", function() {
                setTimeout(function() {
                  $("#phone").focus();
                }, 10);
              });
      return false;  
  }

  if (!$("#ip_address").val().length) {
      bootbox.alert("<?php echo __LANG_RES_SHOP_MANAGEMENT_ERROR_MISSING_IP; ?>", function() {
                setTimeout(function() {
                  $("#ip_address").focus();
                }, 10);
              });
      return false;
  }else{
      if(!ValidateIPaddress($("#ip_address").val())){
        bootbox.alert("<?php echo __LANG_RES_SHOP_MANAGEMENT_ERROR_WRONG_IP; ?>", function() {
                setTimeout(function() {
                  $("#ip_address").focus();
                }, 10);
              });
        return false;
      }
  }


  var action = $('#action').val();

  $.ajax({
   type: "POST",
   url: "Ajax_addShop.php",
   data: $("#form_add_shop").serialize(),
   type:"POST",
   success: function(data)
   {
      // console.log(data.status);
      // console.log(data.msg);
      if(data.status){
        window.location.href = "shop_management.php";
        // bootbox.alert(data.msg);
      }else{
        bootbox.alert(data.msg);
      }
   },
   error:function(xhr, ajaxOptions, thrownError){ 
      alert(xhr.status); 
      alert(thrownError); 
   }
 });  
}

function ValidateIPaddress(ipaddress) 
{
 if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress))
  {
    return true;
  }
return false
}

</script>
