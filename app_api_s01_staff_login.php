<?php
require_once 'top.php';
require_once 'php_lib/api_functions.php';
header('Content-Type: application/json; charset=utf-8');

function checking(&$req) {
    global $db_conn;

    if(!api_utils_issetAndNotEqual($req['login_type'])){
        throw new Exception("Login type is required");
    }

    $req["login_type"] = mysqli_real_escape_string($db_conn,$req['login_type']);

    if($req['login_type'] == 1){
        $req["member_phone"] = mysqli_real_escape_string($db_conn,$req['member_phone']);
        $req["password"] = mysqli_real_escape_string($db_conn,$req['password']); 
        if(!api_utils_issetAndNotEqual($req['password'])){
            throw new Exception("Password is required");
        }
        if(api_utils_issetAndNotEqual($req['member_phone']) && !preg_match("/([0-9])\w/", $req['member_phone'])){
            throw new Exception("Invalid staff phone format");
        }
        if(!api_utils_issetAndNotEqual($req['domain'])){
            throw new Exception("Domain Name is required");
        }
    }

    $req["device_code"] = mysqli_real_escape_string($db_conn, $req['device_code']);
    // // $req["ip_address"] = mysqli_real_escape_string($db_conn, $req['ip_address']);
    // // $req["domain"] = mysqli_real_escape_string($db_conn, $req['domain']);
    
 
    

    if(!api_utils_issetAndNotEqual($req['device_code'])){
        throw new Exception("Device Code is required");
    }
    // // if(!api_utils_issetAndNotEqual($req['ip_address'])){
    // //     throw new Exception("IP Address is required");
    // // }
    // // if(!api_utils_issetAndNotEqual($req['domain'])){
    // //     throw new Exception("Domain Name is required");
    // // }


    
    return true;
}

function main($request) {
    global $db_conn;
    $res = [];
    $res["status"] = true;
    $res['code'] = "M0001";
    $res['message'] = "Retrieve staff login profile successfully.";
    $res['last_request_at'] = date("Y-m-d H:i:s");
    if($request['login_type'] == 1)
        $res['params'] = api_staff_login($request);
    else if($request['login_type'] == 0){
        $res['params'] = api_host_login($request);
    }
    return $res;
    
}

try {
    checking($_POST);
    $res = main($_POST);
    echo json_encode($res);

} catch (Exception $ex) {
    echo json_encode([
        "status" => false,
        "code" => "M0002",
        "message" => "Login Error\n".$ex->getMessage(),
        "last_request" => date("Y-m-d H:i:s"),
        "params" => [
            "reason" => $ex->getMessage()
        ]
    ]);
}

require_once("bottom.php");


?>
