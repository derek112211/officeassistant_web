<?php
require_once("top.php");
require_once 'php_lib/api_functions.php';
header('Content-Type: application/json; charset=utf-8');

function checking(&$req) {

    global $db_conn;
    $req["report_type"] = mysqli_real_escape_string($db_conn,$req['report_type']);
    $req["staff_id"] = mysqli_real_escape_string($db_conn,$req['staff_id']);
    $req["shop_id"] = mysqli_real_escape_string($db_conn,$req['shop_id']);

    if(!api_utils_issetAndNotEqual($req["report_type"])){
        throw new Exception("Report Type is required");
    }    
    if(!api_utils_issetAndNotEqual($req["staff_id"])){
        throw new Exception("Staff ID is required");
    }    
    if(!api_utils_issetAndNotEqual($req["shop_id"])){
        throw new Exception("Shop ID is required");
    }
    return true;
}


function main($request) {
    global $db_conn;
    $res = [];
    $res["status"] = true;
    $res['code'] = "E0001";
    $res['message'] = "Get Report Data successfully.";
    $res['last_request_at'] = date("Y-m-d H:i:s");
    $res['params'] = getReportData($request);
    $res['report_type'] = $request['report_type'];
    return $res;
}

try {
    checking($_POST);
    $res = main($_POST);
    echo json_encode($res);
} catch (Exception $ex) {
    echo json_encode([
        "status" => false,
        "code" => "E0002",
        "message" => "Get Report Data Failed.Please Contact Office",
        //"message" => $ex->getMessage(),
        "last_request" => date("Y-m-d H:i:s"),
        "params" => [
            "reason" => $ex->getMessage()
        ]
    ]);

}



require_once("bottom.php");
?>