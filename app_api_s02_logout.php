<?php

require_once("top.php");
require_once 'php_lib/api_functions.php';
header('Content-Type: application/json; charset=utf-8');

function checking(&$req) {
    global $db_conn;

    $req["id"] = mysqli_real_escape_string($db_conn,$req['id']);
    $req["logout_type"] = mysqli_real_escape_string($db_conn,$req['logout_type']);

    if(!api_utils_issetAndNotEqual($req['logout_type'])){
        throw new Exception("Logout type is required");
    }

    if(!api_utils_issetAndNotEqual($req['id'])){
        throw new Exception("ID is required");
    }

    return true;
}

function main($request) {
    global $db_conn;
    $res = [];
    $res["status"] = true;
    $res['code'] = "M0001";
    $res['message'] = "Logout Successful.";
    $res['last_request_at'] = date("Y-m-d H:i:s");
    $res['params'] = api_logout($request,$request['logout_type']);
    // if($request['login_type'] == 1)
    //     $res['params'] = api_logout($request,'staff');
    // else if($request['login_type'] == 0){
    //     $res['params'] = api_logout($request,'host');
    // }
    return $res;
    
}

try {
    checking($_POST);
    $res = main($_POST);
    echo json_encode($res);

} catch (Exception $ex) {
    echo json_encode([
        "status" => false,
        "code" => "M0002",
        "message" => "Logout Error\n".$ex->getMessage(),
        "last_request" => date("Y-m-d H:i:s"),
        "params" => [
            "reason" => $ex->getMessage()
        ]
    ]);
}

require_once("bottom.php");


?>
