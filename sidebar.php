    
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo __GLOBAL_home_page;?>" class="brand-link">
      <img src="../../dist/img/AdminLTELogo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">HRMS</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <?php if($current_user){ ?>
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $current_user['full_name']; ?></a>
        </div>
      </div>
      <?php } ?>
    <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?php echo __GLOBAL_home_page;?>" class="nav-link <?php ($sidebar == 'home') ? 'active' : ''; ?>">
              <i class="nav-icon fas fa-home"></i>
              <p>Home page</p>
            </a>
          </li>

          <!-- Management -->
          <?php
            $management_type = ['staff_management','shop_management','permission_management','leave_management'];
          ?>
          <li class="nav-item has-treeview <?php echo (in_array($sidebar, $management_type)) ? 'menu-open' : ''; ?>">
            <a href="#" class="nav-link <?php echo (in_array($sidebar, $management_type)) ? 'active' : ''; ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Management
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="staff_management.php" class="nav-link <?php echo ($sidebar == 'staff_management') ? 'active' : ''; ?>">
                  <i class="far fa-circle nav-icon text-danger"></i>
                  <p>Staff Management</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="shop_management.php" class="nav-link <?php echo ($sidebar == 'shop_management') ? 'active' : ''; ?>">
                  <i class="far fa-circle nav-icon text-danger"></i>
                  <p>Shop Management</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="permission_management.php" class="nav-link <?php echo ($sidebar == 'permission_management') ? 'active' : ''; ?>">
                  <i class="far fa-circle nav-icon text-danger"></i>
                  <p>Permission Management</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="leave_management.php" class="nav-link <?php echo ($sidebar == 'leave_management') ? 'active' : ''; ?>">
                  <i class="far fa-circle nav-icon text-danger"></i>
                  <p>Leave Management</p>
                </a>
              </li>
            </ul>
          </li>

          <!-- Report -->
<!--             <?php
              $application_type = ['report_leave','report_checkin','report_late'];
            ?>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link <?php in_array($sidebar, $report_type) ? 'active' : ''; ?>">
              <i class="nav-icon far fa-file-alt"></i>
              <p>
                Report
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="report_leave.php" class="nav-link <?php echo ($sidebar == 'report_leave') ? 'active' : ''; ?>">
                  <i class="far fa-circle nav-icon text-warning"></i>
                  <p>Leave Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="report_checkin.php" class="nav-link <?php echo ($sidebar == 'report_checkin') ? 'active' : ''; ?>">
                  <i class="far fa-circle nav-icon text-warning"></i>
                  <p>Check in Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="report_late.php" class="nav-link <?php echo ($sidebar == 'report_late') ? 'active' : ''; ?>">
                  <i class="far fa-circle nav-icon text-warning"></i>
                  <p>Late Report</p>
                </a>
              </li>
            </ul>
          </li> -->

          <!-- UUID -->
          <li class="nav-item">
            <a href="uuid_management.php" class="nav-link <?php ($sidebar == 'uuid') ? 'active' : ''; ?>">
              <i class="nav-icon fas fa-code"></i>
              <p>UUID</p>
            </a>
          </li>

          <!-- Staff Schedule -->
          <li class="nav-item">
            <a href="staff_schedule.php" class="nav-link <?php ($sidebar == 'staff_schedule') ? 'active' : ''; ?>">
              <i class="nav-icon fas fa-calendar"></i>
              <p>Staff Schedule</p>
            </a>
          </li>

          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>