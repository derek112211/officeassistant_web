<?php
$__url = "login.php";
// show redirecting msg if from other pages
if(isset($_GET['redirect']) && (int)$_GET['redirect'] === 1){
	?>
	<html>
		<head>
			<title>Redirecting</title>
		</head>
		<body>
			<script type="text/javascript">
				window.location.href = '<?php echo $__url; ?>';
			</script>
			<span style="display: none;">Redirecting to ...</span>
		</body>
	</html>
	<?php
}else{
	header("Location: $__url");
	exit;
}
?>