<?php
  require_once("top.php");
  require_once("header.php");
  require_once("page_check.php");

  $debug = false;

  $page_title = "Permission Management";
  $sidebar = "permission_management";

  // init table header
  $table_header = [];
  $table_header[] = __LANG_RES_PERMISSION_MANAGEMENT_APPROVER_NAME;
  $table_header[] = __LANG_RES_PERMISSION_MANAGEMENT_APPROVER_STAFF;
  $table_header[] = __LANG_RES_PERMISSION_MANAGEMENT_APPROVER_TITLE;
  $table_header[] = __LANG_RES_PERMISSION_MANAGEMENT_ACTION;


  // handle add uuid
  if(isset($_POST['action']) && $_POST['action'] == 'add'){
    $approver_id = $_POST['approver_id'];
    $approve_staff = $_POST['approver_staff'];
    $job_title = $_POST['job_title'];

    $sql_insert = "INSERT INTO leave_application_approver_staff (
                          user_staff_id,
                          leave_application_approver_type_id,
                          leave_application_approver_staff_id) 
                        VALUES ('$approve_staff','$job_title','$approver_id')
                        ";
    $rs_insert = mysqli_query($db_conn,$sql_insert);
    if($rs_insert){
      $alert['style'] = 'success';
      $alert['msg'] = 'INSERT SUCCESS';
    }else{
      $alert['style'] = 'fail';
      $alert['msg'] = 'INSERT FAILED';
    }
  }  

  // edit uuid
  if(isset($_POST['action']) && $_POST['action'] == 'edit'){
    $permission_id = $_POST['permission_id'];

    $approver_id = $_POST['approver_id'];
    $approve_staff = $_POST['approver_staff'];
    $job_title = $_POST['job_title'];

    $sql_update = "UPDATE leave_application_approver_staff SET
                          user_staff_id = $approve_staff,
                          leave_application_approver_type_id = $job_title,
                          leave_application_approver_staff_id = $approver_id
                        WHERE id = $permission_id
                        ";
    $rs_update = mysqli_query($db_conn,$sql_update);
    if($rs_update){
      $alert['style'] = 'success';
      $alert['msg'] = 'EDIT SUCCESS';
    }else{
      $alert['style'] = 'fail';
      $alert['msg'] = 'EDIT FAILED';
    }
  }


  if(isset($_POST['action']) && $_POST['action'] == 'delete'){
    $permission_id = $_POST['permission_id'];

    $sql_delete = "DELETE FROM leave_application_approver_staff WHERE id = $permission_id";
    $rs_delete = mysqli_query($db_conn,$sql_delete) or die ("$sql_delete :".mysqli_error($db_conn));
    if($rs_delete){
      $alert['style'] = 'success';
      $alert['msg'] = 'DELETE SUCCESS';
    }else{
      $alert['style'] = 'fail';
      $alert['msg'] = 'DELETE FAILED';
    }
  }

  // retrieve data from db
  $approvements = [];
  $sql_approvement = "SELECT aas.*,aat.name_chi AS job_title 
                FROM leave_application_approver_staff AS aas
                LEFT JOIN leave_application_approver_type AS aat ON aat.id = aas.leave_application_approver_type_id


                ";
  $rs_approvement = mysqli_query($db_conn,$sql_approvement) or die ("$sql_approvement :".mysqli_error($db_conn));
  while($row_approvement = mysqli_fetch_assoc($rs_approvement)){
    $approvements[$row_approvement['id']] = $row_approvement;
  } 

  // get staff
  $staffs = [];
  $sql_staff = "SELECT * FROM user_staff";
  $rs_staff = mysqli_query($db_conn,$sql_staff) or die ("$sql_staff :".mysqli_error($db_conn));
  while($row_staff = mysqli_fetch_assoc($rs_staff)){
    $staffs[$row_staff['id']] = $row_staff;
  } 

  // get permission list
  $permissions = [];
  $sql_permission = "SELECT * FROM leave_application_approver_type";
  $rs_permission = mysqli_query($db_conn,$sql_permission) or die ("$sql_permission :".mysqli_error($db_conn));
  while($row_permission = mysqli_fetch_assoc($rs_permission)){
    $permissions[$row_permission['id']] = $row_permission;
  } 

  // retrieve data from db - end  

  if($debug){
    echo '<br>$approvements</br>';
    print_r($approvements);
  }




?>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">



<?php 

require_once('nav.php');
require_once('sidebar.php'); 

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $page_title ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="homePage.php">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $page_title ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SHOP UUID TABLE -->
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-lg-11 col-6"><h3 class="card-title">Permission Management</h3></div>          
              </div>
            </div>

            <!-- /.card-header -->
            <div class="card-body">

              <?php 

              if(isset($alert)){ 
                if($alert['style'] == 'success'){
                  echo '<div class="alert alert-success alert-dismissible">';
                  echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                  echo '<h5><i class="icon fas fa-check"></i> Success!</h5>';
                  echo $alert['msg'].'</div>';
                }else{
                  echo '<div class="alert alert-danger alert-dismissible">';
                  echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                  echo '<h5><i class="icon fas fa-check"></i> FAILED!</h5>';
                  echo $alert['msg'].'</div>';
                }
              }

              ?>

                <table id="shop_uuid_table" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <?php
                      foreach ($table_header as $header) {
                        echo "<th>".$header."</th>";
                      }
                    ?>
                  </tr>
                  </thead>
                  <tbody>
                    <!-- table body -->

                    <!-- Add Row -->
                    <tr>
                      <td>
                        <div class="form-group">
                          <select class="form-control" id="add_approver">
                            <?php
                              foreach ($staffs as $staff_id => $value) {
                                echo '<option value="'.$staff_id.'">'.$value['staff_number'].' '.$value['full_name'].'</option>';
                              }
                            ?>
                          </select>
                        </div>
                      </td>
                      <td>
                        <div class="form-group">
                          <select class="form-control" id="add_approver_staff">
                            <?php
                              foreach ($staffs as $staff_id => $value) {
                                echo '<option value="'.$staff_id.'">'.$value['staff_number'].' '.$value['full_name'].'</option>';
                              }
                            ?>
                          </select>
                        </div>
                      </td>
                      <td>
                        <div class="form-group">
                          <select class="form-control" id="add_permission">
                            <?php
                              foreach ($permissions as $permission_id => $value) {
                                echo '<option value="'.$permission_id.'">'.$value['name_chi'].'</option>';
                              }
                            ?>
                          </select>
                        </div>
                      </td>
                      <td>
                        <div class="col-sm-12 col-lg-12 col-xl-12">
                          <button type="button" class="btn btn-block btn-success" onclick="check_add($(this))">Add</button>
                        </div>
                      </td>
                    </tr>

                    <?php
                      foreach ($approvements as $approvement) {
                        echo '<tr data-id="'.$approvement['id'].'">';

                        // approver name
                        // (for view)
                        echo '<td name="display_approver_staff">'.$staffs[$approvement['leave_application_approver_staff_id']]['staff_number'].' '.$staffs[$approvement['leave_application_approver_staff_id']]['full_name'].'</td>';
                        // (for input)
                        echo '<td style="display:none;">';
                        echo '<select class="form-control" name="edit_approver_staff">';
                          foreach ($staffs as $staff_id => $value) {
                            echo '<option value="'.$staff_id.'" '.formOption_isSelected($approvement['leave_application_approver_staff_id'],$staff_id).'>'.$value['staff_number'].' '.$value['full_name'].'</option>';
                          }
                        echo '</select></td>';

                        // approver staff
                        // (for view)
                        echo '<td name="display_approver">'.$staffs[$approvement['user_staff_id']]['staff_number'].' '.$staffs[$approvement['user_staff_id']]['full_name'].'</td>';
                        // (for input)
                        echo '<td style="display:none;" >';
                        echo '<select class="form-control" name="edit_approver">';
                          foreach ($staffs as $staff_id => $value) {
                            echo '<option value="'.$staff_id.'" '.formOption_isSelected($approvement['user_staff_id'],$staff_id).'>'.$value['staff_number'].' '.$value['full_name'].'</option>';
                          }
                        echo '</select></td>';

                        // job title
                        // (for view)
                        echo '<td name="display_job_title">'.$approvement['job_title'].'</td>';
                        // (for input)
                        echo '<td style="display:none;"><select class="form-control" name="edit_job_title">';
                          foreach ($permissions as $permission_id => $value) {
                            echo '<option value="'.$permission_id.'" '.formOption_isSelected($approvement['leave_application_approver_type_id'],$permission_id).'>'.$value['name_chi'].'</option>';
                          }
                        echo '</select></td>';

                        echo '<td>';
                        echo '<div class="row">';

                          echo '<div class="col-sm-6 col-lg-6 col-xl-6">';
                            echo '<button type="button" class="btn btn-block btn-primary" onclick="edit_row($(this));">';
                              echo __LANG_RES_PERMISSION_MANAGEMENT_ACTION_EDIT;
                            echo '</button>';
                          echo '</div>';

                          echo '<div class="col-sm-6 col-lg-6 col-xl-6"><button type="button" class="btn btn-block btn-danger" onclick="delete_row($(this));">'.__LANG_RES_PERMISSION_MANAGEMENT_ACTION_DELETE.'</button></div>';

                        echo '</div>';
                        echo '</td>';
                        // echo '<input type="hidden" id="type" value="submit">';
                        echo '</tr>';
                      }
                    ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <!-- table footer -->
                  </tr>
                  </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
          </div>

    </section>
    <!-- /.content -->
  </div>

<?php
  // load footer
  require_once("footer.php");

  // close resources, without global resources
  require_once("bottom.php");
?>
<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#staff_uuid_table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });    
    $('#shop_uuid_table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
});

  function edit_row(element){
    var row_id = $(element).parents('tr').attr('data-id');
    var row = $(element).parents('tr');

    // change view to input

    row.find('[name=edit_approver]').parent('td').attr('style','');
    row.find('[name=edit_approver_staff]').parent('td').attr('style','');
    row.find('[name=edit_job_title]').parent('td').attr('style','');

    row.find('[name=display_approver]').attr('style','display:none;');
    row.find('[name=display_approver_staff]').attr('style','display:none;');
    row.find('[name=display_job_title]').attr('style','display:none;');

    // $('#type').val('edit');

    // change btn 
    // success
    $(element).html('<?php echo __LANG_RES_PERMISSION_MANAGEMENT_ACTION_SUBMIT; ?>');
    $(element).removeClass("btn-primary");
    $(element).addClass("btn-success");
    $(element).attr('onclick',"submit_edit_row($(this))");

    // cancel btn
    var cancel_btn = $(element).parent().siblings().find('button');
    cancel_btn.attr('onclick',"cancel_edit_row($(this))");
    cancel_btn.html('<?php echo __LANG_RES_PERMISSION_MANAGEMENT_ACTION_CANCEL; ?>');



  }

  function cancel_edit_row(element,type){
    
    var row = $(element).parents('tr');
    var submit_btn = $(element).parent().siblings().find('button');

    row.find('[name=edit_approver]').parent('td').attr('style','display:none;');
    row.find('[name=edit_approver_staff]').parent('td').attr('style','display:none;');
    row.find('[name=edit_job_title]').parent('td').attr('style','display:none;');

    row.find('[name=display_approver]').attr('style','');
    row.find('[name=display_approver_staff]').attr('style','');
    row.find('[name=display_job_title]').attr('style','');

    // change btn status
    $(element).html('<?php echo __LANG_RES_PERMISSION_MANAGEMENT_ACTION_DELETE; ?>');
    $(element).attr('onclick',"delete_row($(this))");
    submit_btn.attr('onclick',"edit_row($(this))");
    submit_btn.removeClass('btn-success');
    submit_btn.addClass('btn-primary');
    submit_btn.html('<?php echo __LANG_RES_PERMISSION_MANAGEMENT_ACTION_EDIT; ?>');

  }


  function submit_edit_row(element){
    // var cancel_btn = $(element).parent().siblings().find('button');
    var row = $(element).parents('tr');

    var permission_id = row.attr('data-id');
    var approver_id = row.find('[name=edit_approver]').val();
    var approver_staff = row.find('[name=edit_approver_staff]').val();
    var job_title = row.find('[name=edit_job_title]').val();


    var form = document.createElement("form");
    var action = document.createElement("input");
    var element1 = document.createElement("input");
    var element2 = document.createElement("input");
    var element3 = document.createElement("input");
    var element4 = document.createElement("input");

    form.method = "POST";
    form.action = "permission_management.php";

    element1.value = permission_id;
    element1.name = "permission_id";
    element1.style.cssText = "display: none;";
    form.appendChild(element1);

    element2.value = approver_id;
    element2.name = "approver_id";
    element2.style.cssText = "display: none;";
    form.appendChild(element2);

    element3.value = approver_staff;
    element3.name = "approver_staff";
    element3.style.cssText = "display: none;";
    form.appendChild(element3);

    element4.value = job_title;
    element4.name = "job_title";
    element4.style.cssText = "display: none;";
    form.appendChild(element4);


    action.value = 'edit';
    action.name = "action";
    action.style.cssText = "display: none;";
    form.appendChild(action);
    
    document.body.appendChild(form);

    form.submit();
    form.remove();
  }

  function check_add(element){

    var row = $(element).parents('tr');

    var approver_id = row.find('#add_approver').val();
    var approver_staff = row.find('#add_approver_staff').val();
    var job_title = row.find('#add_permission').val();


    var form = document.createElement("form");
    var action = document.createElement("input");
    var element2 = document.createElement("input");
    var element3 = document.createElement("input");
    var element4 = document.createElement("input");

    form.method = "POST";
    form.action = "permission_management.php";


    element2.value = approver_id;
    element2.name = "approver_id";
    element2.style.cssText = "display: none;";
    form.appendChild(element2);

    element3.value = approver_staff;
    element3.name = "approver_staff";
    element3.style.cssText = "display: none;";
    form.appendChild(element3);

    element4.value = job_title;
    element4.name = "job_title";
    element4.style.cssText = "display: none;";
    form.appendChild(element4);


    action.value = 'add';
    action.name = "action";
    action.style.cssText = "display: none;";
    form.appendChild(action);
    
    document.body.appendChild(form);

    form.submit();
    form.remove();
  }

  function delete_row(element){
    bootbox.confirm('<?php echo __LANG_RES_PERMISSION_MANAGEMENT_CONFIRM_DELETE_STAFF; ?>',function(result){
        if(result){
            var permission_id = $(element).parents('tr').attr('data-id');

            var form = document.createElement("form");
            var element1 = document.createElement("input");
            var action = document.createElement("input");

            form.method = "POST";
            form.action = "permission_management.php";

            element1.value = permission_id;
            element1.name = "permission_id";
            element1.style.cssText = "display: none;";
            form.appendChild(element1);

            action.value = 'delete';
            action.name = "action";
            action.style.cssText = "display: none;";
            form.appendChild(action);

            document.body.appendChild(form);

            form.submit();
            form.remove();
        }else{
          return;
        }
    }); 

  }
</script>