<?php

require_once("top.php");
header('Content-Type: application/json; charset=utf-8');

$debug = false;

$result = [];

if($debug){
	print_r($_POST);
	echo json_encode($result);
	return;
}

$range_str = $_POST['range'];
$staff_schedule_item_id = $_POST['schedule_item_id'];
$staff_id = $_POST['staff_id'];
$shop_id = $_POST['shop_id'];

// handle date range
$temp = explode(" ", $range_str); // e,g. 2020-04-25 12:00 - 2020-04-29 11:00

$start_date = $temp[0];
$end_date = $temp[2];
// $start_time = $temp[1].':00';
// $end_time = $temp[4].':00';


$temp_date = $start_date;

// insert process
mysqli_query($db_conn, "START TRANSACTION");
$error = false;

while($temp_date <= $end_date){
	// check date is inserted?
	$sql_check = "SELECT leave_id FROM staff_schedule WHERE working_date = '$temp_date' AND user_staff_id = $staff_id";
	$rs_check = mysqli_query($db_conn,$sql_check) or die ("$sql_check :".mysqli_error($db_conn));
	if(mysqli_num_rows($rs_check) > 0){
		$sql_update = "UPDATE staff_schedule 
						SET staff_schedule_item_id = $staff_schedule_item_id,
						 	shop_id = $shop_id 
					   WHERE working_date = '$temp_date'
					   AND user_staff_id = $staff_id
					   ";
		// echo $sql_update.'<br>';
		$rs_update = mysqli_query($db_conn,$sql_update) or die ("$sql_update :".mysqli_error($db_conn));
		if(!$rs_update){
			mysqli_query($db_conn, "ROLL BACK");
			$result['status'] = false;
			$result['msg'] = "UPDATE schedule failed";
			echo json_encode($result);
			return;
		}

		// update leave
		// $leave_ids = [];
		// while($row_check = mysqli_fetch_assoc($rs_check)){
		// 	if(in_array($row_check['leave_id'], $leave_ids)){
		// 		$leave_ids[] = $row_check['leave_id'];
		// 	}
		// }

		// foreach ($leave_ids as $value) {
		// 	$sql_update_leave = "UPDATE staff_leave"
		// }
	}else{
		$sql_insert = "INSERT INTO staff_schedule (shop_id,user_staff_id,staff_schedule_item_id,working_date) VALUES ('$shop_id','$staff_id','$staff_schedule_item_id','$temp_date')";
		// echo $sql_insert.'<br>';
		$rs_insert = mysqli_query($db_conn,$sql_insert) or die ("$sql_insert :".mysqli_error($db_conn));
		if(!$rs_insert){
			mysqli_query($db_conn, "ROLL BACK");
			$result['status'] = false;
			$result['msg'] = "INSERT schedule failed";
			echo json_encode($result);
			return;
		}
	}


	$temp_date = date('Y-m-d',strtotime($temp_date . "+1 days"));
}



mysqli_query($db_conn, "COMMIT");
$result['status'] = true;
$result['msg'] = "create schedule success";
echo json_encode($result);


require_once("bottom.php");
?>