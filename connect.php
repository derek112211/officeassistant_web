<?php

$debug = false;
$db_conn;

if($debug){
	echo 'CONNECT WITH DB_SERVER = '.DB_SERVER.'<br>';
	echo 'CONNECT WITH DB_USERNAME = '.DB_USERNAME.'<br>';
	echo 'CONNECT WITH DB_PASSWORD = '.DB_PASSWORD.'<br>';
	echo 'CONNECT WITH DB_DATABASE = '.DB_DATABASE.'<br>';
	echo 'CONNECT WITH DB_PORT = '.DB_PORT.'<br>';
}

$db_conn = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT) or die ('Can not connect the MySQL server. ' . mysqli_error());

mysqli_query($db_conn, "SET NAMES utf8");
mysqli_query($db_conn, "SET CHARACTER_SET_CLIENT=utf8");
mysqli_query($db_conn, "SET CHARACTER_SET_RESULTS=utf8");

// require_once('bottom.php');
?>