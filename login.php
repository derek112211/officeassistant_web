<?php
  require_once("top.php");
  require_once("header.php");

// handle response
  if(isLogin()){
    redirectTo('homePage.php');
  }

// handle login check
if(isset($_POST['user_name'])&&isset($_POST['user_password'])){
  $user_name = $_POST['user_name'];
  $user_password = $_POST['user_password'];
  $user_password = md5($user_password);
  
  $user_name = mysqli_real_escape_string($db_conn, $user_name);

  $sql_login = "SELECT * FROM user_staff WHERE user_name ='$user_name' AND password = '$user_password' AND is_admin = 1 LIMIT 1";
  $rs_login = mysqli_query($db_conn, $sql_login) or die("$sql_login ".mysqli_error($db_conn));
  $user_info = mysqli_fetch_assoc($rs_login);
  if(mysqli_num_rows($rs_login) > 0){
    // login success
    print_r($user_info);
    $_SESSION['user_id'] = $user_info['id'];
    redirectTo("index.php");
    debug_to_console("Login Success");
  }else{
    // login failed
    $values['login_error_msg'] = "Login Failure";
  }
}
?>
<body class="hold-transition login-page">
  <div class="login-logo">
    <b>Smart HR System</b>
  </div>
<div class="login-box">

  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">System Login</p>

      <form action="login.php" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="User Name..." name="user_name">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password..." name="user_password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">Login</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

    </div>
    <!-- /.login-card-body -->
  </div>
  <?php 
  if(isset($values['login_error_msg'])){
    print(" <div class=\"alert alert-danger\" style=\"text-align:center;\">
          <span class=\"glyphicon glyphicon-warning-sign\"></span>
          ".$values['login_error_msg']."
        </div>");
  }
  ?>
</div>
<!-- /.login-box -->
</body>
</html>
<?php
  // close resources, without global resources
  require_once("bottom.php");
?>
