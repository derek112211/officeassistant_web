<?php

function genstr($length){
	$random = '';
	srand((double)microtime() * 1000000);
	$char_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$char_list .= "abcdefghijklmnopqrstuvwxyz";
	$char_list .= "1234567890";
	for($i = 0; $i < $length; ++$i){
		$random .= substr($char_list,(rand()%(strlen($char_list))), 1);
	}
	return $random;
}

function api_utils_issetAndNotEqual($val){
    return isset($val) && $val != "";
}

function getDateTimeNow(){
    return date("Y-m-d H:i:s");
}

/**
 * Staff login function || find staff with member_phone
 * @param type $request 
 * [
 *  member_phone => <number>
 *  login_type => <nubmer> //default 1 for staff
 *  device_code => <String>
 * ]
 * 
 * @return type
 */

function api_staff_login($request) {
    $checking = function($req) {
        if (!isset($req['member_phone'])) {
            throw new Exception("Phone number is required");
        }
        if (!isset($req['domain'])) {
            throw new Exception("Domain Name is required");
        }
        if(!isset($req['login_type'])){
            throw new Exception("Login type is required");
        }
        if($req['login_type'] != 1){
            throw new Exception("Login type is not staff, please check");
        }
        if(!isset($req['device_code'])){
            throw new Exception("No device code get");
        }
        if(!isset($req['password'])){
            throw new Exception("No password get");
        }
        // if(!isset($req['ip_address'])){
        //     throw new Exception("No ip address get");
        // }
        // if(!isset($req['domain'])){
        //     throw new Exception("No domain name get");
        // }


        return true;
    };
    $main = function($request) {
        global $db_conn;
        $debug = true;
        $res = [];

        if($request['domain'] != '127.0.0.1'){
            throw new Exception("Domain Name is invaild.");
        }

        $password = md5($request['password']);

        $sql_find_staff = "SELECT user_staff.*, 
                                  shop.name AS shop_name,
                                  shop.phone AS shop_phone 
                           FROM user_staff, shop "
                . "WHERE user_staff.home_shop_id = shop.id ";

        if(isset ($request['member_phone']) && $request['member_phone'] != "") {
            $sql_find_staff .= " AND (user_staff.phone = '{$request['member_phone']}')";
            $sql_find_staff .= " AND (user_staff.password = '$password') ";
        }
        $rs_find_staff = mysqli_query($db_conn, $sql_find_staff);
        if (!$rs_find_staff) {
            if (isset($request["debug"]))
                throw new Exception($sql_find_staff . ":" . mysqli_error($db_conn));
            else
                throw new Exception("Server Error");
        }

        if (mysqli_num_rows($rs_find_staff) == 0) {
        	if($debug){
        		throw new Exception("No staff is found. Please Check Your Password OR Phone number.");
        	}
            throw new Exception("No staff is found. Please Check Your Password OR Phone number.");
        }

        $staff_profiles = mysqli_fetch_all($rs_find_staff,MYSQLI_ASSOC);
        $res['params'] = $staff_profiles[0];

        if($staff_profiles[0]['device_token'] != $request['device_code'] && $staff_profiles[0]['device_token'] != ''){
            throw new Exception("UUID not match. Please contact Office.");
        }

        // $rand_token = genstr(32);

        //update user unique device token AND add random login token
        $sql_device_code = "UPDATE user_staff SET device_token = ('{$request['device_code']}') WHERE (user_staff.phone = '{$request['member_phone']}')";

        $rs_device_code = mysqli_query($db_conn,$sql_device_code);
        if (!$rs_device_code) {
            if (isset($request["debug"]))
                throw new Exception($rs_device_code . ":" . mysqli_error($db_conn));
            else
                throw new Exception("Server Error");
        }
        $res['params']['device_code'] = $request['device_code'];


        // $res['params']['login_token'] = $rand_token;
        return $res['params'];
    };
    $checking($request);
    $res = $main($request);
    return $res;
}

/**
 * Api device check function || check the same device using as login device
 * @author derek
 * @param type $request 
 * [
 *  member_phone => <number> // shop_id => <number>
 *  device_code  => <string>
 * ]
 * 
 * @return type
 */
function api_check_device($request) {
    $checking = function($req) {
        if (!isset($req['member_phone']) && !isset($req['shop_id'])) {
            throw new Exception("Phone Number OR Shop Id is required");
        }
        if (!isset($req['device_code'])) {
            throw new Exception("Device code is required");
        }
        // if (!isset($req['login_type']) && $req['login_type'] != 0) {
        //     throw new Exception("Login type is required or not equal to normal user");
        // }

        return true;
    };
    $main = function($request) {
        global $db_conn;
        $res = [];

        if(isset($request['member_phone'])){
            $sql_check_device = "SELECT id FROM user_staff WHERE phone = '{$request['member_phone']}' AND device_token = '{$request['device_code']}'";
        }else if(isset($request['shop_id'])){
            $sql_check_device = "SELECT s.id FROM shop AS s LEFT JOIN shop_uuid AS tsu ON s.id = tsu.shop_id WHERE tsu.device_token = '{$request['device_code']}' AND s.id = '{$request['shop_id']}'";
        }

        $rs_check_device = mysqli_query($db_conn,$sql_check_device);
        if (!$rs_check_device) {
            if (isset($request["debug"]))
                throw new Exception($sql_check_device . ":" . mysqli_error($db_conn));
            else
                throw new Exception($sql_check_device);
        }

        if (mysqli_num_rows($rs_check_device) == 0) {
            throw new Exception("No Device Found");
        }
        return true;
    };
    $checking($request);
    $main($request);
    return true;
}


/**
 * LOGOUT
 * @author derek
 * @param type $request
 * [
 *  id => <string>
 * ]
 * 
 * @param type $type [staff/host]
 * @return type
 *  
 */
function api_logout($request,$type){
    $checking = function($req) {
        if (!isset($req['id'])) {
            throw new Exception("id is missing");
        }   
        if (!isset($req['logout_type'])) {
            throw new Exception("logout type is missing");
        }   

        return true;
    };
    $main = function($request) {
        global $db_conn;
        $res = [];
        $id = $request['id'];
        $type = $request['logout_type'];

        switch ($type) {
            case 'staff':
                $sql_search = "SELECT id FROM user_staff WHERE id = $id";
        
                $rs_searh = mysqli_query($db_conn,$sql_search) or die("$sql_search ".mysqli_error($db_conn));

                if(mysqli_num_rows($rs_searh) == 0){
                    throw new Exception("Not Staff Found");
                }
                break;
            case 'host':
                $sql_search = "SELECT id FROM shop WHERE id = $id";
        
                $rs_searh = mysqli_query($db_conn,$sql_search) or die("$sql_search ".mysqli_error($db_conn));

                if(mysqli_num_rows($rs_searh) == 0){
                    throw new Exception("Not Shop Found");
                }
                break;
            default:
                throw new Exception("Logut type Error");
                break;
        }
        
        return $res;
    };

    $checking($request);
    $res = $main($request);
    return $res;
}

/**
 * Get Shop
 * @author derek
 * @param type $request
 * [
 *  action => <string> list / list_staff [default : list]
 *  shop_id => <string> only need if list_staff action
 * ]
 * 
 * @return type
 *  
 */
function api_get_shop($request){
    $checking = function($req) {
        if ($req['action'] == 'list_staff') {
            if (!isset($req['shop_id'])) {
                throw new Exception("shop_id is missing");
            }   
        } 
        return true;
    };

    $main = function($request) {
        global $db_conn;
        $res = [];
        
        switch ($request['action']) {
            case 'list_shop':
                $sql_shop = "SELECT * FROM shop";
                $rs_shop = mysqli_query($db_conn,$sql_shop) or die("$sql_shop ".mysqli_error($db_conn));
                while($row_shop = mysqli_fetch_assoc($rs_shop)){
                    $res[] = $row_shop;
                }
                return $res;
            case 'list_staff':
                $sql_shop_staff = "SELECT u.*,s.name AS shop_name, s.phone AS shop_phone
                                    FROM user_staff AS u, shop AS s
                                    WHERE u.home_shop_id = s.id 
                                            AND home_shop_id = {$request['shop_id']}
                                            AND u.leave_date = '0000-00-00'
                                    ORDER BY staff_number DESC";
                $rs_shop_staff = mysqli_query($db_conn,$sql_shop_staff) or die("$sql_shop_staff ".mysqli_error($db_conn));
                while($row_shop_staff = mysqli_fetch_assoc($rs_shop_staff)){
                    $res[] = $row_shop_staff;
                }
                return $res;
            default:
                # code...
                break;
        }

    };
    $checking($request);
    $res = $main($request);
    return $res;
}


/**
 * Api apply leave function || update into both 'leave_application_form' && 'leave_application_date' table
 * @author derek
 * @param type $request 
 * [
 *  leave_from => <string> : date format [yyyy-MM-dd HH:mm:ss]
 *  leave_to  => <string> : date format [yyyy-MM-dd HH:mm:ss]
 *  leave_type => <string> : leave id
 *  leave_remarks => <string> : leave reason
 *  staff_id => <string>
 *  shop_id => <string>
 * ]
 * 
 * @return type
 *  appliaction_id => <string>
 */
function api_applyLeave($request){
    $checking = function($req) {
        if (!isset($req['leave_from'])) {
            throw new Exception("leave_from is missing");
        }        
        if (!isset($req['leave_to'])) {
            throw new Exception("leave_to is missing");
        }        
        if (!isset($req['leave_type'])) {
            throw new Exception("leave_type is missing");
        }        
        if (!isset($req['leave_remarks'])) {
            throw new Exception("leave_remarks is missing");
        }
        return true;
    };
    $main = function($request) {
        global $db_conn;
        $res = [];

        $leave_from = $request['leave_from'];
        $leave_to = $request['leave_to'];
        $leave_type = $request['leave_type'];
        $leave_remarks = $request['leave_remarks'];
        $staff_id = $request['staff_id'];
        $shop_id = $request['shop_id'];

        $sql_insert_leave_application_form = "INSERT INTO leave_application_form 
                                                    (application_no, user_staff_id, shop_id, reason, create_time)
                                               SELECT IFNULL(MAX(application_no), 0)+1, '$staff_id', '$shop_id', '$leave_remarks', '".getDateTimeNow()."' FROM leave_application_form ";

        $rs = mysqli_query($db_conn,$sql_insert_leave_application_form);
        if (!$rs) {
            if (isset($request["debug"]))
                throw new Exception($sql_insert_leave_application_form . ":" . mysqli_error($db_conn));
            else
                throw new Exception("Insert Into leave_application_form FAILED");
        }

        //get id
        $sql_get_current_form_id = "SELECT MAX(id) AS id FROM leave_application_form WHERE user_staff_id = $staff_id AND shop_id = $shop_id";
        $rs_get_current_form_id = mysqli_query($db_conn, $sql_get_current_form_id);
        if (!$rs_get_current_form_id) {
            if (isset($request["debug"]))
                throw new Exception($sql_get_current_form_id . ":" . mysqli_error($db_conn));
            else
                throw new Exception("Server Error - Get current form id FAILED");
        }

        if (mysqli_num_rows($rs_get_current_form_id) == 0) {
            throw new Exception("No Record found.");
        }
        
        $row = mysqli_fetch_assoc($rs_get_current_form_id);
        $appliaction_id = $row['id'];
        $res['appliaction_id'] = $appliaction_id;

        //prograss date string - format: yyyy-MM-dd H:i:s +0000
        $date_from = explode(" ", $leave_from);
        $date_to = explode(" ", $leave_to);

        $date_diff = (strtotime($date_to[0]." ".$date_to[1]) - strtotime($date_from[0]." ".$date_from[1]))/(60*60*24);
        $num_of_days = ceil($date_diff*4)/4; //up to nearest .0/.25/.50/.75
        $time_diff = (strtotime($date_to[1]) - strtotime($date_from[1]))/(60*60);

        $res['leave_from'] = $leave_from;
        $res['leave_to'] = $leave_to;
        $res['leave_type'] = $leave_type;
        $res['leave_remarks'] = $leave_remarks;
        $res['staff_id'] = $staff_id;
        $res['shop_id'] = $shop_id;
        $res['num_of_days'] = $num_of_days;
        $res['time_diff'] = $time_diff;

        //insert leave_application_date
        $sql_insert_leave_application_date = "INSERT INTO leave_application_date
                                                (leave_application_form_id, leave_date, start_time,end_date, end_time, leave_type, num_of_days, num_of_hours)
                                                VALUES ('$appliaction_id','{$date_from[0]}','{$date_from[1]}','{$date_to[0]}','{$date_to[1]}','$leave_type',$num_of_days,$time_diff)";
        $rs = mysqli_query($db_conn,$sql_insert_leave_application_date);
        if (!$rs) {
            if (isset($request["debug"]))
                throw new Exception($sql_insert_leave_application_date . ":" . mysqli_error($db_conn));
            else
                throw new Exception("Insert Into sql_insert_leave_application_date FAILED");
        }
        return $res;
    };
    $checking($request);
    $res = $main($request);
    return $res;
}


/**
 * chcek leave approver permission
 * @author derek
 * @param type $request 
 * [
 *  staff_id => <string>
 *  shop_id => <string>
 * ]
 * 
 * @return type
 *  position => <string> : staff || department_approver || hr_approver
 */
function api_checkLeavePermission($request){
    // $checking = function($req) {
    //     return true;
    // };
    $main = function($request) {
        global $db_conn;
        $res = [];

        // get approver type
        $approver_type = [];
        $sql_approver_type = "SELECT * FROM leave_application_approver_type";
        $rs_approver_type = mysqli_query($db_conn, $sql_approver_type) or die("$sql_approver_type ".mysqli_error($db_conn));
        while($row_approver_type = mysqli_fetch_assoc($rs_approver_type)){
            $approver_type[$row_approver_type['id']] = $row_approver_type['label'];
        }

        $staff_id = $request['staff_id'];
        $shop_id = $request['shop_id'];

        $sql_check_leave_permission = "SELECT *
                                        FROM leave_application_approver_staff AS ap
                                        LEFT JOIN leave_application_approver_type AS apt ON ap.leave_application_approver_type_id = apt.id";
        $where_array = [];
        $where_array[] = "ap.leave_application_approver_staff_id = $staff_id";
        // $where_array[] = "ap.shop_id = $shop_id";

        $sql_check_leave_permission .= " WHERE ".implode(" AND ", $where_array);

        $sql_check_leave_permission .= " GROUP BY leave_application_approver_type_id";

        $rs_check_leave_permission = mysqli_query($db_conn, $sql_check_leave_permission) or die("$sql_check_leave_permission ".mysqli_error($db_conn));
        if (!$rs_check_leave_permission) {
            if (isset($request["debug"]))
                throw new Exception($rs_check_leave_permission . ":" . mysqli_error($db_conn));
            else
                throw new Exception("Server Error - Check Leave Permission");
        }
        if(mysqli_num_rows($rs_check_leave_permission) == 0){
            $res['position'] = 'staff';
            throw new Exception("[Staff] Do NOT Have Permission");
        }else if(mysqli_num_rows($rs_check_leave_permission) == 1){
            $row_check = mysqli_fetch_assoc($rs_check_leave_permission);
            $res['position'] = $approver_type[$row_check['leave_application_approver_type_id']];
        }else{
            $res['position'] = 'both';
        }
        return $res;
    };
    // $checking($request);
    $res = $main($request);
    return $res;    
}


/**
 * list all pending leave to approve according the pos
 * @author derek
 * @param type $request 
 * [
 *  staff_id => <string>
 *  shop_id => <string>
 *  position => <string> : staff || department_approver || hr_approver
 * ]
 * 
 * @return type
 *  
 */
function api_listLeave($request){
    $checking = function($req) {
        if (!isset($req['position'])) {
            throw new Exception("position is missing");
        }        
        return true;
    };
    $main = function($request) {
        global $db_conn;
        $res = [];

        $staff_id = $request['staff_id'];
        $shop_id = $request['shop_id'];
        $position = $request['position'];


        $sql_list_leave = "SELECT laf.id,
                                  laf.application_no,
                                  laf.user_staff_id,
                                  laf.shop_id,
                                  u.first_name_eng,
                                  u.last_name_eng,
                                  u.short_name,
                                  laf.reason,
                                  laf.create_time,
                                  lad.leave_date AS leave_from,
                                  lad.start_time AS leave_time_from,
                                  lad.end_date AS leave_to,
                                  lad.end_time AS leave_time_to,
                                  lad.leave_type,
                                  lad.num_of_days AS leave_days,
                                  lad.num_of_hours AS leave_hours,
                                  sci.name AS leave_type_name,
                                  s.name AS shop_name,
                                  lad.department_approve_status,
                                  lad.hr_approve_status,
                                  laf.request_approve_status
                                    FROM leave_application_form AS laf
                                    LEFT JOIN user_staff AS u ON u.id = laf.user_staff_id
                                    LEFT JOIN leave_application_date AS lad ON lad.leave_application_form_id = laf.id
                                    LEFT JOIN staff_schedule_item AS sci ON sci.id = lad.leave_type
                                    LEFT JOIN shop AS s ON s.id = u.home_shop_id
                                    ";
        switch ($position) {
            case 'staff':
                //no permission
                throw new Exception("[Staff] No Permission");
                break;
            case 'department_approver':
                $where_array = [];
                $where_array[] = "lad.department_approve_status = -1"; // not processed
                $where_array[] = "laf.request_cancel_application = 0"; // no cancel
                $where_array[] = "laf.shop_id = $shop_id";
                $where_array[] = "lad.department_approve_time LIKE '0000-00-00%'";
                $where_array[] = "laf.user_staff_id IN (
                    SELECT user_staff_id 
                    FROM leave_application_approver_staff 
                    WHERE leave_application_approver_staff_id = '$staff_id' 
                        AND leave_application_approver_type_id = '1'
                )";
                $sql_list_leave .= " WHERE ".implode(" AND ", $where_array);
                break;
            case 'hr_approver':
                $where_array = [];
                $where_array[] = "lad.hr_approve_status = -1"; // not processed
                $where_array[] = "lad.department_approve_status = 1"; // department approved
                $where_array[] = "laf.request_cancel_application = 0"; // no cancel
                $where_array[] = "laf.user_staff_id IN (
                    SELECT user_staff_id 
                    FROM leave_application_approver_staff 
                    WHERE leave_application_approver_staff_id = '$staff_id' 
                        AND leave_application_approver_type_id = '2'
                )";
                $sql_list_leave .= " WHERE ".implode(" AND ", $where_array);
                break;
            case 'both':
                $where_array = [];
                $where_array[] = "laf.request_approve_status = -1";
                $where_array[] = "laf.request_cancel_application = 0"; // no cancel
                $where_array[] = "laf.user_staff_id IN (
                    SELECT user_staff_id 
                    FROM leave_application_approver_staff 
                    WHERE leave_application_approver_staff_id = '$staff_id' 
                        OR leave_application_approver_type_id = '2'
                )";
                $sql_list_leave .= " WHERE ".implode(" AND ", $where_array);

                break;
            default:
                # code...
                break;
        }
        // $where_array[] = "sci.delete_staff_id = 0";
        $rs_list_leave = mysqli_query($db_conn, $sql_list_leave) or die("$sql_list_leave ".mysqli_error($db_conn));

        if (!$rs_list_leave) {
            if (isset($request["debug"])){
                $res['debug'] = $sql_list_leave;
                throw new Exception($rs_list_leave . ":" . mysqli_error($db_conn));
            }
            else
                throw new Exception("Server Error - List Leave (Staff)");
        }
        if(mysqli_num_rows($rs_list_leave) == 0){
            return $res;
        }
        while($row = mysqli_fetch_assoc($rs_list_leave)){
            $res[] = $row;
        }
        return $res;
    };
    $checking($request);
    $res = $main($request);
    return $res;    
}


/**
 * approve OR not approve Leave [HR || Head]
 * @author derek
 * @param type $request 
 * [
 *  application_no => <string>
 *  application_staff_id => <string>
 *  application_shop_id => <string>
 *  approve_status => <string> : true || false
 *  approve_remark => <string>
 *  approve_position => <string> : staff || department_approver || hr_approver
 *  staff_id => <string> : approver's id
 *  shop_id => <string>: approver's shop id
 * ]
 * 
 * @return type
 *  
 */
function api_approveLeave($request){
    $checking = function($req) {

        if (!isset($req['application_id'])) {
            throw new Exception("application_id is missing");
        }               
        if (!isset($req['application_no'])) {
            throw new Exception("application_no is missing");
        }               
        if (!isset($req['application_staff_id'])) {
            throw new Exception("application_staff_id is missing");
        }        
        if (!isset($req['application_shop_id'])) {
            throw new Exception("application_shop_id is missing");
        }
        if (!isset($req['approve_status'])) {
            throw new Exception("approve_status is missing");
        }
        if (!isset($req['approve_position'])) {
            throw new Exception("approve_position is missing");
        }else if($req['approve_position'] == "staff"){
            throw new Exception("Do Not Have permission");
        }
        if (!isset($req['staff_id'])) {
            throw new Exception("staff_id is missing");
        }
        if (!isset($req['shop_id'])) {
            throw new Exception("shop_id is missing");
        }   
        return true;
    };
    $main = function($request) {
        global $db_conn;
        $res = [];

        $application_id = $request['application_id'];
        $application_no = $request['application_no'];
        $application_staff_id = $request['application_staff_id'];
        $application_shop_id = $request['application_shop_id'];
        $approve_status = $request['approve_status'];
        $approve_position = $request['approve_position'];
        $approve_remark = $request['approve_remark'];
        $staff_id = $request['staff_id'];
        $shop_id = $request['shop_id'];
        $date = date("Y-m-d H:i:s");
        $update_action_success = true;

        // update leave_application_date && leave_application_form - start
        if($approve_status == "true"){
            //approve
            switch ($approve_position) {
                case 'department_approver':
                    $sql_approve_leave = "UPDATE leave_application_date SET department_approve_status = 1, department_approve_time = '{$date}', department_approve_staff_id = $staff_id, department_approve_remark = '$approve_remark'";
                    
                    $where_array = [];
                    $where_array[] = "leave_application_form_id = $application_id";
                    $sql_approve_leave .= " WHERE ".implode(" AND ", $where_array);
                    break;
                case 'hr_approver':
                    $sql_approve_leave = "UPDATE leave_application_date SET hr_approve_status = 1, hr_approve_time = '{$date}', hr_approve_staff_id = $staff_id, hr_approve_remark = '$approve_remark'";
                    
                    $where_array = [];
                    $where_array[] = "leave_application_form_id = $application_id";
                    $sql_approve_leave .= " WHERE ".implode(" AND ", $where_array);

                    //leave_application_form
                    $sql = "UPDATE leave_application_form SET request_approve_status = 1, request_approve_time='{$date}', request_approve_staff_id = $staff_id, request_approve_remark = '$approve_remark' WHERE id = $application_id";
                    $rs = mysqli_query($db_conn, $sql) or die("$sql ".mysqli_error($db_conn));
                    if(!$rs){
                        $update_action_success = false;
                        mysqli_query($db_conn, "ROLLBACK");
                        break;
                    }
                    break;
                default:
                    //staff
                    break;
            }
        }else if($approve_status == "false"){
            //not approve
            $update_action_success = false;
            switch ($approve_position) {
                case 'department_approver':
                    //leave_application_date
                    $sql_approve_leave = "UPDATE leave_application_date 
                                            SET department_approve_status = 0,
                                                department_approve_time = '{$date}', 
                                                department_approve_staff_id = $staff_id, 
                                                department_approve_remark = '$approve_remark'";
                    
                    $where_array = [];
                    $where_array[] = "leave_application_form_id = $application_id";
                    $sql_approve_leave .= " WHERE ".implode(" AND ", $where_array);

                    //leave_application_form
                    $sql_cancel_leave = "UPDATE leave_application_form 
                                            SET request_approve_time = '{$date}',
                                                request_approve_remark = '$approve_remark',
                                                request_approve_status = 0 
                                            WHERE id = $application_id";
                    mysqli_query($db_conn, $sql_cancel_leave) or die("$sql_cancel_leave ".mysqli_error($db_conn));


                    break;
                case 'hr_approver':
                    $sql_approve_leave = "UPDATE leave_application_date 
                                            SET hr_approve_status = 0, 
                                                hr_approve_time = '{$date}', 
                                                hr_approve_staff_id = $staff_id, 
                                                hr_approve_remark = '$approve_remark'";
                    
                    $where_array = [];
                    $where_array[] = "leave_application_form_id = $application_id";
                    $sql_approve_leave .= " WHERE ".implode(" AND ", $where_array);

                    //leave_application_form
                    $sql_cancel_leave = "UPDATE leave_application_form 
                                            SET request_approve_time = '{$date}',
                                                request_approve_status = 0,
                                                request_approve_remark = '$approve_remark'
                                         WHERE id = $application_id";
                    mysqli_query($db_conn, $sql_cancel_leave) or die("$sql_cancel_leave ".mysqli_error($db_conn));
                    break;
                default:
                    //staff
                    break;
            }
        
        }
        $rs_approve_leave = mysqli_query($db_conn, $sql_approve_leave) or die("$sql_approve_leave ".mysqli_error($db_conn));

        if (!$rs_approve_leave) {
            if (isset($request["debug"])){
                $res['debug'] = $sql_approve_leave;
                throw new Exception($rs_approve_leave . ":" . mysqli_error($db_conn));
            }
            else{
                $update_action_success = false;
                mysqli_query($db_conn, "ROLLBACK");
                throw new Exception("Server Error - Approve Leave");
            }
        }
        // update leave_application_date && leave_application_form - end

        // update staff_schedule - start
        if($update_action_success){
            $sql_leave_date = "SELECT   lad.id,
                                        laf.user_staff_id, 
                                        laf.shop_id, 
                                        lad.leave_date,
                                        lad.end_date,
                                        lad.start_time, 
                                        lad.end_time, 
                                        lad.leave_type 
                                    FROM leave_application_form AS laf, 
                                        leave_application_date AS lad";
            $where_array = [];
            $where_array[] = "laf.id = lad.leave_application_form_id";
            $where_array[] = "laf.id = '$application_id'";
            $sql_leave_date .= " WHERE ".implode(" AND ", $where_array);
            $rs_leave_date = mysqli_query($db_conn, $sql_leave_date) or die("$sql_leave_date ".mysqli_error($db_conn));
            $approve_id = "";
            $temp_user_staff_id = "";
            $temp_shop_id = "";
            $temp_leave_date = "";
            $temp_end_date = "";
            $temp_start_time = "";
            $temp_end_time = "";
            $temp_leave_type = "";

            $is_append_to_schedule = false;
            if (mysqli_num_rows($rs_leave_date) > 0){
                $row_leave_date = mysqli_fetch_array($rs_leave_date);

                $temp_user_staff_id = $row_leave_date['user_staff_id'];
                $temp_shop_id = $row_leave_date['shop_id'];
                $temp_leave_date = $row_leave_date['leave_date'];
                $temp_end_date = $row_leave_date['end_date'];
                // return  $temp_leave_date.' // '.$temp_end_date;
                // $temp_num_of_days = $row_leave_date['num_of_days'];
                $temp_start_time = $row_leave_date['start_time'];
                $temp_end_time = $row_leave_date['end_time'];
                $temp_leave_type = $row_leave_date['leave_type'];
                $approve_id = $row_leave_date['id'];

                // find staff_schedule
                // update staff_Schedule to leave if any schedule is found
                $target_dates = [];
                $temp_start_date = $temp_leave_date;
                while($temp_start_date <= $temp_end_date){
                    $target_dates[] = "'".$temp_start_date."'";
                    $temp_start_date = date('Y-m-d',strtotime($temp_start_date . "+1 days"));
                }

                $sql_staff_schedule = "SELECT id FROM staff_schedule WHERE user_staff_id = '$temp_user_staff_id' AND working_date IN (".implode(',', $target_dates).")";
                $rs_staff_schedule = mysqli_query($db_conn, $sql_staff_schedule) or die("$sql_staff_schedule ".mysqli_error($db_conn));
                
                
                if (mysqli_num_rows($rs_staff_schedule) > 0)
                {
                    $row_staff_schedule = mysqli_fetch_array($rs_staff_schedule);

                    $temp_staff_schedule_id = $row_staff_schedule['id'];

                    $is_append_to_schedule = true;
                }
            }

            if($approve_position == 'hr_approver'){
                // insert leave schedule - start


                $sql_insert_staff_leave = "INSERT INTO staff_leave 
                                            (staff_schedule_item_id, leave_time_start, leave_time_end, leave_application_date_id) 
                                            VALUES 
                                            ('$temp_leave_type', '$temp_start_time', '$temp_end_time', '$approve_id')";
                $rs_insert_staff_leave = mysqli_query($db_conn, $sql_insert_staff_leave);


                if (!$rs_insert_staff_leave)
                {
                    $res['error'] = true;
                    $res['sql_insert_staff_leave'] = $sql_insert_staff_leave;
                    mysqli_query($db_conn, "ROLLBACK");
                    return $res;
                }

                $new_leave_id = mysqli_insert_id($db_conn);

                $sql_update_schedule = "UPDATE staff_schedule SET leave_id = $new_leave_id WHERE user_staff_id = working_date IN (".implode(',', $target_dates).")";
                $rs_update_staff_leave = mysqli_query($db_conn, $sql_update_schedule);

                if (!$rs_update_staff_leave)
                {
                    $res['error'] = true;
                    $res['sql_insert_staff_leave'] = $sql_update_schedule;
                    mysqli_query($db_conn, "ROLLBACK");
                    return $res;
                }


            }

            // insert leave schedule - end
        }


        // update staff_schedule - end


        if(isset($request['debug']))
            $res['debug'] = $sql_approve_leave;
        $res['approve_position'] = $approve_position;
        $res['approve_status'] = $approve_status;
        $res['approve_time'] = $date;
        return $res;
    };
    $checking($request);
    $res = $main($request);
    return $res;    
}

/**
 * Api get all leaves type
 * @author derek
 * @param type $request 
 * [
 *  member_phone => <number> // shop_id => <number>
 *  device_code  => <string>
 * ]
 * 
 * @return type
 */
function api_getLeaveTypes($request){
    global $db_conn;
    $sql_leave_option = "SELECT * 
                        FROM staff_schedule_item";

    $where_array = [];
    // $where_array[] = "item_label != 'non_new_staff_training'";
    // $where_array[] = "item_label != 'new_staff_training'";
    // $where_array[] = "item_label != 'lunch'";
    $where_array[] = "is_leave = 1";
    $where_array[] = "delete_staff_id = 0";

    $sql_leave_option .= " WHERE ".implode(" AND ", $where_array); 

    $sql_leave_option .= " ORDER BY ordering ASC";

    $rs_leave_option = mysqli_query($db_conn, $sql_leave_option) or die("$sql_leave_option ".mysqli_error($db_conn));
    if (!$rs_leave_option) {
        if (isset($request["debug"]))
            throw new Exception($rs_leave_option . ":" . mysqli_error($db_conn));
        else
            throw new Exception("Server Error");
    }
    if(mysqli_num_rows($rs_leave_option) == 0){
        throw new Exception("No Leave Type exist");
    }

    return mysqli_fetch_all($rs_leave_option,MYSQLI_ASSOC);
}


/**
 * Api upload IMG function
 * @author derek
 * @param type $file 
 * [
 *  error => <0/1>
 *  name => <string>
 *  size => <number>
 *  tmp_name => <string>
 *  type => <string>  
 * ]
 * 
 * @return type
 */
function uploadImg($request,$file){
    global $db_conn;
    $res = [];

    $base_img_path = 'photo/attendance/';
    $staff_number = $request['staff_number'];
    $date = date('Y-m-d_H:m:s');

    if(!$file){
        throw new Exception("No File received");
    }else{
        $file_data = file_get_contents($file['tmp_name']);
    }

    $photoPath = $base_img_path.$staff_number.'_'.$date.'.jpg';

    if($file_data){
        if(!file_put_contents($photoPath,$file_data)){
            throw new Exception("The file could not be created.");
        }
    }else{
        throw new Exception("The file could not be created.");
    }


    $res['img_path'] = $photoPath;
    $res['staff_number'] = $staff_number;
    return $res;
}


/**
 * Api device check function || check the same device using as login device
 * @author derek
 * @param type $request 
 * [
 *  lantitude => <string>
 *  longitude  => <string>
 *  address => <string>
 * ]
 * 
 * @return type
 */
function api_check_in($request){
    $checking = function($req) {
        if (!isset($req['staff_id'])) {
            throw new Exception("Staff id is required");
        } 
        if (!isset($req['type'])) {
            throw new Exception("TYPE is required");
        }
        else if($req['type'] == 1){
            //GPS
            if (!isset($req['latitude'])) {
                throw new Exception("Latitude is required");
            }
            if (!isset($req['longitude'])) {
                throw new Exception("Longitude is required");
            }        
            if (!isset($req['address'])) {
                throw new Exception("Address is required");
            } 
        }else if($req['type'] == 2){
            //QRCODE
            if (!isset($req['staff_id'])) {
                throw new Exception("Staff ID is required");
            }
            if (!isset($req['device'])) {
                throw new Exception("Staff Device is required");
            }        
            if (!isset($req['timestamp'])) {
                throw new Exception("TimeStamp is required");
            }             
            if (!isset($req['qr_type'])) {
                throw new Exception("QR Type(host/staff) is required");
            }
        }
        // if (!isset($req['login_type']) && $req['login_type'] != 0) {
        //     throw new Exception("Login type is required or not equal to normal user");
        // }

        return true;
    };
    $main = function($request) {
        global $db_conn;
        $res = [];
        if($request['type'] == 1){
            $address = urldecode($request['address']);
            $lang = $request['latitude'];
            $long = $request['longitude'];
            $img = $request['img_path'];

        }
        //$sql_check_in = "INSERT INTO staff_checkin (staff_id,langtitude,longitude,address) VALUES ('{$request['staff_id']}','{$request['lantitude']}','{$request['longitude']}','{$address}')";

        $staff_id = $request['staff_id'];
        $type = $request['type'];
        $timestamp = date("Y-m-d H:i:s", $request['timestamp']);
        $year = date("Y", $request['timestamp']);
        $month = date("m", $request['timestamp']);
        $day = date("d", $request['timestamp']);
        if($type == 1){
            //GPS
            $sql_check_in = "INSERT INTO staff_checkin (staff_id,type,latitude,longitude,address,img,timestamp,year,month,day) VALUES ($staff_id,$type,'$lang','$long','$address','$img','$timestamp',$year,$month,$day)";
            mysqli_query($db_conn,$sql_check_in);
            $res['params']['type'] = $type;
            $res['params']['id'] = $staff_id;


        }else if ($type == 2){
            //QRCODE
            $staff_device = $request['device'];
            $shop_device = $request['shop_device'];
            $shop_id = $request['shop_id'];
            $shop_ip = $request['shop_ip'];

            if($request['qr_type'] == 'staff'){
              //staff scanning
                //verify shop device code exist or not
                $sql_shop_device = "SELECT tsu.id FROM shop AS s LEFT JOIN shop_fixed_ip AS tsi ON s.id = tsi.shop_id LEFT JOIN shop_uuid AS tsu ON s.id = tsu.shop_id ";
                $where_array = [];
                $where_array[] = "tsu.device_token = '$shop_device'";
                $where_array[] = "tsi.ip_address = '$shop_ip'";
                $where_array[] = "s.id = $shop_id";  //may not needed

                $sql_shop_device .= " WHERE ".implode(" AND ", $where_array); 

                $rs_shop_device = mysqli_query($db_conn, $sql_shop_device);
                if (!$rs_shop_device) {
                    if (isset($request["debug"]))
                        throw new Exception($sql_shop_device . ":" . mysqli_error($db_conn));
                    else
                        throw new Exception("Server Error");
                }

                if (mysqli_num_rows($rs_shop_device) <= 0) {
                    throw new Exception("No shop is found. Please check HOST QR code OR contact OFFICE".$sql_shop_device);
                }
            }

            //verify staff exist or not 
            $sql_target = "SELECT id,device_token
                            FROM user_staff
                            WHERE id = $staff_id
                          ";
            $rs_target = mysqli_query($db_conn, $sql_target);
            if (!$rs_target) {
                if (isset($request["debug"]))
                    throw new Exception($sql_target . ":" . mysqli_error($db_conn));
                else
                    throw new Exception("Server Error");
            }

            if (mysqli_num_rows($rs_target) == 0) {
                throw new Exception("No staff is found. Check in Fail");
            }

            $staff_profiles = mysqli_fetch_all($rs_target,MYSQLI_ASSOC);
            $res['params'] = $staff_profiles[0];

            if($staff_profiles[0]['device_token'] != $staff_device){
                throw new Exception("Device Checking Error, Please Contact Office");
            }

            //check in query
            $sql_check_in = "INSERT INTO staff_checkin (staff_id,shop_id,type,timestamp,year,month,day) VALUES ($staff_id,$shop_id,$type,'$timestamp',$year,$month,$day)";
            mysqli_query($db_conn,$sql_check_in);
            $res['params']['type'] = $type;
            $res['params']['id'] = $staff_id;
        }//end if

           //check if attendence of staff//
            $date_y_m_d = date('Y-m-d',$request['timestamp']);
            $date_h_i_s = date('H:i:s',$request['timestamp']);
            $sql_check_attendence = "SELECT working_time_start,working_time_end 
                                     FROM staff_attendance ";
            $where_array = [];
            if($type == 2){
                //QRCODE
                $where_array[] = "shop_id = $shop_id"; 
            }
            $where_array[] = "user_staff_id = $staff_id";
            $where_array[] = "working_date = '$date_y_m_d'";
            $sql_check_attendence .= " WHERE ".implode(" AND ", $where_array); 

            $rs_check_attendence = mysqli_query($db_conn, $sql_check_attendence);

            if (!$rs_check_attendence) {
                if (isset($request["debug"]))
                    throw new Exception($sql_check_attendence . ":" . mysqli_error($db_conn));
                else
                    throw new Exception("Server Error".$sql_check_attendence);
            }

            if($type == 1){
                //GPS
                $sql_update_attend = "INSERT INTO staff_attendance(user_staff_id,working_date,working_time_start,working_time_end) VALUES($staff_id,'$date_y_m_d','$date_h_i_s','$date_h_i_s') ";
            }else if($type == 2){
                //QRCODE
                $sql_update_attend = "INSERT INTO staff_attendance(shop_id,user_staff_id,working_date,working_time_start,working_time_end) VALUES($shop_id,$staff_id,'$date_y_m_d','$date_h_i_s','$date_h_i_s') ";
            }
            
            

            if (mysqli_num_rows($rs_check_attendence) > 0) {
                //first time attend
                $sql_update_attend .= "ON DUPLICATE KEY UPDATE working_time_end = VALUES(working_time_end)";
            }
            $rs_update_attend = mysqli_query($db_conn, $sql_update_attend);

            if (!$rs_update_attend) {
                if (isset($request["debug"]))
                    throw new Exception($sql_update_attend . ":" . mysqli_error($db_conn));
                else
                    throw new Exception("Server Error".$sql_update_attend);
            }


            return $res;  
    };
    $checking($request);
    $res = $main($request);
    return $res;
}

function api_getAvailableStaff($request) {
    global $db_conn;
    $res = [];

    $subordinate_id = [];
    $sql_get_subordinate = "SELECT user_staff_id FROM leave_application_approver_staff WHERE leave_application_approver_staff_id IN ({$request['staff_id']})";
    $rs_get_subordinate = mysqli_query($db_conn, $sql_get_subordinate);
    while($row_subordinate = mysqli_fetch_assoc($rs_get_subordinate)){
        $subordinate_id[] = $row_subordinate['user_staff_id'];
    }

    if(!in_array($request['staff_id'], $subordinate_id)){
        $subordinate_id[] = $request['staff_id'];
    }


    $sql_get_staff_record = "SELECT u.*, s.name AS shop_name FROM user_staff AS u LEFT JOIN shop AS s ON u.home_shop_id = s.id";
    $where_array = [];
    $where_array[] = "u.id IN (".implode(',', $subordinate_id).")";
    $sql_get_staff_record .= " WHERE ".implode(" AND ", $where_array);

    $rs_get_staff_record = mysqli_query($db_conn, $sql_get_staff_record);
    if (!$rs_get_staff_record) {
        if (isset($request["debug"]))
            throw new Exception($sql_get_staff_record . ":" . mysqli_error($db_conn));
        else
            throw new Exception("Server Error");
    }

    if (mysqli_num_rows($rs_get_staff_record) == 0) {
        throw new Exception("No staff found.");
    }

    $staff_profiles = mysqli_fetch_all($rs_get_staff_record,MYSQLI_ASSOC);
    $res = $staff_profiles;

    return $res;
}


/**
 * Api apply day off function || Staff apply one day off
 * @author derek
 * @param type $request 
 * [
 *  staff_id => <nubmer>
 *  report_type => <string> (details/check_in/late/leave)
 *  shop_id => <number>
 *  date => <string> format:"YYYY-mm"
 *
 *  [optional]
 *  year => <string>
 *  month => <string>
 *  day => <string>
 * ]
 * 
 * @return type
 */
function getReportData($request){
        $checking = function($req) {
        if(!isset($req['report_type']) || $req['report_type'] == ""){
            throw new Exception("Report type is required");
        }
        if(!isset($req['staff_id'])){
            throw new Exception("Staff ID is required");
        }
        if(!isset($req['shop_id'])){
            throw new Exception("Shop ID is required");
        }

        return true;
    };
    $main = function($request) {
        global $db_conn;
        $res = [];
        $staff_id = $request['staff_id'];
        $report_type = $request['report_type'];
        $shop_id = $request['shop_id'];

        switch ($report_type) {
            case 'details':
                $date = $request['date'];
                $sql_report_data = "SELECT tss.user_staff_id,
                                        tss.working_date,
                                        ssi.start_time as working_time_start,
                                        ssi.end_time as working_time_end,
                                        tsa.working_time_start AS attendance_start,
                                        tsa.working_time_end AS attendance_end,
                                        tss.staff_schedule_item_id,
                                        tss.leave_id,
                                        tsa.shop_id
                                        FROM staff_schedule AS tss
                                        LEFT JOIN staff_attendance AS tsa ON tss.user_staff_id = tsa.user_staff_id AND tss.working_date = tsa.working_date AND (tsa.shop_id = tss.shop_id OR tsa.shop_id = 0)
                                        LEFT JOIN staff_schedule_item AS ssi ON ssi.id = tss.staff_schedule_item_id 
                                  ";

                $where_array = [];
                $where_array[] = "tss.user_staff_id = $staff_id";
                $where_array[] = "tss.working_date LIKE '{$date}%'";

                $sql_report_data .= " WHERE ".implode(" AND ", $where_array);
                $sql_report_data .= " GROUP BY tss.working_date ";

                if(isset($request['debug'])){
                    $res['params']['request'] = $request;
                    $res['params']['sql'] = $sql_report_data;
                    return $res['params'];
                }

                break;
            case 'check_in':
                $date = $request['date'];
                $date = explode("-", $date);
                $sql_report_data = "SELECT id,staff_id,shop_id,type,year,month,day,COUNT(*) AS count FROM staff_checkin";

                $where_array = [];
                $where_array[] = "year = {$date[0]}";
                $where_array[] = "month = {$date[1]}"; 
                $where_array[] = "staff_id = $staff_id"; 

                $sql_report_data .= " WHERE ".implode(" AND ", $where_array);

                $sql_report_data .= " GROUP BY day,month,year";

                if(isset($request['debug'])){
                    $res['params']['request'] = $request;
                    $res['params']['sql'] = $sql_report_data;
                    return $res['params'];
                }

                break;
            case 'check_in_details':
                $year = $request['year'];
                $month = $request['month'];
                $day = $request['day'];
                $sql_report_data = "SELECT tsc.staff_id,
                                           tsc.shop_id,
                                           tsc.latitude,
                                           tsc.longitude,
                                           tsc.address,
                                           tsc.img,
                                           tsc.type,
                                           tsc.timestamp,
                                           s.address AS shop_address,
                                           s.name AS shop_name 
                                    FROM staff_checkin AS tsc 
                                    LEFT JOIN shop AS s ON s.id = tsc.shop_id";
                $where_array = [];
                $where_array[] = "tsc.year = $year";
                $where_array[] = "tsc.month = $month";
                $where_array[] = "tsc.day = $day";
                $where_array[] = "tsc.staff_id = $staff_id";

                $sql_report_data .= " WHERE ".implode(" AND ", $where_array);

                if(isset($request['debug'])){
                    $res['params']['request'] = $request;
                    $res['params']['month'] = $month;       
                    $res['params']['year'] = $year;
                    $res['params']['day'] = $day;
                    $res['params']['sql'] = $sql_report_data;
                    return $res['params'];
                }
                break;
            case 'late':
                $date = $request['date'];
                $date_array = explode("-", $date);
                $month = $date_array[1];
                $year = $date_array[0];

                $sql_report_data = "SELECT tss.user_staff_id,
                                        tss.working_date,
                                        ssi.start_time as working_time_start,
                                        tsa.working_time_start AS attendance_start,
                                        tss.staff_schedule_item_id
                                   FROM staff_schedule AS tss
                                   LEFT JOIN staff_attendance AS tsa ON tss.user_staff_id = tsa.user_staff_id
                                   AND tss.working_date = tsa.working_date
                                   AND (tsa.shop_id = tss.shop_id OR tsa.shop_id = 0)
                                   LEFT JOIN staff_schedule_item AS ssi ON ssi.id = tss.staff_schedule_item_id 
                                  ";

                $where_array = [];
                $where_array[] = "tss.user_staff_id = $staff_id";
                $where_array[] = "tss.working_date LIKE '{$date}%'";
                $where_array[] = "tss.leave_id = 0";
                $where_array[] = "tss.user_staff_id = $staff_id";

                $sql_report_data .= " WHERE ".implode(" AND ", $where_array);
                $sql_report_data .= " GROUP BY tss.working_date";


                if(isset($request['debug'])){
                    $res['params']['request'] = $request;
                    $res['params']['month'] = $month;       
                    $res['params']['year'] = $year;
                    $res['params']['sql'] = $sql_report_data;
                    return $res['params'];
                }

                $rs_report_data = mysqli_query($db_conn, $sql_report_data);
                if (!$rs_report_data) {
                    if (isset($request["debug"]))
                        throw new Exception($rs_report_data . ":" . mysqli_error($db_conn));
                    else
                        throw new Exception("Server Error\n".$sql_report_data);
                }
                $report_profiles = mysqli_fetch_all($rs_report_data,MYSQLI_ASSOC);

                //cal number of days in month
                $numberOfDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                $existRecordDays = []; 
                $total = 0;

                foreach ($report_profiles as $key => $profile) {
                    $day = substr($profile['working_date'],-2);
                    $report_profiles[$key]['day'] = (int)$day;
                    $existRecordDays[] = $day;

                    //calculate the time diff && add into $report_profiles
                    $work_start = strtotime($profile['working_time_start']);
                    $attend_start = strtotime($profile['attendance_start']);
                    $timeDiff = floor(($attend_start-$work_start)/60);
                    if($timeDiff > 0){
                        $report_profiles[$key]["lateInMins"] = $timeDiff;
                        $total += $timeDiff;
                    }else{
                        $report_profiles[$key]["lateInMins"] = 0;
                    }
                }

                for($i=1;$i<=$numberOfDay;$i++){
                    if(!in_array($i, $existRecordDays)){
                        //new profile obj
                        $temp['user_staff_id'] = $staff_id;
                        $temp['working_date'] = "$date-$i";
                        $temp["working_time_start"] = "00:00:00";
                        $temp['attendance_start'] = "00:00:00";
                        $temp['staff_schedule_item_id'] = "-1";
                        $temp['day'] = $i;
                        $temp['lateInMins'] = 0;
                        $report_profiles[] = $temp;
                    }
                }

                $res = $report_profiles;
                
                $res['params'][] = $report_profiles;
                $res['params']['total'] = $total;
                return $res['params'];
                //break;
            case 'leave':
                $date = $request['date'];
                $sql_report_data = "SELECT laf.id,
                                    lad.leave_date,
                                    lad.num_of_days,
                                    lad.start_time,
                                    lad.end_time,
                                    si.name,
                                    laf.request_approve_remark
                                    FROM leave_application_form AS laf
                                    LEFT JOIN leave_application_date AS lad ON laf.id = lad.leave_application_form_id
                                    LEFT JOIN staff_schedule_item AS si ON si.id = lad.leave_type
                                ";
                
                $where_array = [];
                $where_array[] = "laf.shop_id = shop_id";
                $where_array[] = "laf.user_staff_id = $staff_id";
                $where_array[] = "lad.leave_date LIKE '{$date}%'";
                $where_array[] = "laf.request_approve_status = 1";

                $sql_report_data .= " WHERE ".implode(' AND ', $where_array);

                if(isset($request['debug'])){
                    $res['params']['request'] = $request;
                    $res['params']['sql'] = $sql_report_data;
                    return $res['params'];
                }

                $rs_report_data = mysqli_query($db_conn, $sql_report_data);
                if (!$rs_report_data) {
                    if (isset($request["debug"]))
                        throw new Exception($rs_report_data . ":" . mysqli_error($db_conn));
                    else
                        throw new Exception("Server Error\n".$sql_report_data);
                }

                $res['params'] = [];
                while($row_report_data = mysqli_fetch_assoc($rs_report_data)){
                    $temp = [];
                    $temp['id'] = $row_report_data['id'];
                    $temp['start_date'] = $row_report_data['leave_date'];
                    $temp['end_date'] = date('Y-m-d',strtotime($row_report_data['leave_date'].'+'.$row_report_data['num_of_days'].' days'));
                    $temp['start_time'] = $row_report_data['start_time'];
                    $temp['end_time'] = $row_report_data['end_time'];
                    $temp['leave_type'] = $row_report_data['name'];
                    $temp['remark'] = $row_report_data['request_approve_remark'];
                    $res['params'][] = $temp;
                }

                return $res['params'];
            default:
                # code...
                break;
        }

        $rs_report_data = mysqli_query($db_conn, $sql_report_data);
        if (!$rs_report_data) {
            if (isset($request["debug"]))
                throw new Exception($rs_report_data . ":" . mysqli_error($db_conn));
            else
                throw new Exception("Server Error\n".$sql_report_data);
        }

        // if (mysqli_num_rows($rs_report_data) == 0) {
        //     throw new Exception("No shop is found. Please Check Your IP address.");
        // }
        if($report_type != 'details'){
            $report_profiles = mysqli_fetch_all($rs_report_data,MYSQLI_ASSOC);
            $res['params'] = $report_profiles;
        }else{
            // $res['params'] = [];
            // while($row_data = mysqli_fetch_assoc($rs_report_data)){
            //     $res['params'][][$row_data['working_date']] = $row_data;
            // }
            $report_profiles = mysqli_fetch_all($rs_report_data,MYSQLI_ASSOC);
            $res['params'] = $report_profiles;
        }

        
        if(isset($request["debug"])){
            $res['params']['sql'] = $sql_report_data;
        }
        return $res['params'];
    };
    $checking($request);
    $res = $main($request);
    return $res;
}

/**
 * Host login function || use UUID
 * @param type $request 
 * [
 *  device_code  => <String>
 *  login_type => <Int> //defaut 0 for host
 * ]
 * 
 * @return type
 */
function api_host_login($request) {
    $checking = function($req) {
        if(!isset($req['login_type'])){
            throw new Exception("Login type is required");
        }
        if($req['login_type'] != 0){
            throw new Exception("Login type is not host, please check");
        }
        if(!isset($req['device_code'])){
            throw new Exception("No device code get");
        }

        return true;
    };
    $main = function($request) {
        global $db_conn;
        $res = [];
        $sql_find_shop = "SELECT s.id,
                                 s.shop_no,
                                 s.name,
                                 s.address,
                                 s.phone,
                                 ts.ip_address,
                                 tsu.device_token
                           FROM shop AS s
                           LEFT JOIN shop_fixed_ip AS ts ON s.id = ts.shop_id
                           LEFT JOIN shop_uuid AS tsu ON s.id = tsu.shop_id
                           WHERE ts.ip_address = '{$request['ip']}'
                          ";

        $rs_find_shop = mysqli_query($db_conn, $sql_find_shop);
        if (!$rs_find_shop) {
            if (isset($request["debug"]))
                throw new Exception($rs_find_shop . ":" . mysqli_error($db_conn));
            else
                throw new Exception("Server Error");
        }

        if (mysqli_num_rows($rs_find_shop) == 0) {
            throw new Exception("No shop is found. Please Check Your IP address.");
        }

        $shop_profiles = mysqli_fetch_all($rs_find_shop,MYSQLI_ASSOC);
        $res['params'] = $shop_profiles[0];

        if($shop_profiles[0]['device_token'] != $request['device_code'] && $shop_profiles[0]['device_token'] != ''){
            throw new Exception("UUID not match. Please contact Office.");
        }



        //update user unique device token
        $sql_device_code = "INSERT INTO shop_uuid (shop_id,device_token) VALUES ('{$shop_profiles[0]['id']}','{$request['device_code']}') ON DUPLICATE KEY UPDATE device_token = '{$request['device_code']}'";

        $rs_device_code = mysqli_query($db_conn,$sql_device_code);
        if (!$rs_device_code) {
            if (isset($request["debug"]))
                throw new Exception($rs_device_code . ":" . mysqli_error($db_conn));
            else
                //throw new Exception("Server Error");
                throw new Exception($sql_device_code);
        }
        $res['params']['device_code'] = $request['device_code'];
        return $res['params'];
    };
    $checking($request);
    $res = $main($request);
    return $res;
}
?>