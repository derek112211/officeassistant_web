<?php

// common function
function debug_to_console( $data ) {
    $output = $data;
    if ( is_array( $output ) )
        $output = implode( ',', $output);

    echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
}

function getPostVal($index_name){
	$val = isset($_POST[$index_name]) ? $_POST[$index_name] : '';
	return $val;
}

function redirectTo($url, $delay = null){
	if($delay == null){
		header("Location: $url");
	}else{
		header("Refresh: $delay; url=$url");
	}
	exit;
}

function formOption_isSelected($option_value, $check_value){
	if("".$option_value === "".$check_value){
		return "selected";
	}else{
		return "";
	}
}

function formCheckBox_isChecked($check_value){
	if(isset($check_value) && $check_value == '1'){
		return "checked";
	}else{
		return "";
	}
}

function isLogin(){
	if(!isset($_SESSION['user_id'])){
		return false;
	}
	if($_SESSION['user_id']."" === "0" || (isset($_SESSION['user_id']) && !empty($_SESSION['user_id']))){
		return true;
	}else{
		return false;
	}
}



?>