<?php

require_once("top.php");
require_once 'php_lib/api_functions.php';
header('Content-Type: application/json; charset=utf-8');

function checking(&$req) {
    global $db_conn;

    // action = list / list_staff [default : list]

    // $req["shop_id"] = mysqli_real_escape_string($db_conn,$req['shop_id']);


    if($req["action"] == "list_staff" && !api_utils_issetAndNotEqual($req['shop_id'])){
        throw new Exception("Shop ID is required");
    }
    
    return true;
}

function main($request) {
    global $db_conn;
    $res = [];
    $res["status"] = true;
    $res['code'] = "M0001";
    $res['message'] = "Get Shop Successful. action: ".$request['action'];
    $res['last_request_at'] = date("Y-m-d H:i:s");
    $res['action'] = $request['action'];
    $res['params'] = api_get_shop($request);
    return $res;
    
}

try {
    checking($_POST);
    $res = main($_POST);
    echo json_encode($res);

} catch (Exception $ex) {
    echo json_encode([
        "status" => false,
        "code" => "M0002",
        "message" => "Get Shop Error\n".$ex->getMessage(),
        "last_request" => date("Y-m-d H:i:s"),
        "params" => [
            "reason" => $ex->getMessage()
        ]
    ]);
}

require_once("bottom.php");


?>
