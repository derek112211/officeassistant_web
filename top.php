<?php 
// load database config
include "./conf/dbinfo.php";
require_once 'connect.php';

set_time_limit(300); // 5 min
session_start();

// load php libraries
require_once("php_lib/functions.php");
require_once("global.php");

// load lang file
require_once("lang/zh-hant.php");

// php ini setting 
// error_reporting(E_ALL);
// ini_set('display_errors','1');
date_default_timezone_set("Asia/Taipei");    
?>