<?php
  require_once("top.php");
  require_once("header.php");
  require_once("page_check.php");

  $debug = false;

  $page_title = "Staff Schedule";
  $sidebar = "staff_schedule";

  // init table header
  $table_header = [];
  $table_header[] = __LANG_RES_STAFF_SCHEDULE_NAME;
  $table_header[] = __LANG_RES_STAFF_SCHEDULE_LABEL;
  $table_header[] = __LANG_RES_STAFF_SCHEDULE_TIME_START;
  $table_header[] = __LANG_RES_STAFF_SCHEDULE_TIME_END;
  $table_header[] = __LANG_RES_STAFF_SCHEDULE_LEAVE;
  $table_header[] = __LANG_RES_STAFF_SCHEDULE_ACTION;

  $table_header2 = [];
  $table_header2[] = __LANG_RES_STAFF_SCHEDULE_STAFF_CODE;
  $table_header2[] = __LANG_RES_STAFF_SCHEDULE_STAFF_NAME;
  $table_header2[] = __LANG_RES_STAFF_SCHEDULE_SHOP_NAME;
  $table_header2[] = __LANG_RES_STAFF_SCHEDULE_ACTION;


  // delete
  if(isset($_POST['action']) && $_POST['action'] == 'delete'){

    $schedule_item_id = $_POST['schedule_item_id'];
    $delete_time = date('Y-m-d H:i:s');

    mysqli_query($db_conn, "START TRANSACTION");
    $sql_delete_item = "UPDATE staff_schedule_item SET delete_staff_id = {$current_user['id']},delete_time = '$delete_time' WHERE id = $schedule_item_id";
    $rs_delete_item = mysqli_query($db_conn,$sql_delete_item);

    if($rs_delete_item){
      // update leave table
      mysqli_query($db_conn, "COMMIT");
      $alert['style'] = 'success';
      $alert['msg'] = 'DELETE SUCCESS';

    }else{
      mysqli_query($db_conn, "ROLLBACK");
      // print_r($sql_delete_form);
      $alert['style'] = 'fail';
      $alert['msg'] = 'DELETE FAILED';

    }
  }

  // edit
  if(isset($_POST['action']) && $_POST['action'] == 'edit'){

    $schedule_item_id = $_POST['schedule_item_id'];
    $schedule_name = $_POST['schedule_name'];
    $schedule_label = $_POST['schedule_label'];
    $schedule_start_time = $_POST['schedule_start_time'];
    $schedule_end_time = $_POST['schedule_end_time'];
    $is_leave = $_POST['is_leave'];

    mysqli_query($db_conn, "START TRANSACTION");
    $sql_update_item = "UPDATE staff_schedule_item SET 
                          name = '$schedule_name',
                          label = '$schedule_label',
                          start_time = '$schedule_start_time',
                          end_time = '$schedule_end_time',
                          is_leave = '$is_leave'
                        WHERE id = $schedule_item_id
                        ";
    $rs_update = mysqli_query($db_conn,$sql_update_item);

    if($rs_update){
      // update leave table
      mysqli_query($db_conn, "COMMIT");
      $alert['style'] = 'success';
      $alert['msg'] = 'UPDATE SUCCESS';

    }else{
      mysqli_query($db_conn, "ROLLBACK");
      // print_r($sql_delete_form);
      $alert['style'] = 'fail';
      $alert['msg'] = 'UPDATE FAILED';

    }
  }

  if(isset($_POST['action']) && $_POST['action'] == 'add'){
    $schedule_name = $_POST['schedule_name'];
    $schedule_label = $_POST['schedule_label'];
    $schedule_start_time = $_POST['schedule_start_time'];
    $schedule_end_time = $_POST['schedule_end_time'];
    $is_leave = $_POST['is_leave'];

    if($schedule_start_time >= $schedule_end_time){
      $alert['style'] = 'fail';
      $alert['msg'] = 'INSERT FAILED<br>Schedule time should be larger than start time';
    }else{
      $sql_insert = "INSERT INTO staff_schedule_item (name,label,start_time,end_time,is_leave) 
                          VALUES ('$schedule_name','$schedule_label','$schedule_start_time','$schedule_end_time','$is_leave')
                          ";
      $rs_insert = mysqli_query($db_conn,$sql_insert);
      if($rs_insert){
        $alert['style'] = 'success';
        $alert['msg'] = 'INSERT SUCCESS';
      }else{
        $alert['style'] = 'fail';
        $alert['msg'] = 'INSERT FAILED';
      }
    }


  }
  // retrieve data from db

  // get staff
  $staffs = [];
  $sql_staff = "SELECT * FROM user_staff";
  $rs_staff = mysqli_query($db_conn,$sql_staff) or die ("$sql_staff :".mysqli_error($db_conn));
  while($row_staff = mysqli_fetch_assoc($rs_staff)){
    $staffs[$row_staff['id']] = $row_staff;
  } 

  // get shop
  $shops = [];
  $sql_shop = "SELECT * FROM shop";
  $rs_shop = mysqli_query($db_conn,$sql_shop) or die ("$sql_shop :".mysqli_error($db_conn));
  while($row_shop = mysqli_fetch_assoc($rs_shop)){
    $shops[$row_shop['id']] = $row_shop;
  }

  // get schedule item
  $schedule_items = [];
  $sql_schedule_item = "SELECT * FROM staff_schedule_item WHERE delete_staff_id = 0";

  $rs_schedule_item = mysqli_query($db_conn,$sql_schedule_item) or die ("$sql_schedule_item :".mysqli_error($db_conn));
  if(mysqli_num_rows($rs_schedule_item) > 1){
    while($row_schedule_item = mysqli_fetch_assoc($rs_schedule_item)){
      $schedule_items[$row_schedule_item['id']] = $row_schedule_item;
    }
  }




  // retrieve data from db - end  

?>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">


<?php 

require_once('nav.php');
require_once('sidebar.php'); 

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $page_title ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="homePage.php">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $page_title ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- STAFF SCHEDULE ITEM TABLE -->
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-lg-11 col-6">
              <h3 class="card-title">Staff Schedule Items</b>
              </h3>
            </div>          
          </div>
        </div>

        <!-- /.card-header -->
        <div class="card-body">

          <?php 

          if(isset($alert)){ 
            if($alert['style'] == 'success'){
              echo '<div class="alert alert-success alert-dismissible">';
              echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
              echo '<h5><i class="icon fas fa-check"></i> Success!</h5>';
              echo $alert['msg'].'</div>';
            }else{
              echo '<div class="alert alert-danger alert-dismissible">';
              echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
              echo '<h5><i class="icon fas fa-check"></i> FAILED!</h5>';
              echo $alert['msg'].'</div>';
            }
          }

          ?>

          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title"></h3>

                  <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                      <!-- <input type="text" name="leave_table" id="leave_table" class="form-control float-right" placeholder="Search"> -->

                      <div class="input-group-append">
                        <!-- <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button> -->
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                  <table class="table table-bordered table-striped" id="staff_schedule_item_table">
                    <thead>
                      <tr>
                        <?php
                          foreach ($table_header as $value) {
                            echo '<th>'.$value.'</th>';
                          }
                        ?>
                      </tr>
                    </thead>
                    <tbody>
                     <!-- Add Row -->
                      <tr>
                        <td>
                          <div class="form-group">
                            <input class="form-control" name="add_schedule_name">
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <input class="form-control" name="add_schedule_label">
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <input class="form-control" type='text' name="add_schedule_start_time">
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <input class="form-control" name="add_schedule_end_time">
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <select class="form-control" name="add_is_leave">
                              <option value="1">YES</option>
                              <option value="0">NO</option>
                            </select>
                          </div>
                        </td>
                        <td>
                          <div class="col-sm-12 col-lg-12 col-xl-12">
                            <button type="button" class="btn btn-block btn-success" onclick="check_add($(this))">Add</button>
                          </div>
                        </td>
                      </tr>

                      <?php

                        foreach ($schedule_items as $schedule_item_id => $value) {
                          echo '<tr data-id="'.$schedule_item_id.'">';
                          
                          // Name
                          // (for view)
                          echo '<td name="display_schedule_name">'.$value['name'].'</td>';
                          // (for input)
                          echo '<td style="display:none;">';
                          echo '<input name="edit_schedule_name" class="form-control" value="'.$value['name'].'">';
                          echo '</select></td>';


                          // label
                          echo '<td name="display_schedule_label">'.$value['label'].'</td>';
                          // (for input)
                          echo '<td style="display:none;">';
                          echo '<input name="edit_schedule_label" class="form-control" value="'.$value['label'].'">';
                          echo '</select></td>';

                          // start time
                          echo '<td name="display_schedule_start_time">'.$value['start_time'].'</td>';
                          // (for input)
                          echo '<td style="display:none;">';
                          echo '<input name="edit_schedule_start_time" class="form-control" value="'.$value['start_time'].'">';
                          echo '</select></td>';

                          // end time
                          echo '<td name="display_schedule_end_time">'.$value['end_time'].'</td>';
                          // (for input)
                          echo '<td style="display:none;">';
                          echo '<input name="edit_schedule_end_time" class="form-control" value="'.$value['end_time'].'">';
                          echo '</select></td>';

                          // is leave
                          // (for view)
                          if($value['is_leave'] == 1){
                            echo '<td name="display_is_leave">YES</td>';
                          }else{
                            echo '<td name="display_is_leave">NO</td>';
                          }
                          // (for input)
                          echo '<td style="display:none;">';
                          echo '<select class="form-control" name="edit_is_leave">';
                            echo '<option value="1" '.formOption_isSelected($value['is_leave'],1).'>YES</option>';
                            echo '<option value="0" '.formOption_isSelected($value['is_leave'],0).'>NO</option>';
                          echo '</select></td>';

                          // action
                          echo '<td>';
                          echo '<div class="row">';
                            echo '<div class="col-sm-6 col-lg-6 col-xl-6">';
                            echo '<button type="button" class="btn btn-block btn-primary" onclick="edit_row($(this));">'.__LANG_RES_STAFF_SCHEDULE_ACTION_EDIT.'</button>';
                            echo '</div>';
                            echo '<div class="col-sm-6 col-lg-6 col-xl-6"><button type="button" class="btn btn-block btn-danger" onclick="delete_row($(this));">'.__LANG_RES_STAFF_SCHEDULE_ACTION_DELETE.'</button></div>';
                            echo '</div>';
                          echo '</td>';

                          echo '</tr>';
                        }

                      ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>


      <!-- STAFF SCHEDULE TABLE -->
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-lg-11 col-6">
              <h3 class="card-title">Staff Schedule</b>
              </h3>
            </div>          
          </div>
        </div>

        <!-- /.card-header -->
        <div class="card-body">

          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <div class="row">
                    <div class="col-lg-11 col-6"><h3 class="card-title">Staffs</h3></div>
                    

                    <div class="col-lg-1 col-6">
                      <!-- <a href="add_schedule.php" class="btn btn-block btn-success" role="button">Add Schedule</a> -->
                    </div>
                    
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                  <table class="table table-bordered table-striped" id="staff_schedule_table">
                    <thead>
                      <tr>
                        <?php
                          foreach ($table_header2 as $value) {
                            echo '<th>'.$value.'</th>';
                          }
                        ?>
                      </tr>
                    </thead>
                    <tbody>

                      <?php

                        foreach ($staffs as $staff_id => $value) {
                          echo '<tr data-id="'.$staff_id.'">';
                          
                          // Staff Code
                          echo '<td id="staff_code">'.$value['staff_number'].'</td>';
                          
                          // Staff Name
                          echo '<td id="staff_name">'.$value['full_name'].'</td>';

                          // Shop name
                          echo '<td id="shop_name">'.$shops[$value['home_shop_id']]['name'].'</td>';

                          // Action
                          echo '<td>';
                            echo '<div class="col-sm-12 col-lg-12 col-xl-12">';
                              echo '<button type="button" class="btn btn-block btn-primary" onclick="view_schedule($(this));">'.__LANG_RES_STAFF_SCHEDULE_ACTION_VIEW_SCHEDULE.'</button>';
                            echo '</div>';
                          echo '</td>';

                          echo '</tr>';
                        }

                      ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
    </section>
    <!-- /.content -->
  </div>

<?php
  // load footer
  require_once("footer.php");

  // close resources, without global resources
  require_once("bottom.php");
?>

<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script type="text/javascript">
$(document).ready(function(){
    $('#staff_schedule_table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });    
    $('#staff_schedule_item_table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
});
    $('[name=add_schedule_start_time]').timepicker({
      timeFormat: 'HH:mm:ss',
    });
    $('[name=add_schedule_end_time]').timepicker({
      timeFormat: 'HH:mm:ss',
    });


  function delete_row(element){
    bootbox.confirm('<?php echo __LANG_RES_STAFF_SCHEDULE_CONFIRM_DELETE_STAFF; ?>',function(result){
        if(result){

          var schedule_item_id = $(element).parents('tr').attr('data-id');

          var form = document.createElement("form");
          var element1 = document.createElement("input");
          var action = document.createElement("input");

          form.method = "POST";
          form.action = "staff_schedule.php";

          element1.value = schedule_item_id;
          element1.name = "schedule_item_id";
          element1.style.cssText = "display: none;";
          form.appendChild(element1);

          action.value = 'delete';
          action.name = "action";
          action.style.cssText = "display: none;";
          form.appendChild(action);

          document.body.appendChild(form);

          form.submit();
          form.remove();

        }else{
          return;
        }
    }); 

  }


  function edit_row(element){
    var row = $(element).parents('tr');
    // change view to input

    row.find('[name=edit_schedule_name]').parent('td').attr('style','');
    row.find('[name=edit_schedule_label]').parent('td').attr('style','');
    row.find('[name=edit_schedule_start_time]').parent('td').attr('style','');
    row.find('[name=edit_schedule_end_time]').parent('td').attr('style','');
    row.find('[name=edit_is_leave]').parent('td').attr('style','');

    row.find('[name=display_schedule_name]').attr('style','display:none;');
    row.find('[name=display_schedule_label]').attr('style','display:none;');
    row.find('[name=display_schedule_start_time]').attr('style','display:none;');
    row.find('[name=display_schedule_end_time]').attr('style','display:none;');
    row.find('[name=display_is_leave]').attr('style','display:none;');

    // change btn - uuid
      // success
      $(element).html('<?php echo __LANG_RES_UUID_MANAGEMENT_ACTION_SUBMIT; ?>');
      $(element).removeClass("btn-primary");
      $(element).addClass("btn-success");
      $(element).attr('onclick',"submit_edit_row($(this))");

      // cancel btn
      var cancel_btn = $(element).parent().siblings().find('button');
      cancel_btn.attr('onclick',"cancel_edit_row($(this))");
      cancel_btn.html('<?php echo __LANG_RES_UUID_MANAGEMENT_ACTION_CANCEL; ?>');

  }


  function cancel_edit_row(element){
    
    var row = $(element).parents('tr');
    var submit_btn = $(element).parent().siblings().find('button');

    row.find('[name=edit_schedule_name]').parent('td').attr('style','display:none;');
    row.find('[name=edit_schedule_label]').parent('td').attr('style','display:none;');
    row.find('[name=edit_schedule_start_time]').parent('td').attr('style','display:none;');
    row.find('[name=edit_schedule_end_time]').parent('td').attr('style','display:none;');
    row.find('[name=edit_is_leave]').parent('td').attr('style','display:none;');


    row.find('[name=display_schedule_name]').attr('style','');
    row.find('[name=display_schedule_label]').attr('style','');
    row.find('[name=display_schedule_start_time]').attr('style','');
    row.find('[name=display_schedule_end_time]').attr('style','');
    row.find('[name=display_is_leave]').attr('style','');

    // change btn status
    $(element).html('<?php echo __LANG_RES_STAFF_SCHEDULE_ACTION_DELETE; ?>');
    $(element).attr('onclick',"delete_row($(this))");
    submit_btn.attr('onclick',"edit_row($(this))");
    submit_btn.removeClass('btn-success');
    submit_btn.addClass('btn-primary');
    submit_btn.html('<?php echo __LANG_RES_STAFF_SCHEDULE_ACTION_EDIT; ?>');

  }

  function submit_edit_row(element){
    // var cancel_btn = $(element).parent().siblings().find('button');
    var row = $(element).parents('tr');

    var schedule_item_id = row.attr('data-id');
    var schedule_name = row.find('[name=edit_schedule_name]').val();
    var schedule_label = row.find('[name=edit_schedule_label]').val();
    var schedule_start_time = row.find('[name=edit_schedule_start_time]').val();
    var schedule_end_time = row.find('[name=edit_schedule_end_time]').val();
    var is_leave = row.find('[name=edit_is_leave]').val();

    var form = document.createElement("form");
    var action = document.createElement("input");
    var element1 = document.createElement("input");
    var element2 = document.createElement("input");
    var element3 = document.createElement("input");
    var element4 = document.createElement("input");
    var element5 = document.createElement("input");
    var element6 = document.createElement("input");

    form.method = "POST";
    form.action = "staff_schedule.php";

    element1.value = schedule_item_id;
    element1.name = "schedule_item_id";
    element1.style.cssText = "display: none;";
    form.appendChild(element1);

    element2.value = schedule_name;
    element2.name = "schedule_name";
    element2.style.cssText = "display: none;";
    form.appendChild(element2);

    element3.value = schedule_label;
    element3.name = "schedule_label";
    element3.style.cssText = "display: none;";
    form.appendChild(element3);

    element4.value = schedule_start_time;
    element4.name = "schedule_start_time";
    element4.style.cssText = "display: none;";
    form.appendChild(element4);

    element5.value = schedule_end_time;
    element5.name = "schedule_end_time";
    element5.style.cssText = "display: none;";
    form.appendChild(element5);

    element6.value = is_leave;
    element6.name = "is_leave";
    element6.style.cssText = "display: none;";
    form.appendChild(element6);

    action.value = 'edit';
    action.name = "action";
    action.style.cssText = "display: none;";
    form.appendChild(action);
    
    document.body.appendChild(form);

    form.submit();
    form.remove();
  }

  function check_add(element){

    var row = $(element).parents('tr');

    var schedule_name = row.find('[name=add_schedule_name]').val();
    var schedule_label = row.find('[name=add_schedule_label]').val();
    var schedule_start_time = row.find('[name=add_schedule_start_time]').val();
    var schedule_end_time = row.find('[name=add_schedule_end_time]').val();
    var is_leave = row.find('[name=add_is_leave]').val();

    var form = document.createElement("form");
    var action = document.createElement("input");
    var element2 = document.createElement("input");
    var element3 = document.createElement("input");
    var element4 = document.createElement("input");
    var element5 = document.createElement("input");
    var element6 = document.createElement("input");

    form.method = "POST";
    form.action = "staff_schedule.php";

    element2.value = schedule_name;
    element2.name = "schedule_name";
    element2.style.cssText = "display: none;";
    form.appendChild(element2);

    element3.value = schedule_label;
    element3.name = "schedule_label";
    element3.style.cssText = "display: none;";
    form.appendChild(element3);

    element4.value = schedule_start_time;
    element4.name = "schedule_start_time";
    element4.style.cssText = "display: none;";
    form.appendChild(element4);

    element5.value = schedule_end_time;
    element5.name = "schedule_end_time";
    element5.style.cssText = "display: none;";
    form.appendChild(element5);

    element6.value = is_leave;
    element6.name = "is_leave";
    element6.style.cssText = "display: none;";
    form.appendChild(element6);

    action.value = 'add';
    action.name = "action";
    action.style.cssText = "display: none;";
    form.appendChild(action);
    
    document.body.appendChild(form);

    form.submit();
    form.remove();
  }

  function view_schedule(element){
    var row = $(element).parents('tr');
    var staff_id = row.attr('data-id');

    var form = document.createElement("form");
    var action = document.createElement("input");
    var element1 = document.createElement("input");

    form.method = "POST";
    form.action = "view_schedule.php";

    element1.value = staff_id;
    element1.name = "staff_id";
    element1.style.cssText = "display: none;";
    form.appendChild(element1);


    document.body.appendChild(form);

    form.submit();
    form.remove();
  }

</script>