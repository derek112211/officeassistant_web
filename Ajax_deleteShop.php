<?php
	require_once("top.php");
	header('Content-Type: application/json; charset=utf-8');
	
	$debug = false;

	if($debug){
		print_r($_POST);
		return;
	}


	$shop_id = $_POST['shop_id'];


	$result = [];
	$error = false;

	// check shop
	$sql_check_shop = "SELECT id FROM shop WHERE id = $shop_id";
	$rs_check_shop = mysqli_query($db_conn,$sql_check_shop) or die ("$sql_check_shop: ".mysqli_error($db_conn));
	if(mysqli_num_rows($rs_check_shop) > 0){
		// shop exist
		// delete shop
		mysqli_query($db_conn, "START TRANSACTION");
		$sql_delete_shop = "DELETE FROM shop WHERE id = $shop_id";
		$rs_delete_shop = mysqli_query($db_conn,$sql_delete_shop) or die ("$sql_delete_shop: ".mysqli_error($db_conn));
		if($rs_delete_shop){
			// delete success

			// delete related ip
			$sql_delete_ip = "DELETE FROM shop_fixed_ip WHERE shop_id = $shop_id";
			$rs_delete_ip = mysqli_query($db_conn,$sql_delete_ip) or die ("$sql_delete_ip: ".mysqli_error($db_conn));
			if(!$rs_delete_ip){
				mysqli_query($db_conn, "ROLLBACK");
				$result['status'] = false;
				$result['msg'] = "shop delete error(ip address)";
				$error = true;
			}

			// delete related device token
			// check device token exist
			$sql_check_token = "SELECT * FROM shop_uuid WHERE shop_id = $shop_id";
			$rs_check_token = mysqli_query($db_conn,$sql_check_token) or die ("$sql_check_token: ".mysqli_error($db_conn));
			if(mysqli_num_rows($rs_check_token) > 0){
				// have token record
				$sql_delete_token = "DELETE FROM shop_uuid WHERE shop_id = $shop_id";
				$rs_delete_token = mysqli_query($db_conn,$sql_delete_token) or die ("$sql_delete_token: ".mysqli_error($db_conn));
				if(!$rs_delete_token){
					mysqli_query($db_conn, "ROLLBACK");
					$result['status'] = false;
					$result['msg'] = "shop delete error(device token)";
					$error = true;
				}
			}

			if(!$error){
				mysqli_query($db_conn, "COMMIT");
				$result['status'] = true;
				$result['msg'] = "shop deleted";
			}

		}else{
			// delete shop failed
			mysqli_query($db_conn, "ROLLBACK");
			$result['status'] = false;
			$result['msg'] = "NO shop IS FOUND";
		}
	}else{
		// shop not exist
		$result['status'] = false;
		$result['msg'] = "NO shop IS FOUND";
	}

	echo json_encode($result);


	require_once("bottom.php");
?>