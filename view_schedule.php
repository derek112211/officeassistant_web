<?php
  require_once("top.php");
  require_once("header.php");
  require_once("page_check.php");
  
  $debug = false;
  $page_title = "View Schedule";
  $sidebar = "staff_schedule";

  if($debug){
    print_r($_POST);
  }
  

  $table_header = [];

  $table_header[] = __LANG_RES_VIEW_SCHEDULE_WORKING_DATE;
  $table_header[] = __LANG_RES_VIEW_SCHEDULE_SCHEDULE_NAME;
  $table_header[] = __LANG_RES_VIEW_SCHEDULE_START_TIME;
  $table_header[] = __LANG_RES_VIEW_SCHEDULE_END_TIME;
  $table_header[] = __LANG_RES_VIEW_SCHEDULE_LEAVE_APPLIED;
  // $table_header[] = __LANG_RES_VIEW_SCHEDULE_ACTION;



  $staff_id = $_POST['staff_id'];

  // get staff
  $staff = [];
  $sql_staff = "SELECT * FROM user_staff WHERE id = $staff_id";
  $rs_staff = mysqli_query($db_conn,$sql_staff) or die ("$sql_staff :".mysqli_error($db_conn));
  while($row_staff = mysqli_fetch_assoc($rs_staff)){
    $staff = $row_staff;
  } 

  // get shop
  $shops = [];
  $sql_shop = "SELECT * FROM shop";
  $rs_shop = mysqli_query($db_conn,$sql_shop) or die ("$sql_shop :".mysqli_error($db_conn));
  while($row_shop = mysqli_fetch_assoc($rs_shop)){
    $shops[$row_shop['id']] = $row_shop;
  }

  // get staff schedule
  $schedules = [];
  $temp_leave_ids = [];
  $sql_schedule = "SELECT s.* ,
                    si.name AS schedule_item_name,
                    si.start_time AS schedule_start_time,
                    si.end_time AS schedule_end_time
                   FROM staff_schedule AS s
                   LEFT JOIN staff_schedule_item AS si ON si.id = s.staff_schedule_item_id
                   WHERE user_staff_id = $staff_id 
                   ";
  $rs_schedule = mysqli_query($db_conn,$sql_schedule) or die ("$sql_schedule :".mysqli_error($db_conn));
  while($row_schedule = mysqli_fetch_assoc($rs_schedule)){
    $schedules[$row_schedule['id']] = $row_schedule;
    if(!in_array($row_schedule['leave_id'], $temp_leave_ids)){
      $temp_leave_ids[] = $row_schedule['leave_id'];
    }
  }

  // get staff leave
  $leaves = [];
  if(sizeof($temp_leave_ids) > 0){
    $sql_schedule_leave = "SELECT sl.* ,si.name AS item_name
                           FROM staff_leave AS sl
                           LEFT JOIN staff_schedule_item AS si ON si.id = sl.staff_schedule_item_id
                           WHERE sl.id IN (".implode(',', $temp_leave_ids).")
                           ";
    $rs_schedule_leave = mysqli_query($db_conn,$sql_schedule_leave) or die ("$sql_schedule_leave :".mysqli_error($db_conn));
    while($row_schedule_leave = mysqli_fetch_assoc($rs_schedule_leave)){
      // foreach ($schedules as $schedule_id => $value) {
      //   if($value['leave_id'] == $row_schedule_leave['id']){
      //     $schedules[$schedule_id]['leave_name'] = $row_schedule_leave['name'];
      //     $schedules[$schedule_id]['leave_time_start'] = $row_schedule_leave['leave_time_start'];
      //     $schedules[$schedule_id]['leave_time_end'] = $row_schedule_leave['leave_time_end'];
      //   }
      // }
      $leaves[$row_schedule_leave['id']] = $row_schedule_leave;
    }
  }

  // get normal_schedule option 
  $normal_schedules = [];
  $sql_normal_schedule = "SELECT * FROM staff_schedule_item WHERE is_leave = 0 
                   AND delete_staff_id = 0";
  $rs_normal_schedule = mysqli_query($db_conn,$sql_normal_schedule) or die ("$sql_normal_schedule :".mysqli_error($db_conn));
  while($row_normal_schedule = mysqli_fetch_assoc($rs_normal_schedule)){
    $normal_schedules[$row_normal_schedule['id']] = $row_normal_schedule;
  }

  // print_r($schedules);
  // print_r($leaves);


  // echo '<br>';
  // print_r($check_in);

?>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

<?php 

require_once('nav.php');
require_once('sidebar.php'); 

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $page_title ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="homePage.php">Home</a></li>
              <li class="breadcrumb-item"><a href="staff_schedule.php">Staff Schedule</a></li>
              <li class="breadcrumb-item active"><?php echo $page_title ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fas fa-users"></i> Staff Code - <?php echo $staff['staff_number']; ?>
                  </h4>
                </div>
                <!-- /.col -->
              </div>

              <div class="card">
                <div class="card-header">
                  <div class="row">
                    <div class="col-lg-10 col-6">
                      <h3 class="card-title">Staff Schedule</b>
                      </h3>
                    </div> 
                    <div class="col-lg-2 col-6">
                      <!-- <a href="add_staff.php" class="btn btn-block btn-success" role="button">Add Staff</a> -->
                      <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Add Schedule</button>

                    </div>         
                  </div>

                </div>

              <!-- info row -->
              <div class="row invoice-info" style="margin: 20px 0px;">
                <!-- /.col -->
                <div class="col-sm-12 invoice-col">


                  <!-- Staff Name -->
                  <div class="row">
                    <div class="col-4">
                      <b>Staff Name: </b>
                    </div>
                    <div class="col-4">
                      <?php
                        echo $staff['full_name'];
                      ?>
                    </div>
                    <div class="col-4">
                      
                    </div>
                  </div>

                  <!-- Shop -->
                  <div class="row">
                    <div class="col-4">
                      <b>Staff Number: </b>
                    </div>
                    <div class="col-4">
                      <?php echo $shops[$staff['home_shop_id']]['name']; ?>
                    </div>
                    <div class="col-4">
                      
                    </div>
                  </div>
                  <br>

                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
                <!-- /.card-header -->
                <div class="card-body">

                  <div class="row">
                    <div class="col-12">
                      <div class="card">
                        <div class="card-header">
                          <div class="row">
                            <!-- <div class="col-lg-11 col-6"><h3 class="card-title">Add</h3></div> -->
                            
                          </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                          <table class="table table-bordered table-striped" id="staff_schedule_table">
                            <thead>
                              <tr>
                                <?php
                                foreach ($table_header as $value) {
                                    echo '<th>'.$value.'</th>';
                                  }
                                ?>
                              </tr>
                            </thead>
                            <tbody>

                              <?php

                                foreach ($schedules as $schedule_id => $value) {
                                  echo '<tr data-id="'.$value['user_staff_id'].'">';
                                  
                                  // working date
                                  echo '<td id="working_date">'.$value['working_date'].'</td>';
                                  
                                  // schedule Name
                                  echo '<td id="schedule_name">'.$value['schedule_item_name'].'</td>';

                                  // Start time
                                  echo '<td id="start_time">'.$value['schedule_start_time'].'</td>';

                                  // End time
                                  echo '<td id="end_time">'.$value['schedule_end_time'].'</td>';


                                  // LEAVE
                                  if($value['leave_id'] != 0){
                                    echo '<td>';
                                      echo '<h5>Leave Type:</h5><br>';
                                      echo $leaves[$value['leave_id']]['item_name'];
                                    echo '</td>';

                                  }else{
                                    echo '<td>'.'--'.'</td>';

                                  }

                                  // Action
                                  // echo '<td>';
                                  //   echo '<div class="col-sm-12 col-lg-12 col-xl-12">';
                                  //     echo '<button type="button" class="btn btn-block btn-primary" onclick="view_schedule($(this));">'.__LANG_RES_STAFF_SCHEDULE_ACTION_VIEW_SCHEDULE.'</button>';
                                  //   echo '</div>';
                                  // echo '</td>';

                                  echo '</tr>';
                                }

                              ?>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.card-body -->
                      </div>
                      <!-- /.card -->
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>


              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  <!-- <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a> -->
                  <a href="staff_schedule.php">
                  <button type="button" class="btn btn-primary float-right">
                    <i class="fas fa-chevron-left"></i>Back
                  </button>
                  </a>
                </div>
              </div>
            </div>
    <!-- /.content -->
  </div>

  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <!-- <h4 class="modal-title">Modal Header</h4> -->
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12 col-lg-12">
            <!-- Date and time range -->
            <div class="form-group">
              <label>Schedule Range:</label>

              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="far fa-clock"></i></span>
                </div>
                <input type="text" class="form-control float-right" id="reservationtime">
              </div>
              <!-- /.input group -->
            </div>
            <!-- /.form group -->
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Schedule Type</label>
                  <select name="schedule_type" class="form-control">
                    <?php
                      foreach ($normal_schedules as $normal_schedule_id => $value) {
                        echo '<option value="'.$normal_schedule_id.'">'.$value['name'].'</option>';
                      }
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <input type="hidden" name="shop_id" value="<?php echo $staff['home_shop_id']; ?>">
            <input type="hidden" name="staff_id" value="<?php echo $staff['id']; ?>">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="check_add()">Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
  </div>
  <!-- /.content-wrapper -->
<?php
  // load footer
  require_once("footer.php");

  // close resources, without global resources
  require_once("bottom.php");
?>
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/jquery-validation/additional-methods.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
  <!-- daterange picker -->
<link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script type="text/javascript">

$(document).ready(function () {

      $('#staff_schedule_table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });  
    $('#reservationtime').daterangepicker({
      locale: {
        format: 'YYYY-MM-DD',
      }
    });
});

function check_add(){
  var schedule_item_id = $('[name="schedule_type"]').val();
  var shop_id = $('[name="shop_id"]').val();
  var staff_id = $('[name="staff_id"]').val();
  var range = $('#reservationtime').val();

  console.log(schedule_item_id);
  console.log(range);

  bootbox.confirm("<?php echo __LANG_RES_VIEW_SCHEDULE_CONFIRM_ADD_SCHDULE; ?>", function(result){ 
    if(result){
      $.ajax({
           type: "POST",
           url: "Ajax_addSchedule.php",
           data: {
              schedule_item_id : schedule_item_id,
              shop_id : shop_id,
              staff_id : staff_id,
              range : range
           },
           type:"POST",
           success: function(data)
           {
              // console.log(data.status);
              // console.log(data.msg);
              if(data.status){
                
                bootbox.alert(data.msg,function(){
                  setTimeout(function() {
                    window.location.href = "view_schedule.php";
                     var form = document.createElement("form");
                     var element1 = document.createElement("input");
                     form.method = "POST";
                     form.action = "view_schedule.php";
                      element1.value = staff_id;
                      element1.name = "staff_id";
                      element1.style.cssText = "display: none;";
                      form.appendChild(element1);
                      document.body.appendChild(form);

                      form.submit();
                      form.remove();
                  }, 10);


                });
              }else{
                bootbox.alert(data.msg);
              }
           },
           error:function(xhr, ajaxOptions, thrownError){ 
              alert(xhr.status); 
              alert(thrownError); 
           }
       });
    }
  });

  
}
</script>
