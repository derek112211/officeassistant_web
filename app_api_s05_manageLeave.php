<?php
require_once("top.php");
require_once 'php_lib/api_functions.php';
header('Content-Type: application/json; charset=utf-8');

function checking(&$req) {

    global $db_conn;
    $req["operation"] = mysqli_real_escape_string($db_conn,$req['operation']);
    $req["staff_id"] = mysqli_real_escape_string($db_conn,$req['staff_id']);
    $req["shop_id"] = mysqli_real_escape_string($db_conn,$req['shop_id']);

    if(!api_utils_issetAndNotEqual($req["operation"])){
        throw new Exception("Manage Type is required");
    }    
    if(!api_utils_issetAndNotEqual($req["staff_id"])){
        throw new Exception("Staff id is required");
    }    
    if(!api_utils_issetAndNotEqual($req["shop_id"])){
        throw new Exception("shop_id is required");
    }    
    return true;
}


function main($request) {
    global $db_conn;
    $res = [];
    $res["status"] = true;
    $res['code'] = "E0001";
    $res['message'] = "Manage Leave successfully.";
    $res['last_request_at'] = date("Y-m-d H:i:s");
    $res['operation'] = $request['operation'];
    switch ($request['operation']) {
        case 'insert':
            //apply leave
            $res['params'] = api_applyLeave($request);
            break;
        case 'check':
            //check permission
            $res['params'] = api_checkLeavePermission($request);
            break;
        case 'list':
            $res['params'] = api_listLeave($request);
            break;
        case 'approve':
            $res['params'] = api_approveLeave($request);
        default:
            break;
    }
    return $res;
}

try {
    checking($_POST);
    $res = main($_POST);
    echo json_encode($res);
} catch (Exception $ex) {
    echo json_encode([
        "status" => false,
        "code" => "E0002",
        "message" => "Manage Leave Failed",
        //"message" => $ex->getMessage(),
        "last_request" => date("Y-m-d H:i:s"),
        "params" => [
            "reason" => $ex->getMessage()
        ]
    ]);

}



require_once("bottom.php");
?>