<?php

require_once("top.php");
require_once 'php_lib/api_functions.php';
header('Content-Type: application/json; charset=utf-8');

function checking(&$req) {
    global $db_conn;

    $req["device_code"] = mysqli_real_escape_string($db_conn, $req['device_code']);
    
    if(!api_utils_issetAndNotEqual($req['device_code'])){
        throw new Exception("Device Code is required");
    }

    return true;
}

function main($request) {
    global $db_conn;
    $res = [];
    $res["status"] = true;
    $res['code'] = "M0001";
    $res['message'] = "Check successfully.";
    $res['last_request_at'] = date("Y-m-d H:i:s");
    api_check_device($request);
    return $res;
        
}

try {
    checking($_POST);
    $res = main($_POST);
    echo json_encode($res);

} catch (Exception $ex) {
    echo json_encode([
        "status" => false,
        "code" => "M0002",
        "message" => "Not the same device",
        "last_request" => date("Y-m-d H:i:s"),
        "params" => [
            "reason" => $ex->getMessage()
        ]
    ]);
}

require_once("bottom.php");


?>
