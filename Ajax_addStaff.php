<?php
	require_once("top.php");
	header('Content-Type: application/json; charset=utf-8');
	
	$debug = false;

	if($debug){
		print_r($_POST);
		$action = $_POST['action'];	

		$user_name = $_POST['user_name'];
		$firstname_chi = $_POST['firstname_chi'];
		$lastname_chi = $_POST['lastname_chi'];	
		$firstname_eng = $_POST['firstname_eng'];
		$lastname_eng = $_POST['lastname_eng'];
		$password = md5($_POST['password1']);
		$short_name = $_POST['short_name'];
		$address = $_POST['address'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];
		// $dob = str_replace('/', '-', $_POST['dob']);
		$dob = $_POST['dob'];
		$id_card = $_POST['id_card'];
		$sex = $_POST['sex'];
		// $job_title_id = $_POST['job_title'];
		$home_shop_id = $_POST['shop'];
		if(isset($_POST['cms_admin']) && $_POST['cms_admin'] == 'on'){
			$is_admin = 1;
		}else{
			$is_admin = 0;
		}
		$full_name = $lastname_eng." ".$firstname_eng;
		$staff_id = $_POST['staff_id'];
		$sql_update_staff = "UPDATE user_staff SET password = '$password',
															 short_name = '$short_name',
															 home_shop_id = $home_shop_id,
															 first_name = '$firstname_chi',
															 last_name = '$lastname_chi',
															 first_name_eng = '$firstname_eng',
															 last_name_eng = '$lastname_eng',
															 full_name = '$full_name',
															 dob = '$dob',
															 identity_number = '$id_card',
															 phone = '$phone',
															 address = '$address',
															 email = '$email',
															 sex = '$sex',
															 is_admin = '$is_admin'
									 WHERE id = $staff_id";
		print_r($sql_update_staff);
		return;
	}

	$error = false;

	// init data
	$action = $_POST['action'];	

	$user_name = $_POST['user_name'];
	$firstname_chi = $_POST['firstname_chi'];
	$lastname_chi = $_POST['lastname_chi'];	
	$firstname_eng = $_POST['firstname_eng'];
	$lastname_eng = $_POST['lastname_eng'];
	$password = md5($_POST['password1']);
	$short_name = $_POST['short_name'];
	$address = $_POST['address'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	// $dob = str_replace('/', '-', $_POST['dob']);
	$dob = $_POST['dob'];
	$id_card = $_POST['id_card'];
	$sex = $_POST['sex'];
	// $job_title_id = $_POST['job_title'];
	$home_shop_id = $_POST['shop'];
	if(isset($_POST['cms_admin']) && $_POST['cms_admin'] == 'on'){
		$is_admin = 1;
	}else{
		$is_admin = 0;
	}

	$result = [];

	switch ($action) {
		case 'add':
			// get new staff int/id
			$sql_new_id = "SELECT MAX(id) AS new_id FROM user_staff";
			$rs_new_id = mysqli_query($db_conn,$sql_new_id) or die ("$sql_new_id: ".mysqli_error($db_conn));
			$row_new_id = mysqli_fetch_assoc($rs_new_id);
			$new_staff_id = ($row_new_id['new_id'] + 1);

			if(!$new_staff_id){
				$result['status'] = false;
				$result['msg'] = "FAIL TO GET NEW STAFF ID";
			}else{
				// Create Staff
				$date_of_entry = date('Y-m-d');
				$staff_number = 'STAFF'.str_pad($new_staff_id, 4, '0', STR_PAD_LEFT);
				$full_name = $lastname_eng." ".$firstname_eng;
				$sql_create_staff = "INSERT INTO user_staff 
									 (staff_int,
									  user_name,
									  password,
									  short_name,
									  home_shop_id,
									  date_of_entry,
									  staff_number,
									  first_name,
									  last_name,
									  first_name_eng,
									  last_name_eng,
									  full_name,
									  identity_number,
									  dob,
									  phone,
									  address,
									  email,
									  sex,
									  is_admin)
									 VALUES
									 ($new_staff_id,
									 '$user_name',
									 '$password',
									 '$short_name',
									 $home_shop_id,
									 '$date_of_entry',
									 '$staff_number',
									 '$firstname_chi',
									 '$lastname_chi',
									 '$firstname_eng',
									 '$lastname_eng',
									 '$full_name',
									 '$id_card',
									 '$dob',
									 '$phone',
									 '$address',
									 '$email',
									 '$sex',
									 $is_admin);
									";
				$rs_create_staff = mysqli_query($db_conn,$sql_create_staff) or die ("$sql_create_staff: ".mysqli_error($db_conn));
				if($rs_create_staff){
					$result['status'] = true;
					$result['msg'] = 'staff create successful';
				}else{
					$result['status'] = false;
					$result['msg'] = $sql_create_staff;
				}
			}
			break;
		case 'edit':
			$staff_id = $_POST['staff_id'];
			if($staff_id){
				$full_name = $lastname_eng." ".$firstname_eng;
				$sql_update_staff = "UPDATE user_staff SET password = '$password',
															 short_name = '$short_name',
															 home_shop_id = $home_shop_id,
															 first_name = '$firstname_chi',
															 last_name = '$lastname_chi',
															 first_name_eng = '$firstname_eng',
															 last_name_eng = '$lastname_eng',
															 full_name = '$full_name',
															 dob = '$dob',
															 identity_number = '$id_card',
															 phone = '$phone',
															 address = '$address',
															 email = '$email',
															 sex = '$sex',
															 is_admin = '$is_admin'
									 WHERE id = $staff_id";
				$rs_update_staff = mysqli_query($db_conn,$sql_update_staff) or die ("$sql_update_staff: ".mysqli_error($db_conn));
				if($rs_update_staff){
					$result['status'] = true;
					$result['msg'] = 'staff update successful';
				}else{
					$result['status'] = false;
					$result['msg'] = 'staff update FAILED';
				}

			}else{
				$result['status'] = false;
				$result['msg'] = 'FAIL TO GET STAFF ID';
			}
			break;
		default:
			$result['status'] = false;
			$result['msg'] = 'FAIL TO GET ACTION';
			break;
	}


	echo json_encode($result);


	require_once("bottom.php");
?>