<?php
require_once("top.php");
require_once 'php_lib/api_functions.php';
header('Content-Type: application/json; charset=utf-8');

function checking(&$req) {

    if($req['image'] == null){
        throw new Exception("No File Received");
    }

    if($req['image']['size'] <= 0){
        throw new Exception("Image Size is not valid");
    }
    return true;
}


function main($request,$file) {
    global $db_conn;
    $res = [];
    $res["status"] = true;
    $res['code'] = "E0001";
    $res['message'] = "Upload Successfully.";
    $res['last_request_at'] = date("Y-m-d H:i:s");
    //$res['params'] = $request['image'];
    $res['params'] = uploadImg($request,$file);
    return $res;
}

try {
    checking($_FILES);
    // $res['debug1'] = $_POST;
    // $res['debug2'] = $_REQUEST;

    $res = main($_POST,$_FILES["image"]);
    echo json_encode($res);
} catch (Exception $ex) {
    echo json_encode([
        "status" => false,
        "code" => "E0002",
        "message" => "Upload Fail",
        //"message" => $ex->getMessage(),
        "last_request" => date("Y-m-d H:i:s"),
        "params" => [
            "reason" => $ex->getMessage()
        ]
    ]);

}



require_once("bottom.php");
?>