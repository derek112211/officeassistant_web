<?php

require_once("top.php");
require_once 'php_lib/api_functions.php';
header('Content-Type: application/json; charset=utf-8');

function checking(&$req) {
    global $db_conn;
    
    // $req["lantitude"] = mysqli_real_escape_string($db_conn,$req['lantitude']);
    // $req["longitude"] = mysqli_real_escape_string($db_conn,$req['longitude']);
    // $req["address"] = mysqli_real_escape_string($db_conn, $req['address']);
    $req["staff_id"] = mysqli_real_escape_string($db_conn, $req['staff_id']);
    $req["type"] = mysqli_real_escape_string($db_conn, $req['type']);
    
    // if(!api_utils_issetAndNotEqual($req['lantitude'])){
    //     throw new Exception("Lantitude is missing");
    // }

    // if(!api_utils_issetAndNotEqual($req['longitude'])){
    //     throw new Exception("Longitude is missing");
    // }

    if(!api_utils_issetAndNotEqual($req['type'])){
        throw new Exception("Type is missing");
    }

    if(!api_utils_issetAndNotEqual($req['staff_id'])){
        throw new Exception("Staff id is missing");
    }
    
    return true;
}

function main($request) {
    global $db_conn;
    $res = [];
    $res["status"] = true;
    $res['code'] = "M0001";
    $res['message'] = "Check in successfully.";
    $res['last_request_at'] = date("Y-m-d H:i:s");
    $res['params'] = api_check_in($request);
    return $res;
    
}


try {
    checking($_POST);
    $res = main($_POST);
    echo json_encode($res);

} catch (Exception $ex) {
    echo json_encode([
        "status" => false,
        "code" => "M0002",
        "message" => "Check In Failed",
        "last_request" => date("Y-m-d H:i:s"),
        "params" => [
            "reason" => $ex->getMessage()
        ]
    ]);
}

require_once("bottom.php");


?>
