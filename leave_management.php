<?php
  require_once("top.php");
  require_once("header.php");
  require_once("page_check.php");

  $debug = false;

  $page_title = "Leave Management";
  $sidebar = "leave_management";

  // init table header
  $table_header = [];
  $table_header[] = __LANG_RES_LEAVE_MANAGEMENT_ID;
  $table_header[] = __LANG_RES_LEAVE_MANAGEMENT_STAFF;
  $table_header[] = __LANG_RES_LEAVE_MANAGEMENT_SHOP;
  $table_header[] = __LANG_RES_LEAVE_MANAGEMENT_STATUS;
  $table_header[] = __LANG_RES_LEAVE_MANAGEMENT_DATE;
  $table_header[] = __LANG_RES_LEAVE_MANAGEMENT_ACTION;


  // delete
  if(isset($_POST['action']) && $_POST['action'] == 'delete'){

    $leave_id = $_POST['leave_id'];
    $leave_application_date_id = $_POST['leave_application_date_id'];

    mysqli_query($db_conn, "START TRANSACTION");
    $sql_delete_form = "UPDATE leave_application_form SET delete_staff_id = {$current_user['id']} WHERE id = $leave_id";
    $rs_delete_form = mysqli_query($db_conn,$sql_delete_form);


    $sql_staff_leave = "SELECT * FROM staff_leave WHERE leave_application_date_id = $leave_application_date_id";
    $rs_staff_leave = mysqli_query($db_conn,$sql_staff_leave) or die ("$sql_staff_leave :".mysqli_error($db_conn));
    $staff_leave = mysqli_fetch_assoc($rs_staff_leave);

    // update staff leave table
    $sql_delete_leave = "UPDATE staff_leave SET delete_staff_id = {$current_user['id']} WHERE id = {$staff_leave['id']}";
    $rs_delete_leave = mysqli_query($db_conn,$sql_delete_leave);

    // update staff schedule
    // $sql_delete_schedule = "UPDATE staff_schedule SET delete_staff_id = {$current_user['id']} WHERE id = {$staff_leave['staff_schedule_id']}";
    // $rs_delete_schedule = mysqli_query($db_conn,$sql_delete_schedule);

    if($rs_delete_form && $rs_delete_leave){
      // update leave table
      mysqli_query($db_conn, "COMMIT");
      $alert['style'] = 'success';
      $alert['msg'] = 'DELETE SUCCESS';

    }else{
      mysqli_query($db_conn, "ROLLBACK");
      // print_r($sql_delete_form);
      $alert['style'] = 'fail';
      $alert['msg'] = 'DELETE FAILED';

    }
  }

  // handle approve
  if(isset($_POST['action']) && ($_POST['action'] == 'approve' || $_POST['action'] == 'deny')){
    $approve_time = date('Y-m-d H:i:s');
    $leave_id = $_POST['leave_id'];
    $leave_application_date_id = $_POST['leave_application_date_id'];
    $reason = $_POST['reason'];

    // get application data
    $sql_application = "SELECT * FROM leave_application_date WHERE id = $leave_application_date_id";
    $rs_application = mysqli_query($db_conn,$sql_application) or die ("$sql_application :".mysqli_error($db_conn));
    $application = mysqli_fetch_assoc($rs_application);

    // analysis action
    if($_POST['action'] == 'approve'){

      mysqli_query($db_conn, "START TRANSACTION");
      if($application['department_approve_status'] == -1){
        // approve as department head
        $sql_update_application = "UPDATE leave_application_date 
                                   SET department_approve_status = 1,
                                       department_approve_time = '$approve_time',
                                       department_approve_remark = '$reason',
                                       department_approve_staff_id = '{$current_user['id']}'
                                   WHERE id = $leave_application_date_id
                                   ";
        $rs_update_application = mysqli_query($db_conn,$sql_update_application);
        if($rs_update_application){
          mysqli_query($db_conn, "COMMIT");
          $alert['style'] = 'success';
          $alert['msg'] = 'UPDATE SUCCESS';
        }else{
          mysqli_query($db_conn, "ROLL BACK");
          $alert['style'] = 'fail';
          $alert['msg'] = 'UPDATE FAILED';      
        }
      }else if($application['hr_approve_status'] == -1){
        $sql_update_application = "UPDATE leave_application_date 
                                   SET hr_approve_status = 1,
                                       hr_approve_time = '$approve_time',
                                       hr_approve_remark = '$reason',
                                       hr_approve_staff_id = '{$current_user['id']}'
                                   WHERE id = $leave_application_date_id
                                   ";
        $rs_update_application = mysqli_query($db_conn,$sql_update_application);

        if($rs_update_application){
          // update application form

          $sql_update_application = "UPDATE leave_application_form 
                                     SET request_approve_time = '$approve_time',
                                         request_approve_status = 1,
                                         request_approve_staff_id = '{$current_user['id']}',
                                         request_approve_remark = '$reason'
                                     WHERE id = {$application['leave_application_form_id']}
                                     ";
          $rs_update_application = mysqli_query($db_conn,$sql_update_application);

          // create leave
          $sql_insert_staff_leave = "INSERT INTO staff_leave 
                                      (staff_schedule_item_id, leave_time_start, leave_time_end, leave_application_date_id) 
                                      VALUES 
                                      ('{$application['leave_type']}', '{$application['start_time']}', '{$application['end_time']}', '{$application['id']}')";
          $rs_insert_staff_leave = mysqli_query($db_conn, $sql_insert_staff_leave);
          $new_leave_id = mysqli_insert_id($db_conn);

          $temp_leave_date = $application['leave_date'];
          $temp_end_date = $application['end_date'];

          // find staff_schedule
          // update staff_Schedule to leave if any schedule is found
          $target_dates = [];
          $temp_start_date = $temp_leave_date;
          while($temp_start_date <= $temp_end_date){
              $target_dates[] = "'".$temp_start_date."'";
              $temp_start_date = date('Y-m-d',strtotime($temp_start_date . "+1 days"));
          }

          $sql_update_schedule = "UPDATE staff_schedule SET leave_id = $new_leave_id WHERE user_staff_id = working_date IN (".implode(',', $target_dates).")";
          $rs_update_staff_leave = mysqli_query($db_conn, $sql_update_schedule);

          if (!$rs_update_staff_leave)
          {
              $res['error'] = true;
              $res['sql_insert_staff_leave'] = $sql_update_schedule;
              mysqli_query($db_conn, "ROLLBACK");
              return $res;
          }


          if ($rs_update_application)
          {
            mysqli_query($db_conn, "COMMIT");
            $alert['style'] = 'success';
            $alert['msg'] = 'UPDATE SUCCESS';
          }else{
            $alert['style'] = 'fail';
            $alert['msg'] = 'UPDATE FAILED';    
          }


        }
      }
    }else if($_POST['action'] == 'deny'){

      mysqli_query($db_conn, "START TRANSACTION");
      if($application['department_approve_status'] == -1){
        // approve as department head
        $sql_update_application = "UPDATE leave_application_date 
                                   SET department_approve_status = 0,
                                       department_approve_time = '$approve_time',
                                       department_approve_remark = '$reason',
                                       department_approve_staff_id = '{$current_user['id']}'
                                   WHERE id = $leave_application_date_id
                                   ";
        $rs_update_application = mysqli_query($db_conn,$sql_update_application);

        $sql_cancel_leave = "UPDATE leave_application_form 
                                SET request_cancel_time = '{$approve_time}',
                                    request_cancel_application = 1, 
                                    request_approve_status = 0 
                                WHERE id = $leave_id";
        $rs_cancel_leave = mysqli_query($db_conn, $sql_cancel_leave) or die("$sql_cancel_leave ".mysqli_error($db_conn));

        if($rs_update_application && $rs_cancel_leave){
          mysqli_query($db_conn, "COMMIT");
          $alert['style'] = 'success';
          $alert['msg'] = 'UPDATE SUCCESS';
        }else{
          mysqli_query($db_conn, "ROLL BACK");
          $alert['style'] = 'fail';
          $alert['msg'] = 'UPDATE FAILED';      
        }
      }else if($application['hr_approve_status'] == -1){

        $sql_update_hr_date = "UPDATE leave_application_date 
                                   SET hr_approve_status = 0,
                                       hr_approve_time = '$approve_time',
                                       hr_approve_remark = '$reason',
                                       hr_approve_staff_id = '{$current_user['id']}'
                                   WHERE id = $leave_application_date_id
                                   ";

        $rs_update_hr_date = mysqli_query($db_conn,$sql_update_hr_date);

        // update application form
        $sql_update_hr_form = "UPDATE leave_application_form 
                                   SET request_cancel_time = '$approve_time',
                                       request_cancel_application = 2,
                                       request_approve_status = 0
                                   WHERE id = {$application['leave_application_form_id']}
                                   ";
        $rs_update_hr_form = mysqli_query($db_conn,$sql_update_hr_form);



          if ($rs_update_hr_form && $rs_update_hr_date)
          {
            mysqli_query($db_conn, "COMMIT");
            $alert['style'] = 'success';
            $alert['msg'] = 'UPDATE SUCCESS';
          }else{
            $alert['style'] = 'fail';
            $alert['msg'] = 'UPDATE FAILED';    
          }

      }
    }

  }

  // retrieve data from db

  // get staff
  $staffs = [];
  $sql_staff = "SELECT * FROM user_staff";
  $rs_staff = mysqli_query($db_conn,$sql_staff) or die ("$sql_staff :".mysqli_error($db_conn));
  while($row_staff = mysqli_fetch_assoc($rs_staff)){
    $staffs[$row_staff['id']] = $row_staff;
  } 

  // get current user permission
    $handled_staffs = [];
    $permissions = [];
    $sql_permission = "SELECT aas.*,aat.name_chi AS job_title 
                FROM leave_application_approver_staff AS aas
                LEFT JOIN leave_application_approver_type AS aat ON aat.id = aas.leave_application_approver_type_id
                WHERE leave_application_approver_staff_id = {$current_user['id']}";

    $rs_permission = mysqli_query($db_conn,$sql_permission) or die ("$sql_permission :".mysqli_error($db_conn));
    // $permission = mysqli_fetch_assoc($rs_permission);
    if(mysqli_num_rows($rs_permission) > 0){
      while($row_permission = mysqli_fetch_assoc($rs_permission)){
        $handled_staffs[] = $row_permission['user_staff_id'];
        if(!in_array($row_permission['job_title'], $permissions)){
          $permissions[] = $row_permission['job_title'];
        }
      }
    }


  // get leave application
  $leave_applications = [];
  $sql_leave_application = "SELECT laf.*,
                                   s.name AS shop_name,
                                   lad.department_approve_status,
                                   lad.hr_approve_status,
                                   lad.id AS leave_application_date_id
                            FROM leave_application_form AS laf
                            LEFT JOIN shop AS s ON s.id = laf.shop_id
                            LEFT JOIN leave_application_date AS lad ON lad.leave_application_form_id = laf.id
                            WHERE laf.delete_staff_id = 0
                            
                           ";

  if(sizeof($handled_staffs)>0){
    $sql_leave_application .= "AND laf.user_staff_id IN (".implode(',', $handled_staffs).") ";
  }
  $rs_leave_application = mysqli_query($db_conn,$sql_leave_application) or die ("$sql_leave_application :".mysqli_error($db_conn));
  while($row_leave_application = mysqli_fetch_assoc($rs_leave_application)){
    $leave_applications[$row_leave_application['id']] = $row_leave_application;
  } 



  // retrieve data from db - end  

?>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">


<?php 

require_once('nav.php');
require_once('sidebar.php'); 

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $page_title ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="homePage.php">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $page_title ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SHOP UUID TABLE -->
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-lg-11 col-6">
                  <h3 class="card-title">Leave Record - Your position as <b><?php echo implode(' & ', $permissions); ?></b>
                  </h3>
                </div>          
              </div>
            </div>

            <!-- /.card-header -->
            <div class="card-body">

              <?php 

              if(isset($alert)){ 
                if($alert['style'] == 'success'){
                  echo '<div class="alert alert-success alert-dismissible">';
                  echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                  echo '<h5><i class="icon fas fa-check"></i> Success!</h5>';
                  echo $alert['msg'].'</div>';
                }else{
                  echo '<div class="alert alert-danger alert-dismissible">';
                  echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                  echo '<h5><i class="icon fas fa-check"></i> FAILED!</h5>';
                  echo $alert['msg'].'</div>';
                }
              }

              ?>

<div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"></h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <!-- <input type="text" name="leave_table" id="leave_table" class="form-control float-right" placeholder="Search"> -->

                    <div class="input-group-append">
                      <!-- <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button> -->
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap" id="leave_table">
                  <thead>
                    <tr>
                      <?php
                        foreach ($table_header as $value) {
                          echo '<th>'.$value.'</th>';
                        }
                      ?>
                    </tr>
                  </thead>
                  <tbody>
                    <?php

                      foreach ($leave_applications as $application_id => $value) {
                        echo '<tr data-id="'.$value['id'].'" staff-id="'.$value['user_staff_id'].'">';
                        // ID
                        echo '<td>'.$value['id'].'</td>';

                        // Staff name
                        echo '<td>'.$staffs[$value['user_staff_id']]['full_name'].'</td>';

                        // Shop name
                        echo '<td>'.$value['shop_name'].'</td>';

                        // Status

                        echo '<td>';
                        $head_handled = false;
                        $hr_handled = false;
                        switch ($value['department_approve_status']) {
                          case '-1':
                            echo 'Waiting Department Head Approve';
                            break;
                          case '0':
                            $head_handled = true;
                            $hr_handled = true;
                            echo '<span style="color:red;">Rejected(Department Head)</span>';
                            break;
                          default:
                            $head_handled = true;
                            switch ($value['hr_approve_status']) {
                              case '-1':
                                echo 'Waiting HR Approve';
                                break;
                              case '0':
                                $hr_handled = true;
                                echo '<span style="color:red;">Rejected(HR)</span>';
                                break;
                              case '1':
                                $hr_handled = true;
                                echo '<span style="color:green;">Approved</span>';
                                break;
                              default:
                                echo '<span style="color:red;">Error</span>';
                                break;
                            }
                            break;
                        }
                        echo '<input type="hidden" id="approve_status" value="'.($hr_handled || $head_handled).'">';
                        echo '<input type="hidden" id="leave_application_date_id" value="'.$value['leave_application_date_id'].'">';
                        echo '</td>';

                        // Create Time
                        echo '<td>'.$value['create_time'].'</td>';
                        

                        // Action
                        $action_disabled = '';
                        if($hr_handled && $head_handled){
                          $action_disabled = 'disabled';
                        }

                        // if(!$head_handled){
                        //   $handle_status = 'department_approver';
                        // }else if(!$hr_handled){
                        //   $handle_status = 'hr_approver';
                        // }

                        echo '<td>';
                        
                        echo '<div class="row">';

                          echo '<div class="col-sm-6 col-lg-6 col-xl-6">';
                            echo '<button type="button" class="btn btn-block btn-success" onclick="approve_row($(this));" '.$action_disabled.'>';
                              echo __LANG_RES_LEAVE_MANAGEMENT_ACTION_APPROVE;
                            echo '</button>';
                          echo '</div>';

                          echo '<div class="col-sm-6 col-lg-6 col-xl-6">';
                            echo '<button type="button" class="btn btn-block btn-danger" onclick="deny_row($(this));" '.$action_disabled.'>';
                              echo __LANG_RES_LEAVE_MANAGEMENT_ACTION_DENIED;
                            echo '</button>';
                          echo '</div>';



                        echo '</div>';

                        echo '<div style="height:20px"></div>';

                        echo '<div class="row">';
                        echo '<div class="col-sm-6 col-lg-6 col-xl-6">';
                        echo '<button type="button" class="btn btn-block btn-primary" onclick="detail($(this));">'.__LANG_RES_LEAVE_MANAGEMENT_ACTION_EDIT.'</button>';
                        echo '</div>';
                        echo '<div class="col-sm-6 col-lg-6 col-xl-6"><button type="button" class="btn btn-block btn-danger" onclick="delete_row($(this));">'.__LANG_RES_LEAVE_MANAGEMENT_ACTION_DELETE.'</button></div>';
                        echo '</div>';
                        echo '</td>';
                      }

                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
            </div>
            <!-- /.card-body -->
          </div>

    </section>
    <!-- /.content -->
  </div>

<?php
  // load footer
  require_once("footer.php");

  // close resources, without global resources
  require_once("bottom.php");
?>
<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#leave_table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
    }); 
});


  function delete_row(element){
    bootbox.confirm('<?php echo __LANG_RES_LEAVE_MANAGEMENT_CONFIRM_DELETE_RECORD; ?>',function(result){
        if(result){
            if(element.parents('tr').find('#approve_status').val() == 1){
               bootbox.confirm('<?php echo __LANG_RES_LEAVE_MANAGEMENT_CONFIRM_DELETE_RECORD_APPROVED; ?>',function(result2){
                  if(result2){
                    var leave_id = $(element).parents('tr').attr('data-id');
                    var leave_application_date_id = $(element).parents('tr').find('#leave_application_date_id').val();

                    var form = document.createElement("form");
                    var element1 = document.createElement("input");
                    var element2 = document.createElement("input");
                    var action = document.createElement("input");

                    form.method = "POST";
                    form.action = "leave_management.php";

                    element1.value = leave_id;
                    element1.name = "leave_id";
                    element1.style.cssText = "display: none;";
                    form.appendChild(element1);

                    element2.value = leave_application_date_id;
                    element2.name = "leave_application_date_id";
                    element2.style.cssText = "display: none;";
                    form.appendChild(element2);

                    action.value = 'delete';
                    action.name = "action";
                    action.style.cssText = "display: none;";
                    form.appendChild(action);

                    document.body.appendChild(form);

                    form.submit();
                    form.remove();
                  }else{
                    return;
                  }
               });
            }

        }else{
          return;
        }
    }); 

  }

  function approve_row(element){
    bootbox.prompt("Please enter approve reason(optional)", function(result){ 
      var leave_id = $(element).parents('tr').attr('data-id');
      var staff_id = $(element).parents('tr').attr('staff-id');
      var leave_application_date_id = $(element).parents('tr').find('#leave_application_date_id').val();

      var form = document.createElement("form");
      var element1 = document.createElement("input");
      var element2 = document.createElement("input");
      var element3 = document.createElement("input");
      var element4 = document.createElement("input");
      var action = document.createElement("input");

      form.method = "POST";
      form.action = "leave_management.php";

      element1.value = leave_id;
      element1.name = "leave_id";
      element1.style.cssText = "display: none;";
      form.appendChild(element1);

      element2.value = leave_application_date_id;
      element2.name = "leave_application_date_id";
      element2.style.cssText = "display: none;";
      form.appendChild(element2);

      element3.value = result;
      element3.name = "reason";
      element3.style.cssText = "display: none;";
      form.appendChild(element3);

      element4.value = staff_id;
      element4.name = "staff_id";
      element4.style.cssText = "display: none;";
      form.appendChild(element4);

      action.value = 'approve';
      action.name = "action";
      action.style.cssText = "display: none;";
      form.appendChild(action);

      document.body.appendChild(form);

      form.submit();
      form.remove();
    });

  }
  function deny_row(element){
    bootbox.prompt("Please enter approve reason(optional)", function(result){ 
      var leave_id = $(element).parents('tr').attr('data-id');
      var staff_id = $(element).parents('tr').attr('staff-id');
      var leave_application_date_id = $(element).parents('tr').find('#leave_application_date_id').val();

      var form = document.createElement("form");
      var element1 = document.createElement("input");
      var element2 = document.createElement("input");
      var element3 = document.createElement("input");
      var element4 = document.createElement("input");
      var action = document.createElement("input");

      form.method = "POST";
      form.action = "leave_management.php";

      element1.value = leave_id;
      element1.name = "leave_id";
      element1.style.cssText = "display: none;";
      form.appendChild(element1);

      element2.value = leave_application_date_id;
      element2.name = "leave_application_date_id";
      element2.style.cssText = "display: none;";
      form.appendChild(element2);

      element3.value = result;
      element3.name = "reason";
      element3.style.cssText = "display: none;";
      form.appendChild(element3);

      element4.value = staff_id;
      element4.name = "staff_id";
      element4.style.cssText = "display: none;";
      form.appendChild(element4);

      action.value = 'deny';
      action.name = "action";
      action.style.cssText = "display: none;";
      form.appendChild(action);

      document.body.appendChild(form);

      form.submit();
      form.remove();
    });

  }
  function detail(element){
      var leave_appliaction_form_id = $(element).parents('tr').attr('data-id'); //leave_appliaction_form_id
      var staff_id = $(element).parents('tr').attr('staff-id');
      var leave_application_date_id = $(element).parents('tr').find('#leave_application_date_id').val();

      var form = document.createElement("form");
      var element1 = document.createElement("input");
      var element2 = document.createElement("input");
      var element3 = document.createElement("input");

      form.method = "POST";
      form.action = "add_leave.php";

      element1.value = leave_appliaction_form_id;
      element1.name = "leave_appliaction_form_id";
      element1.style.cssText = "display: none;";
      form.appendChild(element1);

      element2.value = leave_application_date_id;
      element2.name = "leave_application_date_id";
      element2.style.cssText = "display: none;";
      form.appendChild(element2);

      element3.value = staff_id;
      element3.name = "staff_id";
      element3.style.cssText = "display: none;";
      form.appendChild(element3);

      document.body.appendChild(form);

      form.submit();
      form.remove();  

  }
</script>