<?php
  require_once("top.php");
  require_once("header.php");
  require_once("page_check.php");

  $debug = false;

  $page_title = "Shop Management";
  $sidebar = "shop_management";

  // init table header
  $table_header = [];

  $table_header[] = __LANG_RES_SHOP_MANAGEMENT_SHOP_NO;
  $table_header[] = __LANG_RES_SHOP_MANAGEMENT_SHOP_NAME;
  $table_header[] = __LANG_RES_SHOP_MANAGEMENT_SHOP_ADDRESS;
  $table_header[] = __LANG_RES_SHOP_MANAGEMENT_SHOP_PHONE;
  $table_header[] = __LANG_RES_SHOP_MANAGEMENT_SHOP_FAX;
  $table_header[] = __LANG_RES_SHOP_MANAGEMENT_SHOP_WEBSITE;
  $table_header[] = __LANG_RES_SHOP_MANAGEMENT_SHOP_IP;
  // $table_header[] = __LANG_RES_SHOP_MANAGEMENT_SHOP_GEO;
  $table_header[] = __LANG_RES_SHOP_MANAGEMENT_SHOP_ACTION;

  // retrieve data from db
  $shops = [];
  $sql_shop = "SELECT s.*,sfi.ip_address,su.device_token FROM shop AS s
                LEFT JOIN shop_fixed_ip AS sfi ON s.id = sfi.shop_id
                LEFT JOIN shop_uuid AS su ON s.id = su.shop_id
                GROUP BY shop_no
               ";
  $rs_shop = mysqli_query($db_conn,$sql_shop) or die ("$sql_shop :".mysqli_error($db_conn));
  while($row_shop = mysqli_fetch_assoc($rs_shop)){
    $shops[] = $row_shop;
  } 

  if($debug){
    echo '<br>$shops</br>';
    print_r($shops);
  }

?>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">



<?php 

require_once('nav.php');
require_once('sidebar.php'); 

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $page_title ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="homePage.php">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $page_title ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- TABLE -->
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-lg-11 col-6"><h3 class="card-title">Shops</h3></div>
                

                <div class="col-lg-1 col-6">
                  <a href="add_shop.php" class="btn btn-block btn-success text-nowrap" role="button">Add Shop</a>
                </div>
                
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="shop_table" class="table table-bordered table-striped nowrap">
                  <thead>
                  <tr>
                    <?php
                      foreach ($table_header as $header) {
                        echo "<th>".$header."</th>";
                      }
                    ?>
                  </tr>
                  </thead>
                  <tbody>
                    <!-- table body -->
                    <?php
                      foreach ($shops as $shop) {
                        echo '<tr data-id="'.$shop['id'].'">';
                        echo '<td>'.$shop['shop_no'].'</td>';
                        echo '<td>'.$shop['name'].'</td>';
                        echo '<td>'.$shop['address'].'</td>';
                        echo '<td>'.$shop['phone'].'</td>';
                        echo '<td>'.$shop['fax'].'</td>';
                        echo '<td>'.$shop['website'].'</td>';
                        echo '<td>'.$shop['ip_address'].'</td>';

                        // geo location
                        // echo '<td>';
                        // echo 'LAT: '.$shop['latitude'].'<br>';
                        // echo 'LONG: '.$shop['longtitude'];
                        // echo '</td>';

                        echo '<td>';
                        echo '<div class="row">';
                        echo '<div class="col-sm-12 col-lg-12 col-xl-12"><button type="button" class="btn btn-block btn-primary" onclick="edit_shop($(this));">Edit</button></div>';
                        echo '<div class="col-sm-12 col-lg-12 col-xl-12"><button type="button" class="btn btn-block btn-danger" onclick="delete_shop($(this));">Delete</button></div>';
                        echo '</div>';
                        echo '</td>';

                        echo '</tr>';
                      }
                    ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <!-- table footer -->
                  </tr>
                  </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
          </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

<?php
  // load footer
  require_once("footer.php");

  // close resources, without global resources
  require_once("bottom.php");
?>
<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#shop_table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
  });
  });

  function delete_shop(element){
    var shop_id = element.parents('tr').attr('data-id');

    bootbox.confirm("<?php echo __LANG_RES_SHOP_CONFIRM_DELETE; ?>", function(result){ 
      if(result){
        $.ajax({
             type: "POST",
             url: "Ajax_deleteShop.php",
             data: {
                shop_id : shop_id
             },
             type:"POST",
             success: function(data)
             {
                // console.log(data.status);
                // console.log(data.msg);
                if(data.status){
                  window.location.href = "shop_management.php";
                  // bootbox.alert(data.msg);
                }else{
                  bootbox.alert(data.msg);
                }
             },
             error:function(xhr, ajaxOptions, thrownError){ 
                alert(xhr.status); 
                alert(thrownError); 
             }
         });
      }
    });
  }

  function edit_shop(element){
    var shop_id = element.parents('tr').attr('data-id');
    var form = document.createElement("form");
    var element1 = document.createElement("input");

    form.method = "POST";
    form.action = "add_shop.php";

    element1.value = shop_id;
    element1.name = "shop_id";
    element1.style.cssText = "display: none;";
    form.appendChild(element1);

    document.body.appendChild(form);

    form.submit();
    form.remove();
  }

</script>